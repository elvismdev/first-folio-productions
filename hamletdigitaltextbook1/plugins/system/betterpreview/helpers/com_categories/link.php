<?php
/**
 * Link Helper class: com_categories
 *
 * @package         Better Preview
 * @version         3.0.0m
 *
 * @author          Peter van Westen <peter@nonumber.nl>
 * @link            http://www.nonumber.nl
 * @copyright       Copyright © 2013 NoNumber All Rights Reserved
 * @license         http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 */

defined('_JEXEC') or die;

class helperBetterPreviewLinkCategories extends helperBetterPreviewLink
{
	function getLinks()
	{
		// don't show any extra links by default
		return array();
	}
}
