<?php
/**
 * Helper class: com_k2.item
 *
 * @package         Better Preview
 * @version         3.0.0m
 *
 * @author          Peter van Westen <peter@nonumber.nl>
 * @link            http://www.nonumber.nl
 * @copyright       Copyright © 2013 NoNumber All Rights Reserved
 * @license         http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 */

defined('_JEXEC') or die;

class helperBetterPreviewPreviewK2item extends helperBetterPreviewPreview
{

	function renderPreview(&$article, $context)
	{
		if ($context != 'com_k2.item' || !isset($article->id) || $article->id != JFactory::getApplication()->input->get('id')) {
			return;
		}
		parent::renderPreview($article, $context);
	}

	function initStates()
	{
		$this->getState(
			JFactory::getApplication()->input->get('id'),
			'k2_items',
			array('parent' => 'catid')
		);
		$item = $this->states[count($this->states) - 1];
		while ($item->parent != 0) {
			$this->getState(
				$item->parent,
				'k2_categories',
				array(),
				1
			);
			$item = $this->states[count($this->states) - 1];
		}
		$this->setStates();
	}

	function getShowIntro(&$article)
	{
		if (isset($article->params)) {
			return 1;
		}

		if (!is_object($params)) {
			$params = (object) json_decode($article->params);
			return $params->itemIntroText;
		}

		return $article->params->get('itemIntroText', '1');
	}
}
