<?php
/**
 * Plugin Preview Helper File
 *
 * @package         Better Preview
 * @version         3.0.0m
 *
 * @author          Peter van Westen <peter@nonumber.nl>
 * @link            http://www.nonumber.nl
 * @copyright       Copyright © 2013 NoNumber All Rights Reserved
 * @license         http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 */

defined('_JEXEC') or die;

/**
 * Plugin that cleans cache
 */
class helperBetterPreviewPreview extends plgSystemBetterPreviewHelper
{
	function __construct(&$params)
	{
		$this->params = $params;
		$this->states = array();
		$this->errors = array();
	}

	function renderPreview(&$article, $context)
	{
		JHtml::_('jquery.framework');
		JHtml::script('betterpreview/preview.min.js', false, true);
		JHtml::stylesheet('betterpreview/preview.min.css', false, true);

		$has_changes = 0;
		$data = JFactory::getApplication()->input->get('previewdata', array(), 'array');
		$this->urlDecode($data);

		if ($this->getShowIntro($article) && isset($article->introtext) && isset($article->fulltext)) {
			$textpre = $article->introtext . ' ' . $article->fulltext;
		} elseif (isset($article->fulltext)) {
			$textpre = $article->fulltext;
		} elseif (isset($article->introtext)) {
			$textpre = $article->introtext;
		} else {
			$textpre = $article->text;
		}

		if (isset($data['attribs']) && !isset($data['params'])) {
			$data['params'] = $data['attribs'];
		}

		foreach ($data as $key => $val) {
			// ignore unexsistent fields
			if (!isset($article->{$key})) {
				continue;
			}

			// ignore crappy tags
			// and dynamic/unsettable fields
			// and date fields (because offset checking is a hell)
			if (in_array($key, array('tags', 'hits', 'version', 'created', 'modified', 'publish_up', 'publish_down'))) {
				continue;
			}

			$objects = array('params', 'attribs', 'metadata', 'urls', 'images');

			if (in_array($key, $objects)) {
				if (is_object($article->{$key})) {
					foreach ($val as $k => $v) {
						if (!is_string($v) || $v != '') {
							if (!$has_changes && $this->diff($article->{$key}->get($k), $v)) {
								$has_changes = 1;
							}
							$article->{$key}->set($k, $v);
						}
					}
					continue;
				} else if (is_string($article->{$key})) {
					$obj = (object) json_decode($article->{$key});
					if (is_object($obj)) {
						// force fields to null if originals are.
						foreach ($val as $k => $v) {
							$this->prepareValueByKey($v, $k, $obj);
							if (!isset($v)) {
								unset($val[$k]);
							} else {
								$val[$k] = $v;
							}
						}
						$val = urldecode(json_encode($val));
					}
				}
			} else {
				$this->prepareValueByKey($val, $key, $article);
			}

			if (!in_array($key, array('introtext', 'fulltext', 'text'))) {
				if (!$has_changes && $this->diff($article->{$key}, $val)) {
					$has_changes = 1;
				}
			}

			$article->{$key} = $val;
		}

		if ($this->getShowIntro($article) && isset($article->introtext) && isset($article->fulltext)) {
			$article->text = $article->introtext . ' ' . $article->fulltext;
		} elseif (isset($article->fulltext)) {
			$article->text = $article->fulltext;
		} elseif (isset($article->introtext)) {
			$article->text = $article->introtext;
		}

		if (!$has_changes && $this->diff($article->text, $textpre)) {
			$has_changes = 1;
		}

		if ($has_changes) {
			$this->errors['BP_MESSAGE_HAS_CHANGES'] = JText::_('BP_MESSAGE_HAS_CHANGES');
		}
	}

	function urlDecode(&$data)
	{
		if (is_array($data) || is_object($data)) {
			foreach ($data as $k => $v) {
				$this->urlDecode($data[$k]);
			}
		} else if (is_string($data)) {
			$data = urldecode($data);
		}
	}

	function prepareValueByKey(&$val, $key, &$obj)
	{
		if (is_array($val) && $val == array(0)) {
			unset($val);

			return;
		}

		if (property_exists($obj, $key)) {
			$v = $obj->{$key};
			switch ($v) {
				case null:
					if (empty($val)) {
						$val = $v;
					}
					break;
				case '0000-00-00 00:00:00':
					if ($val == '') {
						$val = $v;
					}
					break;
			}
			if (is_bool($v)) {
				$val = $val ? true : false;
			}
		}
	}

	function diff($str1, $str2)
	{
		if (is_string($str1) && !is_string($str2)) {
			if (!is_array($str2)) {
				return 1;
			}
			$str1 = explode(',', $str1);
		} else if (is_array($str1) && !is_array($str2)) {
			return 1;
		} else if (is_object($str1) && !is_object($str2)) {
			return 1;
		}

		if (is_array($str2) || is_object($str2)) {
			foreach ($str2 as $k => $v) {
				if ($this->diff($str1[$k], $v)) {
					return 1;
				}
			}
		} else if (is_string($str2)) {
			return (trim(preg_replace('#\s#', '', $str1)) != trim(preg_replace('#\s#', '', $str2)));
		}

		return 0;
	}

	function addMessages()
	{
		$html = JResponse::getBody();

		if ($html == '') {
			return;
		}

		// Set the category description if original description is empty
		// Need to do this because the onContentPrepare is not triggered when the description is empty
		if (JFactory::getApplication()->input->get('view') == 'category') {
			$empty_cat = '#(<div class="category-desc clearfix">)\s*(</div>)#';
			if (preg_match($empty_cat, $html)) {
				$data = JFactory::getApplication()->input->get('previewdata', array(), 'array');
				$this->urlDecode($data);
				if (isset($data['description'])) {
					$html = preg_replace($empty_cat, '\1' . $data['description'] . '\2', $html);
				}
			}
		}

		$html = str_replace('</body>', '<div class="betterpreview_message">' . JText::_('BP_MESSAGE_PAGE') . '</div></body>', $html);
		if (!empty($this->errors)) {
			$html = str_replace('</body>', '<div class="betterpreview_error">' . implode('<br />', $this->errors) . '</div></body>', $html);
		}

		JResponse::setBody($html);
	}

	function initStates()
	{
		$this->getState(
			JFactory::getApplication()->input->get('id'),
			'content',
			array('published' => 'state', 'parent' => 'catid')
		);
		$item = $this->states[count($this->states) - 1];
		while ($item->parent != 0) {
			$this->getState(
				$item->parent,
				'categories',
				array('parent' => 'parent_id'),
				1
			);
			$item = $this->states[count($this->states) - 1];
		}
		$this->setStates();
	}

	function restoreStates()
	{
		$this->setStates(1);
	}

	function getShowIntro(&$article)
	{
		return $article->params->get('show_intro', '1');
	}

	function getState($id, $table, $names = array(), $isparent = 0)
	{
		$names = (object) array_merge(
			array(
				'id' => 'id',
				'published' => 'published',
				'access' => 'access',
				'parent' => 'parent',
			), $names
		);

		$db = JFactory::getDbo();
		$query = $db->getQuery(true)
			->from('#__' . $table . ' as a')
			->where('a.' . $names->id . ' = ' . (int) $id);
		foreach ($names as $k => $v) {
			$query->select('a.' . $v . ' as ' . $k);
		}

		$db->setQuery($query);
		$item = $db->loadObject();

		$this->states[] = (object) array(
			'table' => $table,
			'id' => $id,
			'published' => $item->published,
			'access' => $item->access,
			'parent' => $item->parent,
			'names' => $names
		);

		if (empty($this->errors)) {
			if ($item->published != 1) {
				$this->errors['BP_MESSAGE'] = $isparent ? JText::_('BP_MESSAGE_PARENT_UNPUBLISHED') : JText::_('BP_MESSAGE_ITEM_UNPUBLISHED');
			} else if ($item->access != 1) {
				$this->errors['BP_MESSAGE'] = $isparent ? JText::_('BP_MESSAGE_PARENT_ACCESS') : JText::_('BP_MESSAGE_ITEM_ACCESS');
			}
		}
	}

	function setStates($restore = 0)
	{
		foreach ($this->states as $state) {
			if ($state->published === 1 && $state->access === 1) {
				continue;
			}
			$db = JFactory::getDbo();
			$query = $db->getQuery(true)
				->update('#__' . $state->table)
				->where($db->quoteName($state->names->id) . ' = ' . $state->id);
			if ($restore) {
				$query->set($db->quoteName($state->names->published) . ' = ' . $state->published)
					->set($db->quoteName($state->names->access) . ' = ' . $state->access);
			} else {
				$query->set($db->quoteName($state->names->published) . ' = 1')
					->set($db->quoteName($state->names->access) . ' = 1');
			}
			$db->setQuery($query);
			$db->execute();
		}
	}

	function checkToken()
	{
		$db = JFactory::getDbo();
		$query = $db->getQuery(true)
			->select($db->quoteName('data'))
			->from($db->quoteName('#__session'))
			->where($db->quoteName('userid') . ' = ' . $db->quote(JFactory::getApplication()->input->get('user')))
			->where($db->quoteName('client_id') . ' = 1');
		$db->setQuery($query);
		$result = (string) $db->loadResult();

		if (!$result || !preg_match('#"session.token".*?"([^"]+)"#', $result, $match) || !$match['1']) {
			return false;
		}

		if ($match['1'] != JFactory::getApplication()->input->get('token')) {
			return false;
		}

		return true;
	}
}
