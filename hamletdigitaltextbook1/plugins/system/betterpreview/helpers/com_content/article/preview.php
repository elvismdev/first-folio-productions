<?php
/**
 * Helper class: com_content.article
 *
 * @package         Better Preview
 * @version         3.0.0m
 *
 * @author          Peter van Westen <peter@nonumber.nl>
 * @link            http://www.nonumber.nl
 * @copyright       Copyright © 2013 NoNumber All Rights Reserved
 * @license         http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 */

defined('_JEXEC') or die;

class helperBetterPreviewPreviewContentArticle extends helperBetterPreviewPreview
{

	function renderPreview(&$article, $context)
	{
		
		if ($context != 'com_content.article' || !isset($article->id) || $article->id != JFactory::getApplication()->input->get('id')) {
			return;
		}
		parent::renderPreview($article, $context);
	}
}
