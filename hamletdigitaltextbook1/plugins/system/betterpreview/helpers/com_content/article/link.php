<?php
/**
 * Link Helper class: com_content.article
 *
 * @package         Better Preview
 * @version         3.0.0m
 *
 * @author          Peter van Westen <peter@nonumber.nl>
 * @link            http://www.nonumber.nl
 * @copyright       Copyright © 2013 NoNumber All Rights Reserved
 * @license         http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 */

defined('_JEXEC') or die;

include_once JPATH_SITE . '/components/com_content/helpers/route.php';

class helperBetterPreviewLinkContentArticle extends helperBetterPreviewLink
{
	function getLinks()
	{
		$item = $this->getItem(
			JFactory::getApplication()->input->get('id'),
			'content',
			array('name' => 'title', 'published' => 'state', 'parent' => 'catid'),
			array('type' => 'NN_ARTICLE')
		);

		$parents = $this->getParents(
			$item,
			'categories',
			array('name' => 'title', 'parent' => 'parent_id'),
			array('type' => 'JCATEGORY'),
			1
		);

		$item->url = ContentHelperRoute::getArticleRoute($item->id, $item->parent);

		foreach ($parents as &$parent) {
			$parent->url = ContentHelperRoute::getCategoryRoute($parent->id);
		}

		return array_merge(array($item), $parents);
	}
}
