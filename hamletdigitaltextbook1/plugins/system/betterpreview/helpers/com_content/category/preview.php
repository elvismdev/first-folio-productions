<?php
/**
 * Helper class: com_content.category
 *
 * @package         Better Preview
 * @version         3.0.0m
 *
 * @author          Peter van Westen <peter@nonumber.nl>
 * @link            http://www.nonumber.nl
 * @copyright       Copyright © 2013 NoNumber All Rights Reserved
 * @license         http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 */

defined('_JEXEC') or die;

class helperBetterPreviewPreviewContentCategory extends helperBetterPreviewPreview
{

	function renderPreview(&$article, $context)
	{
		if ($context != 'com_content.category' || isset($article->introtext)) {
			return;
		}
		parent::renderPreview($article, $context);
	}

	function initStates()
	{
		$this->getState(
			JFactory::getApplication()->input->get('id'),
			'categories',
			array('parent' => 'parent_id')
		);
		$item = $this->states[count($this->states) - 1];
		while ($item->parent != 0) {
			$this->getState(
				$item->parent,
				'categories',
				array('parent' => 'parent_id'),
				1
			);
			$item = $this->states[count($this->states) - 1];
		}
		$this->setStates();
	}
}
