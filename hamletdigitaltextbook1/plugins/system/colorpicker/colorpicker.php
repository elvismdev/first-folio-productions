<?php
/**
* @version   2.0
* @author    XML/SWF http://www.xmlswf.com
* @copyright Copyright (C) 2012 - 2015  XML/SWF
* @license   http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
*/

// no direct access
defined('_JEXEC') or die('Restricted index access');

jimport('joomla.plugin.plugin');

  $app =& JFactory::getApplication();
  $doc =& JFactory::getDocument();

  if ($app->isAdmin()) {
     
	  $document = JFactory::getDocument();
	$document->addScript(JURI::root() .'plugins/system/colorpicker/jscolor/jscolor.js', 'text/javascript');

}
