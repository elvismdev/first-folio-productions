<?php
/**
 * @copyright	Copyright (C) 2005 - 2009 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

/**
 * Joomla User plugin
 *
 * @package		Joomla.Plugin
 * @subpackage	User.joomla
 * @since		1.5
 */
class plgUserAfterlogin_redirect extends JPlugin
{
	
	/**
	 * This method should handle any login logic and report back to the subject
	 *
	 * @param	array	$user		Holds the user data
	 * @param	array	$options	Array holding options (remember, autoregister, group)
	 *
	 * @return	boolean	True on success
	 * @since	1.5 
	 */
	public function onUserLogin($user, $options = array())
	{	
		global $mainframe;
		$mainframe	= JFactory::getApplication();
		$app		= JFactory::getApplication(); 
		$option		= JRequest::getVar('option');
		$task		= JRequest::getVar('task');
		$view		= JRequest::getVar('view');
		$session 	= JFactory::getSession();
		$redirectlink=$this->params->get('redirectlink'); 
		$instance = $this->_getUser($user, $options);
		$type = "";
		$userstatus = "";
		$sessionuser = $this->getUserSession($instance->id);
		
		//check already login user session
		if (!$app->isAdmin()) 
		{
			$updateuserstatus = $this->UpdateUserPaymentStatusByDate($instance->id);
			
			if($sessionuser > 0 )
			{
				$mainframe->redirect('login',JText::_('COM_SESSION_LOGIN_MSG'),'warning');
				return false;	
			}
				
			$type = $this->getUserTypes($instance->id);
			if($type)
			{
				$instance->usertype = $type;
			}
			else
			{
				$instance->usertype = $type;
			}
			//added by pooja check only for registred user
			if($instance->groups['2']==2)
			{
				$userstatus = $this->getUserStatus($instance->id,$type);
				if($userstatus->status)
				{
					$instance->userstatus = $userstatus->status;
					$instance->idsubscription = $userstatus->idsubscription;
					$instance->numoflicense = $userstatus->numoflicense;
					
				}
			}
		}
		// If _getUser returned an error, then pass it back.
		if ($instance instanceof Exception) 
		{
			return false;
		}

		// If the user is blocked, redirect with an error
		//echo $instance->userstatus;die; 
		//echo "<pre>";print_r($instance->groups);exit;
		
		if (!$app->isAdmin()) 
		{
			//$instance->groups['2']==2 added by pooja for only registred user
			if ($instance->userstatus != 'confirmed' && $instance->groups['2']==2) 
			{
				
					if($type == 1)
					{
						$mainframe->redirect('index.php?option=com_users&view=login',JText::_('COM_SUBSCRIPTION_UNBLOCK_MSG_C'),'error');
					}
					else
					{
						$mainframe->redirect('index.php?option=com_subscription&view=payment&layout=active',JText::_('COM_SUBSCRIPTION_UNBLOCK_MSG_P'),'error');
					}
			}
		}
		if ($instance->get('block') == 1) 
		{
			//JError::raiseWarning('SOME_ERROR_CODE', JText::_('JERROR_NOLOGIN_BLOCKED'));
			return false;
		}

		// Authorise the user based on the group information
		if (!isset($options['group'])) 
		{
			$options['group'] = 'USERS';
		}

		// Chek the user can login.
		$result	= $instance->authorise($options['action']);
		
		if (!$result) 
		{

			JError::raiseWarning(401, JText::_('JERROR_LOGIN_DENIED'));
			return false;
		}

		// Mark the user as logged in
		$instance->set('guest', 0);
		// Register the needed session variables
		$session->set('numoflicense', $instance->numoflicense);
		$session->set('idsubscription', $instance->idsubscription);
		$session->set('usertype', $instance->usertype);
		$session->set('userstatus', $instance->userstatus);
		$session->set('user', $instance);
		$db = JFactory::getDBO();
		// Check to see the the session already exists.
		$app = JFactory::getApplication();
		$app->checkSession();

		// Update the user related fields for the Joomla sessions table.
		$db->setQuery(
			'UPDATE '.$db->quoteName('#__session') .
			' SET '.$db->quoteName('guest').' = '.$db->quote($instance->get('guest')).',' .
			'	'.$db->quoteName('username').' = '.$db->quote($instance->get('username')).',' .
			'	'.$db->quoteName('userid').' = '.(int) $instance->get('id') .
			' WHERE '.$db->quoteName('session_id').' = '.$db->quote($session->getId())
		);
		$db->query();

		// Hit the user last visit field
		$instance->setLastVisit();
		
		if ($option=='com_users' && $task=='login') 
		{
			if($redirectlink!='')
			{
				$app->redirect(JRoute::_(JURI::Root().$redirectlink));
			}
		}
		//return true;
	}
	
	/**
	 * This method should handle any logout logic and report back to the subject
	 *
	 * @param	array	$user		Holds the user data.
	 * @param	array	$options	Array holding options (client, ...).
	 *
	 * @return	object	True on success
	 * @since	1.5
	 */
	public function onUserLogout($user, $options = array())
	{
		$my 		= JFactory::getUser();
		$session 	= JFactory::getSession();
		$app 		= JFactory::getApplication();

		// Make sure we're a valid user first
		if ($user['id'] == 0 && !$my->get('tmp_user')) 
		{
			return true;
		}

		// Check to see if we're deleting the current session
		if ($my->get('id') == $user['id'] && $options['clientid'] == $app->getClientId()) 
		{
			// Hit the user last visit field
			$my->setLastVisit();

			// Destroy the php session for this user
			$session->destroy();
		}

		// Force logout all users with that userid
		$db = JFactory::getDBO();
		$db->setQuery(
			'DELETE FROM '.$db->quoteName('#__session') .
			' WHERE '.$db->quoteName('userid').' = '.(int) $user['id'] .
			' AND '.$db->quoteName('client_id').' = '.(int) $options['clientid']
		);
		$db->query();

		return true;
	}

	/**
	 * This method will return a user object
	 *
	 * If options['autoregister'] is true, if the user doesn't exist yet he will be created
	 *
	 * @param	array	$user		Holds the user data.
	 * @param	array	$options	Array holding options (remember, autoregister, group).
	 *
	 * @return	object	A JUser object
	 * @since	1.5
	 */
	protected function _getUser($user, $options = array())
	{
		$instance = JUser::getInstance();
		if ($id = intval(JUserHelper::getUserId($user['username'])))  {
			$instance->load($id);
			return $instance;
		}

		//TODO : move this out of the plugin
		jimport('joomla.application.component.helper');
		$config	= JComponentHelper::getParams('com_users');
		// Default to Registered.
		$defaultUserGroup = $config->get('new_usertype', 2);

		$acl = JFactory::getACL();

		$instance->set('id'			, 0);
		$instance->set('name'			, $user['fullname']);
		$instance->set('username'		, $user['username']);
		$instance->set('password_clear'	, $user['password_clear']);
		$instance->set('email'			, $user['email']);	// Result should contain an email (check)
		$instance->set('usertype'		, 'deprecated');
		$instance->set('groups'		, array($defaultUserGroup));

		//If autoregister is set let's register the user
		$autoregister = isset($options['autoregister']) ? $options['autoregister'] :  $this->params->get('autoregister', 1);

		if ($autoregister) {
			if (!$instance->save()) {
				return JError::raiseWarning('SOME_ERROR_CODE', $instance->getError());
			}
		}
		else {
			// No existing user and autoregister off, this is a temporary user.
			$instance->set('tmp_user', true);
		}

		return $instance;
	}
	
	public static function getUserTypes($id)
	{
		$options = array();
		$db		= JFactory::getDbo();
		$query	= $db->getQuery(true);
		$query->select('a.usertype');
		$query->from('#__userinformation AS a');
		$query->where('a.userid ='.$id);
		// Get the options.
		$db->setQuery($query);
		$result = $db->loadObjectList();
		return $result[0]->usertype;
	}
	
	public static function getUserStatus($id,$type)
	{
		if($type == 1)
		{
				$options = array();
				$db		= JFactory::getDbo();
				$query	= $db->getQuery(true);
				$query->select('u.parent_userid as idparent,u.idsubscription,s.status,s.numoflicense');
				$query->from('#__users_license AS u,#__subscription as s');
				$query->where('s.id = u.idsubscription');
				$query->where('u.userid ='.$id);
				//add extra condition
				$query->where('s.status = "confirmed"');
				$db->setQuery($query);
				$result = $db->loadObjectList();
				if($result[0]->status == "confirmed")
				{				
						$query2	= $db->getQuery(true);
						$query2->select('a.block');
						$query2->from('#__users AS a');
						$query2->where('a.id ='.$result[0]->idparent);
						$db->setQuery($query2);
						$res = $db->loadObjectList();
						if($res[0]->block == 0)
						{
							return $result[0];
						}
						else
						{
							return "expired";
						}
				}
				else
				{
						return $result[0];
				}
		}
		else
		{
				$options = array();
				$db		= JFactory::getDbo();
				$query	= $db->getQuery(true);
				$query->select('s.status,s.id as idsubscription,s.numoflicense');
				$query->from('#__subscription as s');
				$query->where('s.status = "confirmed"');
				$query->where('s.userid ='.$id);
				// Get the options.
				$db->setQuery($query);
				$result = $db->loadObjectList();
				return $result[0];
		}
	}
	
	public function getUserSession($id)
	{
		$options = array();
		$db		= JFactory::getDbo();
		$query	= $db->getQuery(true);
		$query->select('count(*) as tot');
		$query->from('#__session as s');
		$query->where('s.userid ='.$id);
		// Get the options.
		$db->setQuery($query);
		$result = $db->loadObjectList();
		return $result[0]->tot;
	}
	public function getinfo($id)
	{
		$options = array();
		$db		= JFactory::getDbo();
		$query	= $db->getQuery(true);
		$query->select('a.id,a.name,a.email');
		$query->from('#__users as a');
		$query->where('a.id ='.$id);
		// Get the options.
		$db->setQuery($query);
		$result = $db->loadObjectList();
		return $result[0];
	}
	
	public function UpdateUserPaymentStatusByDate($id)
	{
				$options = array();
				$link = JUri::root().'index.php?option=com_subscription&view=payment&layout=active';
				$db		= JFactory::getDbo();
				$query	= $db->getQuery(true);
				$query->select('s.*');
				$query->from('#__subscription as s');
				$query->where('s.userid ='.$id);
				$query->where('s.status ="confirmed"');
				$query->where('date(s.expirationdate) < "'.date('Y-m-d').'"');
				// Get the options.
				$db->setQuery($query);
				$result = $db->loadObjectList();
				if($result[0]->status == "confirmed")
				{
					$db =& JFactory::getDBO();
					$query= "UPDATE #__subscription SET status='expired',modify_date ='".date('Y-m-d H:i:s')."'  where status ='confirmed' and userid=".$id;
					$db->setQuery($query);
					$db->query();
					//insert in log
					$result			= $result[0];
					$insertdata =new stdClass();
					$insertdata->id = null;
					$insertdata->userid = $id;
					$insertdata->numoflicense = $result->numoflicense;
					$insertdata->price = $result->price;
					$insertdata->purchasedate = $result->purchasedate;
					$insertdata->expirationdate = $result->expirationdate;
					$insertdata->licensekey = $result->licensekey;
					$insertdata->createdby = $result->createdby;
					$insertdata->ipaddress = $result->ipaddress;
					$insertdata->status = 	"expired";
					$insertdata->modify_date = date('Y-m-d H:i:s');
					$db->insertObject( '#__subscription_log', $insertdata, id );
					//send mail
					$display_msg 	= "expired";
					$data  			= $this->getinfo($id);
					$config     	= &JFactory::getConfig();
					$from           = $config->get('mailfrom');
					$fromname   	= $config->get('fromname');
					$email			= $data->email;
					$exdate 		= explode(" ",$result->expirationdate);
					$subject        =  JText::_('COM_SUBSCRIPTION_EXPIRE_SUBJECT');
					$msg 			= JText::sprintf('COM_SUBSCRIPTION_EXPIRE_MSG',$config->get('sitename'),$data->name,$exdate[0],$link,$config->get('sitename'));
					$mode 			= 1;
					JFactory::getMailer()->sendMail($from, $fromname, $email, $subject, $msg,$mode);
				}
				return "update";
	}
}
