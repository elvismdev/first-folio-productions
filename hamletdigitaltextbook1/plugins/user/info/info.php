<?php 
defined('JPATH_BASE') or die;
 
class plgUserInfo extends JPlugin
{
    function onContentPrepareData($context, $data)
    {
			
		// Check we are manipulating a valid form.
        if (!in_array($context, array('com_users.profile','com_users.registration','com_users.user','com_admin.profile'))){
            return true;
        }
 
        $userId = isset($data->id) ? $data->id : 0;
 		
        // Load the profile data from the database.
        $db = &JFactory::getDbo();
        $session = JFactory::getUser();
		$app = JFactory::getApplication();
		
		if(isset($session->id) && !$app->isAdmin())
		{
			$db->setQuery(
            'SELECT 
					
					ui.idparent,ui.usertype,ui.companyname,ui.streetname,ui.city,ui.state,ui.zipcode,s.numoflicense,s.price,date(s.purchasedate) as startdate,date(s.expirationdate) as enddate
					 
			FROM 
				#__userinformation ui
			LEFT JOIN #__subscription s ON s.userid = ui.userid and s.status = "confirmed"   
			WHERE 
					ui.userid = '.(int) $userId);
		}
		else
		{
			$db->setQuery(
            'SELECT idparent,usertype,companyname,streetname,city,state,zipcode FROM #__userinformation ' .' WHERE userid = '.(int) $userId);
		}
			
        $results = $db->loadObjectList();
 		
        // Check for a database error.
        if ($db->getErrorNum()) {
            $this->_subject->setError($db->getErrorMsg());
            return false;
        }
 		
        // Merge the profile data.
      // $data->info = array();
		if(isset($results[0]))
		{
        	foreach ($results[0] as $k=>$v) {
		    	$data->info[$k] = $v;
			
        	}
		}
		
        return true;
    }
 
    function onContentPrepareForm($form, $data)
    { 
        
		// Load user_profile plugin language
        $lang = JFactory::getLanguage();
        $lang->load('plg_user_info', JPATH_ADMINISTRATOR);
 
        if (!($form instanceof JForm)) {
            $this->_subject->setError('JERROR_NOT_A_FORM');
            return false;
        }
		
        // Check we are manipulating a valid form.
        if (!in_array($form->getName(), array('com_users.profile', 'com_users.registration','com_users.user','com_admin.profile'))) {
            return true;
        }

        // Add the profile fields to the form.
        JForm::addFormPath(dirname(__FILE__).'/profiles');
		$app = JFactory::getApplication();
		
		if ($app->isAdmin()) 
		{
        	$form->loadFile('profile', false);
			$fields = array(
			'idparent',
			'usertype',
			'companyname',
			'streetname',
			'city',
			'state',
			'zipcode',
			); 
		}
		else
		{
			$session = JFactory::getUser();
			if(isset($session->id) && $session->id > 0)
			{
				$form->loadFile('front_profile', false);
			}
			else
			{
				$form->loadFile('front_profile', false);
				$form->loadFile('front_plan', false);
				$form->loadFile('payment', false);
			}
			$fields = array(
			'companyname',
			'streetname',
			'city',
			'state',
			'zipcode',
			); 
			
		
		}
        
    }
 
    function onUserAfterSave($data, $isNew, $result, $error)
    {    
		$userId    = JArrayHelper::getValue($data, 'id', 0, 'int');
		$app = JFactory::getApplication();
		$session = JFactory::getSession();
		$getUser = JFactory::getUser();
		
		jimport( 'joomla.filesystem.folder' );
		
		if ($userId && $result && isset($data['info']) && (count($data['info'])))
        {
				try
				{
					
					$usertype = 0;
					$idparent = 0;
					if($session->get('usertype') == 1)
					{		
						$usertype = $session->get('usertype');
						$idparent = $this->getUserParentId($userId);
					}
					else
					{
						if(isset($data['info']['usertype']) && $data['info']['usertype'] != 0 && isset($data['info']['idparent']))
						{
							$usertype = 1;
							$idparent = $data['info']['idparent'];
						}
					}
				
				
					$db = &JFactory::getDbo();
					$db->setQuery('DELETE FROM #__userinformation WHERE userid = '.$userId);
					if (!$db->query()) 
					{
						throw new Exception($db->getErrorMsg());
					}
 				$tuples = array();
				$tuples[] = '(" ",'.$idparent.','.$userId.','.$usertype.','.$db->quote($data['info']['companyname']).','.$db->quote($data['info']['streetname']).','.$db->quote($data['info']['city']).','.$db->quote($data['info']['state']).','.$db->quote($data['info']['zipcode']).','.$db->quote($_SERVER['REMOTE_ADDR']).')';
               				
               $db->setQuery('INSERT INTO #__userinformation VALUES '.implode(', ', $tuples));
			   if (!$db->query()) 
				{
                    throw new Exception($db->getErrorMsg());
                }
            }
            catch (JException $e) {
                $this->_subject->setError($e->getMessage());
                return false;
            }
        }
		
		
		if ($userId && $result && isset($data['info']['usertype']) && $data['info']['usertype'] != 0 && isset($data['info']['idparent']))
        {
				$idparent = $data['info']['idparent'];
				$idsubscription	= $this->getSubscriptionId($idparent);
				try
				{
					$db = &JFactory::getDbo();
					$db->setQuery('DELETE FROM #__users_license WHERE userid = '.$userId.' and parent_userid = '.$idparent);
					if (!$db->query()) 
					{
						throw new Exception($db->getErrorMsg());
					}
 				
				$tuples = array();
				$tuples[] = '(" ",'.$userId.','.$idparent.','.$idsubscription.')';
                $db->setQuery('INSERT INTO #__users_license VALUES '.implode(', ', $tuples));
			   if (!$db->query()) 
				{
                    throw new Exception($db->getErrorMsg());
                }
            }
            catch (JException $e) 
			{
                $this->_subject->setError($e->getMessage());
                return false;
            }
        }
		
		return true;
    }
 
    function onUserBeforeDelete($user)
    {
		
		$db = JFactory::getDbo();
		/*if (!$success) 
		{
            return false;
        }*/
 		$userId    = JArrayHelper::getValue($user, 'id', 0, 'int');
		if ($userId)
        {
			
            try
            {
				$usertype	= $this->getUserTypes($userId);
				if($usertype != 1)
				{
						
						$subuserlisting	= $this->getSubuserListing($userId);
						
						if(count($subuserlisting) > 0)
						{
							$db->setQuery('DELETE FROM #__users WHERE id in ('.implode(",",$subuserlisting) . ')');
							if (!$db->query()) 
							{
								throw new Exception($db->getErrorMsg());
							}
							
							$db->setQuery('DELETE FROM #__user_usergroup_map WHERE user_id in ('.implode(",",$subuserlisting) . ')');
							if (!$db->query()) 
							{
								throw new Exception($db->getErrorMsg());
							}
						
							$db->setQuery('DELETE FROM #__users_license WHERE parent_userid = '.$userId);
							if (!$db->query()) 
							{
								throw new Exception($db->getErrorMsg());
							}
							
							$db->setQuery('DELETE FROM #__userinformation WHERE idparent = '.$userId);
							if (!$db->query()) 
							{
								throw new Exception($db->getErrorMsg());
							}
						}
						$db->setQuery('DELETE FROM #__subscription WHERE userid = '.$userId);
						if (!$db->query()) 
						{
							throw new Exception($db->getErrorMsg());
						}
						
						$db->setQuery('DELETE FROM #__subscription_log WHERE userid = '.$userId);
						if (!$db->query()) 
						{
							throw new Exception($db->getErrorMsg());
						}
				}
				else
				{
						$db->setQuery('DELETE FROM #__users_license WHERE userid = '.$userId);
						if (!$db->query()) 
						{
							throw new Exception($db->getErrorMsg());
						}
				}
				
						$db->setQuery('DELETE FROM #__userinformation WHERE userid = '.$userId);
						if (!$db->query()) 
						{
							throw new Exception($db->getErrorMsg());
						}
						
						$db->setQuery('DELETE FROM #__user_usergroup_map WHERE user_id = '.$userId);
						if (!$db->query()) 
						{
							throw new Exception($db->getErrorMsg());
						}
						
						$db->setQuery('DELETE FROM #__rokuserstats WHERE user_id = '.$userId);
						if (!$db->query()) 
						{
							throw new Exception($db->getErrorMsg());
						}
				
				
			}
            catch (JException $e)
            {
                $this->_subject->setError($e->getMessage());
                return false;
            }
			
        }
 
        return true;
	}
	function onUserAfterDelete($user, $success, $msg)
    {
       
    }
	
	public static function getUserParentId($id)
	{
		$options = array();
		$db		= JFactory::getDbo();
		$query	= $db->getQuery(true);
		$query->select('a.idparent');
		$query->from('#__userinformation AS a');
		$query->where('a.userid ='.$id);
		// Get the options.
		$db->setQuery($query);
		$result = $db->loadObjectList();
		return $result[0]->idparent;
	}
	public static function getSubscriptionId($id)
	{
		$options = array();
		$db		= JFactory::getDbo();
		$query	= $db->getQuery(true);
		$query->select('a.id');
		$query->from('#__subscription AS a');
		$query->where('a.status = "confirmed"');
		$query->where('a.userid ='.$id);
		// Get the options.
		$db->setQuery($query);
		$result = $db->loadObjectList();
		return $result[0]->id;
	}
	public static function getUserTypes($id)
	{
		$options = array();
		$db		= JFactory::getDbo();
		$query	= $db->getQuery(true);
		$query->select('a.usertype');
		$query->from('#__userinformation AS a');
		$query->where('a.userid ='.$id);
		// Get the options.
		$db->setQuery($query);
		$result = $db->loadObjectList();
		return $result[0]->usertype;
	}
	public static function getSubuserListing($id)
	{
		$options = array();
		$db		= JFactory::getDbo();
		$query	= $db->getQuery(true);
		$query->select('a.userid');
		$query->from('#__userinformation AS a');
		$query->where('a.idparent ='.$id);
		// Get the options.
		$db->setQuery($query);
		$result = $db->loadObjectList();
		$option = array();
		foreach ($result as $value)
  		{
  			$option[] = $value->userid;
  		}
		return $option;
	}
}
?>