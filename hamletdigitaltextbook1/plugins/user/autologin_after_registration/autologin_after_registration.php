<?php
defined( '_JEXEC' ) or die;
/**
* @version    		2.5.0
* @copyright  		Copyright (C) 2012 Akash Nand. All rights reserved.
* @license   		GNU/GPL, see LICENSE.php
* @autor      		Akash nand
* @autor-email      akashnand404@gmail.com
*
*/
jimport('joomla.event.plugin');
jimport( 'joomla.application.application' );

class plgUserAutologin_after_registration extends JPlugin
{
	public function onUserAfterSave($user, $isnew, $success, $msg)
		{
			  $mainframe	= JFactory::getApplication();
			 $app		    = JFactory::getApplication();
			 $redirectlink=$this->params->get('redirectlink'); 
			 if($redirectlink!='')
			{
				$app->redirect(JRoute::_(JURI::Root().$redirectlink));
			} 
			  	
		}	
	
}
