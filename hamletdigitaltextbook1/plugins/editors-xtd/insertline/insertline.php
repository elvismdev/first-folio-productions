<?php
/**
 * @package     Joomla.Plugin
 * @subpackage  Editors-xtd.pagebreak
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * Editor Pagebreak buton
 *
 * @package     Joomla.Plugin
 * @subpackage  Editors-xtd.pagebreak
 * @since       1.5
 */
class PlgButtonInsertline extends JPlugin
{
	/**
	 * Load the language file on instantiation.
	 *
	 * @var    boolean
	 * @since  3.1
	 */
	protected $autoloadLanguage = true;

	/**
	 * Display the button
	 *
	 * @param   string  $name  The name of the button to add
	 *
	 * @return array A two element array of (imageName, textToInsert)
	 */
	public function onDisplay($name)
	{
		JHtml::_('behavior.modal');
		
		$link = 'index.php?option=com_contentdetails&amp;view=detail&amp;layout=insertline&amp;tmpl=component&amp;e_name=' . $name;
		
		 $js = "
		function insertLinenum(lineno)
		{
			
			var tag = '<span class=\"linenum\">' + lineno + '</span>';
			
			jInsertEditorText(tag, '" . $name . "');
			SqueezeBox.close();
		}";

		$doc = JFactory::getDocument();
		$doc->addScriptDeclaration($js);
		
		$button = new JObject;
		$button->modal = true;
		$button->class = 'btn';
		$button->link  = $link;
		$button->text  = JText::_('PLG_EDITORSXTD_INSERTLINE_BUTTON_INSERTLINE');
		$button->name  = 'copy';
		$button->options = "{handler: 'iframe', size: {x: 500, y: 300}}";

		return $button;
	}
}
