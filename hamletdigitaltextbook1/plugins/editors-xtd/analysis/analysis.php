<?php
/**
 * @package     Joomla.Plugin
 * @subpackage  Editors-xtd.article
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * Editor Article buton
 *
 * @package     Joomla.Plugin
 * @subpackage  Editors-xtd.article
 * @since       1.5
 */
class PlgButtonAnalysis extends JPlugin
{
	/**
	 * Load the language file on instantiation.
	 *
	 * @var    boolean
	 * @since  3.1
	 */
	protected $autoloadLanguage = true;

	/**
	 * Display the button
	 *
	 * @param   string  $name  The name of the button to add
	 *
	 * @return array A four element array of (article_id, article_title, category_id, object)
	 */
	public function onDisplay($name)
	{
		/*
		 * Javascript to insert the link
		 * View element calls jSelectArticle when an article is clicked
		 * jSelectArticle creates the link tag, sends it to the editor,
		 * and closes the select frame.
		 */
		$document = JFactory::getDocument();
		 $getapps= & JFactory::getApplication();
		 $template_dir = JURI::root(true).'/templates/firstfolio';
		 $iconsrc=$template_dir."/images/icons/icon-analysis.png";
		 $js = "
		 
		function jSelectAnalysis(id, selected_part, details,color, object, link, lang)
		{
			
			var hreflang = '';
			if (lang !== '')
			{
				var hreflang = ' hreflang = \"' + lang + '\"';
			}
			var tag = '<span  id=\"analysis\" title=\"'+selected_part+'|'+details.split('\"').join('&quot;')+'\" style=\"color:#'+color+'\">' + selected_part + '</span><img src=".$iconsrc." class=\"analysis tooltip image-right\" title=\"'+selected_part+'|'+details.split('\"').join('&quot;')+'\" />';
			jInsertEditorText(tag, '" . $name . "');
			SqueezeBox.close();
		}
		";
			
		$doc = JFactory::getDocument();
		$doc->addScriptDeclaration($js);

		JHtml::_('behavior.modal');

		/*
		 * Use the built-in element view to select the article.
		 * Currently uses blank class.
		 */
		$link = 'index.php?option=com_contentdetails&amp;view=details&amp;layout=modal&amp;flag=analysis&amp;tmpl=component&amp;' . JSession::getFormToken() . '=1';

		$button = new JObject;
		$button->modal = true;
		$button->class = 'btn';
		$button->link = $link;
		$button->text = JText::_('PLG_ANALYSIS_BUTTON_ANALYSIS');
		$button->name = 'file-add';
		$button->options = "{handler: 'iframe', size: {x: 900, y: 550}}";

		return $button;
	}
}
