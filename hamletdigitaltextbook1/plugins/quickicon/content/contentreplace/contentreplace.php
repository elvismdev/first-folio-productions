<?php
/**
 * @package     Joomla.Plugin
 * @subpackage  Content.emailcloak
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
jimport('joomla.utilities.utility');
//jimport('simplehtmldom.simple_html_dom');

/**
 * Email cloack plugin class.
 *
 * @package     Joomla.Plugin
 * @subpackage  Content.emailcloak
 * @since       1.5
 */
class PlgContentcontentreplace extends JPlugin
{
	/**
	 * Plugin that cloaks all emails in content from spambots via Javascript.
	 *
	 * @param   string   $context  The context of the content being passed to the plugin.
	 * @param   mixed    &$row     An object with a "text" property or the string to be cloaked.
	 * @param   mixed    &$params  Additional parameters. See {@see PlgContentEmailcloak()}.
	 * @param   integer  $page     Optional page number. Unused. Defaults to zero.
	 *
	 * @return  boolean	True on success.
	 */
	public function onContentPrepare($context, &$row, &$params, $page = 0)
	{
		
		// Don't run this plugin when the content is being indexed
		if ($context == 'com_finder.indexer')
		{
			return true;
		}
		if (is_object($row))
		{
			 
			 jimport('joomla.environment.browser');
			 $browser = JBrowser::getInstance();
		     $agent = $browser->getAgentString();
			 
		
		if($browser->isMobile() || stristr($agent, 'mobile') )
		{	
			$regex = '/<span (.*?)>(.*?)<\/span>/is';
			$pattern='<span  $1 class="tooltip">$2</span>';
			$row->text=preg_replace($regex, $pattern,$row->text);
		}
		}

		return true;

	}
	function onShowhighlight($result,$params)
	{
			echo $base = JURI::base(true).'/';exit;
			
			//checking for mobile device
			 jimport('joomla.environment.browser');
			 $browser = JBrowser::getInstance();
		     $agent = $browser->getAgentString();
			if($browser->isMobile() || stristr($agent, 'mobile') )
		{	
			if($params=='defination')
			{
				$regex = '/<span id="defination" (.*?)>(.*?)<\/span>/is';
				$pattern='<span  $1 id="defination" class="tooltip highlight">$2</span>';
				$result[0]->introtext=preg_replace($regex, $pattern,$result[0]->introtext);
				return $result[0]->introtext;
			}
			elseif($params=='analysis')
			{
				$regex = '/<span id="analysis" (.*?)>(.*?)<\/span>/is';
				$pattern='<span  $1 id="analysis" class="tooltip highlight">$2</span>';
				$result[0]->introtext=preg_replace($regex, $pattern,$result[0]->introtext);
				return $result[0]->introtext;
			}
			
			else
			{
				$regex = '/<span (.*?)>(.*?)<\/span>/is';
				$pattern='<span  $1 class="tooltip highlight">$2</span>';
				$result[0]->introtext=preg_replace($regex, $pattern,$result[0]->introtext);
				return $result[0]->introtext;
			}
		}
		else
		{
			
			if($params=='defination')
			{
				$regex = '/<span id="defination" (.*?)>(.*?)<\/span>/is';
				$pattern='<span  $1 id="defination" class="highlight">$2</span>';
				$result[0]->introtext=preg_replace($regex, $pattern,$result[0]->introtext);
				$result[0]->introtext = preg_replace("/(src)=\"(?!http|ftp|https)([^\"]*)\"/", "$1=\"$base\$2\"", $result[0]->introtext);
				return $result[0]->introtext;
			}
			elseif($params=='analysis')
			{
				$regex = '/<span id="analysis" (.*?)>(.*?)<\/span>/is';
				$pattern='<span  $1 id="analysis" class="highlight">$2</span>';
				$result[0]->introtext=preg_replace($regex, $pattern,$result[0]->introtext);
				$result[0]->introtext = preg_replace("/(src)=\"(?!http|ftp|https)([^\"]*)\"/", "$1=\"$base\$2\"", $result[0]->introtext);
				return $result[0]->introtext;
			}
			else
			{
				
				$regex = '/<span (.*?)>(.*?)<\/span>/is';
				$pattern='<span  $1 class="highlight">$2</span>';
				$result[0]->introtext=preg_replace($regex, $pattern,$result[0]->introtext);
				$result[0]->introtext = preg_replace("/(src)=\"(?!http|ftp|https)([^\"]*)\"/", "$1=\"$base\$2\"", $result[0]->introtext);
				return $result[0]->introtext;
			}
		}
		
			
	}
	
	
	
}
