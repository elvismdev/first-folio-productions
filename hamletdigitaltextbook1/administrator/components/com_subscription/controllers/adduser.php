<?php
/**
 * @version     1.0.0
 * @package     com_contentdetails
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      intotality <pooja@intotalityinc.com> - http://intotalityinc.com
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controllerform');

/**
 * Detail controller class.
 */
class SubscriptionControllerAdduser extends JControllerForm
{

    function __construct() 
	{
		$this->view_list = 'transactions';
        parent::__construct();
    }

}