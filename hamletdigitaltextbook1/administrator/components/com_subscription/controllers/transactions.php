<?php
/**
 * @version     1.0.0
 * @package     com_contentdetails
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      intotality <pooja@intotalityinc.com> - http://intotalityinc.com
 */

// No direct access.
defined('_JEXEC') or die;

jimport('joomla.application.component.controlleradmin');

/**
 * Details list controller class.
 */
class SubscriptionControllerTransactions extends JControllerAdmin
{
	public function getModel($name = 'transaction', $prefix = 'SubscriptionModel')
	{
		$model = parent::getModel($name, $prefix, array('ignore_request' => true));
		return $model;
	}
    public function saveOrderAjax()
	{
		// Get the input
		$input = JFactory::getApplication()->input;
		$pks = $input->post->get('cid', array(), 'array');
		$order = $input->post->get('order', array(), 'array');

		// Sanitize the input
		JArrayHelper::toInteger($pks);
		JArrayHelper::toInteger($order);

		// Get the model
		$model = $this->getModel();

		// Save the ordering
		$return = $model->saveorder($pks, $order);

		if ($return)
		{
			echo "1";
		}

		// Close the application
		JFactory::getApplication()->close();
	}
    public function updatepaymentstatus()
	{
		$model	= $this->getModel();
		$data = array();
		$id 					= JRequest::getVar('id');
		$data['id'] 			= JRequest::getVar('id');
		$data['status']  	    = JRequest::getVar('status');
		$mail  	    			= JRequest::getVar('mail');
		//record update
		$return	= $model->update($data);
		//send mail
		if($mail == "yes")
		{
			$status	    = JRequest::getVar('status');
			$display_msg = $status;
			$data  			= $model->getinfo($id);
			$config     	= &JFactory::getConfig();
			$from           = $config->get('mailfrom');
			$fromname   	= $config->get('fromname');
			$email			= $data->email;
			$subject        =  JText::_('COM_SUBSCRIPTION_STATUS_CHANGE_SUBJECT');
			$msg 			= JText::sprintf('COM_SUBSCRIPTION_STATUS_CHANGE_MSG',$config->get('sitename'),$data->name,$display_msg,$config->get('sitename')); '';
			$mode 			= 1;
			JFactory::getMailer()->sendMail($from, $fromname, $email, $subject, $msg,$mode);
		}
		$this->setMessage(JText::_('COM_SUBSCRIPTION_STATUS_UPDATE'));
		$this->setRedirect(JRoute::_('index.php?option=com_subscription&view=transactions', false));
	}
}