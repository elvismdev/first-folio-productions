<?php
/**
 * @copyright	Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access.
defined('_JEXEC') or die;
jimport('joomla.application.component.controllerform');
/**
 * Countries list controller class.
 *
 * @package		Joomla.Administrator
 * @subpackage	com_providers
 * @since		1.6
 */
class SubscriptionControllerAjax extends JControllerForm
{
	public function getprice()
	{
		$plan = JRequest::getVar('plan');
		$db		= JFactory::getDbo();
		$query	= $db->getQuery(true);
		$query->select('a.price As value');
		$query->from($db->quoteName('#__plan_price').' AS a');
		$query->where("a.plan = '".$plan."'");
		$db->setQuery($query);
		$results = $db->loadObjectList();
		foreach($results as $key => $result)
		{
			 $cname = $result->value;
			 echo $cname;
		}
		exit();
	}
}
