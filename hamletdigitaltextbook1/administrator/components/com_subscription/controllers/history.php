<?php
/**
 * @version     1.0.0
 * @package     com_contentdetails
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      intotality <pooja@intotalityinc.com> - http://intotalityinc.com
 */

// No direct access.
defined('_JEXEC') or die;

jimport('joomla.application.component.controlleradmin');

/**
 * Details list controller class.
 */
class SubscriptionControllerHistory extends JControllerAdmin
{
	public function pageback()
	{
		$this->setRedirect(JRoute::_('index.php?option=com_subscription&view=transactions', false));
	}
}