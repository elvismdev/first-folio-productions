<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_subscription
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * Transaction table
 *
 * @package     Joomla.Administrator
 * @subpackage  com_subscription
 * @since       1.5
 */
class SubscriptionTableTransaction extends JTable
{
	/**
	 * Constructor
	 *
	 * @since   1.5
	 */
	public function __construct(&$_db)
	{
		parent::__construct('#__subscription', 'id', $_db);
		//$date = JFactory::getDate();
		//$this->created = $date->toSql();
	}
	function check()
	{   
		//If there is an ordering column and this is a new row then get the next ordering value 
		if (property_exists($this, 'ordering') && ($this->id == "" || $this->id == 0)) 
		{
			$this->ordering = self::getNextOrder();
		}
		return parent::check();
	}
	public function bind($array, $ignore = array())
	{
		return parent::bind($array, $ignore);
	}
	public function store($updateNulls = false)
	{	
			$user = JFactory::getUser();
			$oldrow = JTable::getInstance('Transaction', 'SubscriptionTable');
			if (!$oldrow->load($this->id) && $oldrow->getError())
			{
				$this->setError($oldrow->getError());
			}
			$date = JFactory::getDate();
			if($this->id == "")
			{
				$params = &JComponentHelper::getParams( 'com_subscription');
		 		$time_period = $params->get('time_period');
		 
				$this->purchasedate = $date->toSql();
				$this->expirationdate = date('Y-m-d',strtotime(date("Y-m-d") . "+ ".$time_period." month"));
				$this->licensekey = $this->getLicenseKey();
				$this->numoflicense = $_POST['jform']['plan'];
				$this->price = $_POST['jform']['price'];
				$this->createdby = $user->id;
				$this->usertype = 0;
				$this->ipaddress = $_SERVER['REMOTE_ADDR'];
				$this->modify_date = date('Y-m-d H:i:s');
			}
			else
			{
				$this->modify_date = $date->toSql();
			}
			//save data
			parent::store($updateNulls);
			//logs
			if(isset($this->id) && $this->id != "" )
			{
				//insert data
				$insertdata =new stdClass();
				$insertdata->id = null;
				$insertdata->userid = $this->userid;
				$insertdata->price = $this->price;
				$insertdata->numoflicense = $this->numoflicense;
				$insertdata->purchasedate = $this->purchasedate;
				$insertdata->expirationdate = $this->expirationdate;
				$insertdata->licensekey = $this->licensekey;
				$insertdata->createdby = $this->createdby;
				$insertdata->ipaddress = $this->ipaddress;
				$insertdata->status = $this->status;
				$insertdata->modify_date = $this->modify_date;
				$db = JFactory::getDBO();
				$db->insertObject( '#__subscription_log', $insertdata, id );
				//send subscription mail
				$config = JFactory::getConfig();
				$userdata = $this->getinfo($this->userid);
				$subject        =  JText::_('COM_PAYMENT_SUCCESSFULLY_SUBJECT');
				$msg 			= JText::sprintf('COM_PAYMENT_SUCCESSFULLY_MSG',$config->get('sitename'),$userdata->name,$insertdata->numoflicense,date('M d,Y',strtotime($insertdata->purchasedate)),date('M d,Y',strtotime($insertdata->expirationdate)),'$'.number_format($insertdata->price, 2),$config->get('sitename'));
				// Send the registration email.
				$mode 			= 1;
				$sendmail = JFactory::getMailer()->sendMail($config->get('mailfrom'), $config->get('fromname'), $userdata->email, $subject, $msg,$mode);
				
				
			}
			
			return count($this->getErrors())==0;
			
	}
	
	public function getLicenseKey()
	{
			$d=date ("d");
			$m=date ("m");
			$y=date ("Y");
			$t=time();
			$dmt=$d+$m+$y+$t;    
			$ran= rand(0,10000000);
			$dmtran= $dmt+$ran;
			$un=  uniqid();
			$dmtun = $dmt.$un;
			$mdun = md5($dmtran.$un);
			$sort=substr($mdun, 16); // if you want sort length code.
			return $mdun;
	}
	
	public function getinfo($id)
	{
		$db		= $this->getDbo();
		$query	= $db->getQuery(true);
		$query->select('a.name,a.email');
		$query->from($db->quoteName('#__users').' AS a');
		$query->where('a.id = '.$id);
		$db->setQuery($query);
		$result = $db->loadObjectList();
		return $result[0];
	}
}
