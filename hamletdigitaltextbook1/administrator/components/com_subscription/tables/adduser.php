<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_subscription
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * Adduser table
 *
 * @package     Joomla.Administrator
 * @subpackage  com_subscription
 * @since       1.5
 */
class SubscriptionTableAdduser extends JTable
{
	/**
	 * Constructor
	 *
	 * @since   1.5
	 */
	public function __construct(&$_db)
	{
		
		parent::__construct('#__users', 'id', $_db);
		//$date = JFactory::getDate();
		//$this->created = $date->toSql();
	}
	function check()
	{   
		//If there is an ordering column and this is a new row then get the next ordering value 
		if (property_exists($this, 'ordering') && ($this->id == "" || $this->id == 0)) 
		{
			$this->ordering = self::getNextOrder();
		}
		return parent::check();
	}
	public function bind($array, $ignore = array())
	{
		return parent::bind($array, $ignore);
	}
	public function store($updateNulls = false)
	{	
			echo "<pre>";
			print_r($_POST['jform']);
			die;
			$user = JFactory::getUser();
			$oldrow = JTable::getInstance('Adduser', 'SubscriptionTable');
			if (!$oldrow->load($this->id) && $oldrow->getError())
			{
				$this->setError($oldrow->getError());
			}
			return false;
			//save data
			parent::store($updateNulls);
			//logs
			return count($this->getErrors())==0;
			
	}
}
