CREATE TABLE IF NOT EXISTS `#__subscription` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) NOT NULL,
  `numoflicense` int(11) NOT NULL,
  `purchasedate` datetime NOT NULL,
  `expirationdate` datetime NOT NULL,
  `licensekey` varchar(50) NOT NULL,
  `createdby` int(11) NOT NULL,
  `ipaddress` varchar(255) NOT NULL,
  `status` enum('expired','rejected','stop','confirmed') NOT NULL,
  PRIMARY KEY (`id`)
) DEFAULT COLLATE=utf8_general_ci;


