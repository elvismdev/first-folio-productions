<?php
/**
 * @version     1.0.0
 * @package     com_subscription
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      intotality <pooja@intotalityinc.com> - http://intotalityinc.com
 */

// No direct access.
defined('_JEXEC') or die;

jimport('joomla.application.component.modeladmin');

/**
 * Subscription model.
 */
class SubscriptionModeltransaction extends JModelAdmin
{
	/**
	 * @var		string	The prefix to use with controller messages.
	 * @since	1.6
	 */
	protected $text_prefix = 'COM_SUBSCRIPTION';


	/**
	 * Returns a reference to the a Table object, always creating it.
	 *
	 * @param	type	The table type to instantiate
	 * @param	string	A prefix for the table class name. Optional.
	 * @param	array	Configuration array for model. Optional.
	 * @return	JTable	A database object
	 * @since	1.6
	 */
	public function getTable($type = 'Transaction', $prefix = 'SubscriptionTable', $config = array())
	{
		return JTable::getInstance($type, $prefix, $config);
	}

	/**
	 * Method to get the record form.
	 *
	 * @param	array	$data		An optional array of data for the form to interogate.
	 * @param	boolean	$loadData	True if the form is to load its own data (default case), false if not.
	 * @return	JForm	A JForm object on success, false on failure
	 * @since	1.6
	 */
	public function getForm($data = array(), $loadData = true)
	{
		// Initialise variables.
		$app	= JFactory::getApplication();

		// Get the form.
		$form = $this->loadForm('com_subscription.transaction', 'transaction', array('control' => 'jform', 'load_data' => $loadData));
		if (empty($form)) {
			return false;
		}

		return $form;
	}

	/**
	 * Method to get the data that should be injected in the form.
	 *
	 * @return	mixed	The data for the form.
	 * @since	1.6
	 */
	protected function loadFormData()
	{
		// Check the session for previously entered form data.
		$data = JFactory::getApplication()->getUserState('com_subscription.edit.transaction.data', array());

		if (empty($data)) 
		{
			$data = $this->getItem();
            
		}

		return $data;
	}

	/**
	 * Method to get a single record.
	 *
	 * @param	integer	The id of the primary key.
	 *
	 * @return	mixed	Object on success, false on failure.
	 * @since	1.6
	 */
	public function getItem($pk = null)
	{
		if ($item = parent::getItem($pk)) {

			//Do any procesing on fields here if needed

		}

		return $item;
	}

	/**
	 * Prepare and sanitise the table prior to saving.
	 *
	 * @since	1.6
	 */
	protected function prepareTable($table)
	{
		jimport('joomla.filter.output');

		if (empty($table->id)) {

			// Set ordering to the last item if not set
			if (@$table->ordering === '') {
				$db = JFactory::getDbo();
				$db->setQuery('SELECT MAX(ordering) FROM #__subscription');
				$max = $db->loadResult();
				$table->ordering = $max+1;
			}

		}
	}
	
	public function update($data)
	{
		$db =& JFactory::getDBO();
		$query= "UPDATE #__subscription SET status='".$data['status']."',modify_date ='".date('Y-m-d H:i:s')."'  where id=".$data['id'];
		$db->setQuery($query);
		$db->query();
		$result			= $this->getItem($data['id']);
		$insertdata =new stdClass();
		$insertdata->id = null;
		$insertdata->userid = $result->userid;
		$insertdata->numoflicense = $result->numoflicense;
		$insertdata->price = $result->price;
		$insertdata->purchasedate = $result->purchasedate;
		$insertdata->expirationdate = $result->expirationdate;
		$insertdata->licensekey = $result->licensekey;
		$insertdata->createdby = $result->createdby;
		$insertdata->ipaddress = $result->ipaddress;
		$insertdata->status = $result->status;
		$insertdata->modify_date = $result->modify_date;
		$db->insertObject( '#__subscription_log', $insertdata, id );
		//get child ids
		return true;
	}
	
	public function getChildIds($id)
	{
		$db		= $this->getDbo();
		$query	= $db->getQuery(true);
		$query->select($this->getState('list.select','a.userid'));
		$query->from($db->quoteName('#__users_license').' AS a');
		$query->where('a.parent_userid = '.$id);
		$db->setQuery($query);
		$result = $db->loadObjectList();
		return $result;
	}
	
	public function getChildSubscription($userid,$licensekey)
	{
		$db		= $this->getDbo();
		$query	= $db->getQuery(true);
		$query->select($this->getState('list.select','a.*'));
		$query->from($db->quoteName('#__subscription').' AS a');
		$query->where('a.userid = '.$userid);
		$query->where('a.licensekey = "'.$licensekey.'"');
		$db->setQuery($query);
		$result = $db->loadObjectList();
		return $result[0];
	}

	public function getinfo($id)
	{
		$db		= $this->getDbo();
		$query	= $db->getQuery(true);
		$query->select('a.name,a.email');
		$query->from($db->quoteName('#__users').' AS a,'.$db->quoteName('#__subscription').' AS s');
		$query->where('a.id = s.userid');
		$query->where('s.id = '.$id);
		$db->setQuery($query);
		$result = $db->loadObjectList();
		return $result[0];
	}
	public function getUpdateAllExpireUsers() 
	{
				$options = array();
				$link = JUri::root().'index.php?option=com_subscription&view=payment&layout=active';
				$db		= JFactory::getDbo();
				$query	= $db->getQuery(true);
				$query->select('s.*,a.name,a.email');
				$query->from('#__subscription as s,#__users as a');
				$query->where('s.userid = a.id');
				$query->where('s.status ="confirmed"');
				$query->where('date(s.expirationdate) < "'.date('Y-m-d').'"');
				// Get the options.
				$db->setQuery($query);
				$result = $db->loadObjectList();
				foreach ($result as $i => $item) :
							//update subscription
							$query= "UPDATE #__subscription SET status='expired',modify_date ='".date('Y-m-d H:i:s')."'  where id=".$item->id;
							$db->setQuery($query);
							$db->query();
							//add subscription log
							$insertdata =new stdClass();
							$insertdata->id = null;
							$insertdata->userid = $item->userid;
							$insertdata->numoflicense = $item->numoflicense;
							$insertdata->price = $item->price;
							$insertdata->purchasedate = $item->purchasedate;
							$insertdata->expirationdate = $item->expirationdate;
							$insertdata->licensekey = $item->licensekey;
							$insertdata->createdby = $item->createdby;
							$insertdata->ipaddress = $item->ipaddress;
							$insertdata->status = "expired";
							$insertdata->modify_date = date('Y-m-d H:i:s');
							$db->insertObject( '#__subscription_log', $insertdata, id );
							//send email
							$display_msg 	= "expired";
							$config     	= &JFactory::getConfig();
							$from           = $config->get('mailfrom');
							$fromname   	= $config->get('fromname');
							$email			= $item->email;
							$exdate 		= explode(" ",$item->expirationdate);
							$subject        =  JText::_('COM_SUBSCRIPTION_EXPIRE_SUBJECT');
							$msg 			= JText::sprintf('COM_SUBSCRIPTION_EXPIRE_MSG',$config->get('sitename'),$item->name,$exdate,$link,$config->get('sitename'));
							$mode 			= 1;
							JFactory::getMailer()->sendMail($from, $fromname, $email, $subject, $msg,$mode);
				endforeach;
				return "success";
				
				
	}
	
}