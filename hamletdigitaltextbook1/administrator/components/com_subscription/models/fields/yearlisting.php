<?php
/**
 * @copyright	Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('JPATH_BASE') or die;

JFormHelper::loadFieldClass('list');

require_once dirname(__FILE__) . '/../../helpers/subscription.php';

/**
 * Bannerclient Field class for the Joomla Framework.
 *
 * @package		Joomla.Administrator
 * @subpackage	com_banners
 * @since		1.6
 */
class JFormFieldYearlisting extends JFormFieldList
{
	/**
	 * The form field type.
	 *
	 * @var		string
	 * @since	1.6
	 */
	protected $type = 'Yearlisting';

	/**
	 * Method to get the field options.
	 *
	 * @return	array	The field option objects.
	 * @since	1.6
	 */
	public function getOptions()
	{
		
		return SubscriptionHelper::getYearlistingOptions();
	}
}
?>
<script type="text/javascript">
function populateYearSelect()
{
	d = new Date();
	curr_year = d.getFullYear();
	for(i = 0; i < 8; i++)
	{
		document.getElementById('jform_payment_card_expire_year').options[i] = new Option(curr_year+i,curr_year+i);
	}
	curr_month = d.getMonth();
	document.getElementById('jform_payment_card_expire_month').selectedIndex = curr_month+1;
}
//populateYearSelect();
</script>