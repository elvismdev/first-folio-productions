<?php
/**
 * @package     Hello
 * @subpackage  com_hello
 * @copyright   Copyright (C) 2012 AtomTech, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('JPATH_BASE') or die;
require_once dirname(__FILE__) . '/../../helpers/subscription.php';
JFormHelper::loadFieldClass('list');


/**
 * Displays options as a list of check boxes.
 *
 * @package     Hello
 * @subpackage  com_hello
 * @since       2.5
 */
class JFormFieldPlanprice extends JFormFieldList
{
	/**
	 * The form field type. 
	 *
	 * @var     string
	 * @since   2.5
	 */
	protected $type = 'radio';
	protected $forceMultiple = true;
	protected function getInput() 
	{ 
		$options = $this->getOptions();
		$radiolist = JHTML::_( 'select.radiolist', $options, $this->name, array( 'class' => 'required' ), 'id', 'plan',$this->id);
		return $radiolist;
	}
	protected function getOptions()
	{
		return SubscriptionHelper::getPlanpriceOptions();
	}
}
?>
