<?php
/**
 * @version     1.0.0
 * @package     com_contentdetails
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      intotality <pooja@intotalityinc.com> - http://intotalityinc.com
 */


defined('JPATH_BASE') or die;
require_once dirname(__FILE__) . '/../../helpers/subscription.php';
JFormHelper::loadFieldClass('list');

/**
 * Supports an HTML select list of categories
 */

class JFormFieldSelectparent extends JFormFieldList
{
	
	
	/**
	 * The form field type.
	 *
	 * @var		string
	 * @since	1.6
	 */
	protected $type = 'Selectparent';
	
	
	public function getOptions()
	{
		$doc = JFactory::getDocument();
	    $doc->addScript(JURI::root().'administrator/components/com_subscription/assets/js/script.js');
		
			return SubscriptionHelper::getSelectparentOptions();
	}
}