<?php

/**
 * @version     1.0.0
 * @package     com_subscription
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      intotality <pooja@intotalityinc.com> - http://intotalityinc.com
 */
defined('_JEXEC') or die;

jimport('joomla.application.component.modellist');

/**
 * Methods supporting a list of Subscription records.
 */
class SubscriptionModeluserlist extends JModelList {

    /**
     * Constructor.
     *
     * @param    array    An optional associative array of configuration settings.
     * @see        JController
     * @since    1.6
     */
    public function __construct($config = array()) {
        if (empty($config['filter_fields'])) {
            $config['filter_fields'] = array(
                'id', 'a.id',
				'name', 'a.name',
				'username', 'a.username',
				'email', 'a.email',
				'activation', 'a.activation',
			);
        }

        parent::__construct($config);
    }

    /**
     * Method to auto-populate the model state.
     *
     * Note. Calling getState in this method will result in recursion.
     */
    protected function populateState($ordering = null, $direction = null) 
	{
        // Initialise variables.
        $app = JFactory::getApplication('administrator');

        // Load the filter state.
        $search = $app->getUserStateFromRequest($this->context . '.filter.search', 'filter_search');
        $this->setState('filter.search', $search);
		
		$idsubscription = $app->getUserStateFromRequest($this->context . '.filter.idsubscription', 'filter_idsubscription');
        $this->setState('filter.idsubscription', $idsubscription);

        $published = $app->getUserStateFromRequest($this->context . '.filter.state', 'filter_published', '', 'string');
        $this->setState('filter.state', $published);

        

        // Load the parameters.
        $params = JComponentHelper::getParams('com_subscription');
        $this->setState('params', $params);

        // List state information.
        parent::populateState('a.id', 'asc');
    }

    /**
     * Method to get a store id based on model configuration state.
     *
     * This is necessary because the model is used by the component and
     * different modules that might need different sets of data or different
     * ordering requirements.
     *
     * @param	string		$id	A prefix for the store id.
     * @return	string		A store id.
     * @since	1.6
     */
    protected function getStoreId($id = '') {
        // Compile the store id.
        $id.= ':' . $this->getState('filter.search');
        $id.= ':' . $this->getState('filter.state');
		$id.= ':' . $this->getState('filter.idsubscription');
		

        return parent::getStoreId($id);
    }

    /**
     * Build an SQL query to load the list data.
     *
     * @return	JDatabaseQuery
     * @since	1.6
     */
    protected function getListQuery() 
	{
		
		$idparent = JRequest::getVar('idparent');
		// Create a new query object.
        $db = $this->getDbo();
        $query = $db->getQuery(true);

        // Select the required fields from the tables.
       $query->select(
                $this->getState(
                        'list.select', 'a.*,s.status'
                )
        );
        
		$query->from($db->quoteName('#__users_license').' As ul,'.$db->quoteName('#__users').' AS a,'.$db->quoteName('#__subscription').' AS s');
		$query->where('s.id = ul.idsubscription');
		$query->where('a.id = ul.userid');
		$query->where('ul.parent_userid ='.$idparent);
		//$query->where('a.usertype = 0');
		
		// Filter by search in title
		$search = $this->getState('filter.search');
		if (!empty($search)) 
		{
			$query->where('a.name like "%'.$search.'%"');
		}
		// Filter by search in title
		$idsubscription = $this->getState('filter.idsubscription');
		if (!empty($idsubscription)) 
		{
			$query->where('ul.idsubscription ='.$idsubscription);
		}
		// Add the list ordering clause.
        $orderCol = $this->state->get('list.ordering');
        $orderDirn = $this->state->get('list.direction');
		$query->group('ul.userid');
        if ($orderCol && $orderDirn) 
		{
            $query->order($db->escape($orderCol . ' ' . $orderDirn));
        }
		
		return $query;
    }

    public function getItems() {
        $items = parent::getItems();
        
        return $items;
    }
	
	
	public function getParentSubscriptionList($id) 
	{
		$db		= JFactory::getDbo();
		$query	= $db->getQuery(true);
		$query->select('a.*');
		$query->from($db->quoteName('#__subscription').' AS a');
		$query->where("a.userid = ".$id);
		$db->setQuery($query);
		$results = $db->loadObjectList();
		return $results;
	}

}
