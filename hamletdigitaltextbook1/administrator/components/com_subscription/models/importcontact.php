<?php

/**
 * @version     1.0.0
 * @package     com_subscription
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      intotality <pooja@intotalityinc.com> - http://intotalityinc.com
 */
defined('_JEXEC') or die;

jimport('joomla.application.component.modellist');
/**
 * Methods supporting a list of Subscription records.
 */
class SubscriptionModelimportcontact extends JModelList {

    var $_total        = null;
    var $_pagination   = null;
	
	 public function getItems() 
	{
        $items = parent::getItems();
        return $items;
    }


	
	 public function TotalRecord()
        {
               $iduser = JRequest::getVar('idparent');
			   $idsubscription = $this->getParentSubId($iduser);
			    // Create a new query object.           
                $db = JFactory::getDBO();
                $query = $db->getQuery(true);
				$query->select(
				$this->getState(
				'list.select', 'a.*,ul.idsubscription'
				)
				);
				$query->from($db->quoteName('#__users_license').' As ul,'.$db->quoteName('#__users').' AS a');
				$query->where('a.id = ul.userid');
				$query->where('ul.parent_userid ='.$iduser);
				$query->where('ul.idsubscription !='.$idsubscription);
				$query->group('ul.userid');
				$query->order('a.id ASC');
				$db->setQuery($query);
				$result = $db->loadObjectList();
				return $result;
        }
	
	public function TotalSubUserList()
	{
	   	$option = array();
	  	$iduser = JRequest::getVar('idparent');;
		$idsubscription = $this->getParentSubId($iduser);
		// Create a new query object.           
		$db = JFactory::getDBO();
		$query = $db->getQuery(true);
		$query->select(
		$this->getState(
		'list.select', 'a.id,ul.idsubscription'
		)
		);
		$query->from($db->quoteName('#__users_license').' As ul,'.$db->quoteName('#__users').' AS a');
		$query->where('a.id = ul.userid');
		$query->where('ul.parent_userid ='.$iduser);
		$query->where('ul.idsubscription = '.$idsubscription);
		$query->group('a.id');
		$query->order('a.id DESC');
		$db->setQuery($query);
		$result = $db->loadObjectList();
			foreach ($result as $i => $item) :
				$option[] =  $item->id;
			endforeach;
		return $option;
	}
	public function checkusersubscriptioncount($iduser,$usersubscription)
	{
			$db		= JFactory::getDbo();
			$query	= $db->getQuery(true);
			$query->select('s.numoflicense as plan');
			$query->from('#__subscription as s');
			$query->where('s.status = "confirmed"');
			$query->where('s.id ='.$usersubscription);
			$db->setQuery($query);
			$result = $db->loadObjectList();
			if($result[0]->plan)
			{
					
					$query2	= $db->getQuery(true);
					$query2->select('count(ul.id) as tot');
					$query2->from('#__users_license as ul');
					$query2->where('ul.parent_userid ='.$iduser);
					$query2->where('ul.idsubscription ='.$usersubscription);
					$db->setQuery($query2);
					$scount = $db->loadObjectList();
					$r_user = explode("-",$result[0]->plan);
					$tot = $r_user[1] - $scount[0]->tot;
					return $tot;
			}
		
	}
		
	public function save($iduser,$usersubscription,$data)
	{
		$db		= $this->getDbo();
		foreach ($data as $i => $item) :
				$insertdata =new stdClass();
				$insertdata->id = null;
				$insertdata->userid = $item;
				$insertdata->parent_userid = $iduser;
				$insertdata->idsubscription = $usersubscription;
				$db->insertObject( '#__users_license', $insertdata, id );
				//update user  users
				$query= 'UPDATE #__users SET block=0 where id = '.$item;
				$db->setQuery($query);
				$db->query();
		
		endforeach;
		return "success";
	}
	public function getParentSubId($id)
	{
		$db		= JFactory::getDbo();
		$query2	= $db->getQuery(true);
		$query2->select('s.id');
		$query2->from('#__subscription as s');
		$query2->where('s.userid ='.$id);
		$query2->where('s.status = "confirmed"');
		$db->setQuery($query2);
		$result = $db->loadObjectList();
		return $result[0]->id;
	}
}
