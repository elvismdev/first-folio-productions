<?php

/**
 * @version     1.0.0
 * @package     com_subscription
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      intotality <pooja@intotalityinc.com> - http://intotalityinc.com
 */
defined('_JEXEC') or die;

jimport('joomla.application.component.modellist');

/**
 * Methods supporting a list of Subscription records.
 */
class SubscriptionModeltransactions extends JModelList {

    /**
     * Constructor.
     *
     * @param    array    An optional associative array of configuration settings.
     * @see        JController
     * @since    1.6
     */
    public function __construct($config = array()) {
        if (empty($config['filter_fields'])) {
            $config['filter_fields'] = array(
                'id', 'a.id',
				'userid', 'a.userid',
				'price', 'a.price',
				'purchasedate', 'a.purchasedate',
				'expirationdate', 'a.expirationdate',
				'numoflicense', 'a.numoflicense',
				'createdby', 'a.createdby',
				'status', 'a.status',
			);
        }

        parent::__construct($config);
    }

    /**
     * Method to auto-populate the model state.
     *
     * Note. Calling getState in this method will result in recursion.
     */
    protected function populateState($ordering = null, $direction = null) 
	{
        // Initialise variables.
        $app = JFactory::getApplication('administrator');

        // Load the filter state.
        $userid = $app->getUserStateFromRequest($this->context . '.filter.userid', 'filter_userid');
        $this->setState('filter.userid', $userid);

        $status = $app->getUserStateFromRequest($this->context . '.filter.status', 'filter_status', '', 'string');
        $this->setState('filter.status', $status);

        

        // Load the parameters.
        $params = JComponentHelper::getParams('com_subscription');
        $this->setState('params', $params);

        // List state information.
        parent::populateState('a.id', 'asc');
    }

    /**
     * Method to get a store id based on model configuration state.
     *
     * This is necessary because the model is used by the component and
     * different modules that might need different sets of data or different
     * ordering requirements.
     *
     * @param	string		$id	A prefix for the store id.
     * @return	string		A store id.
     * @since	1.6
     */
    protected function getStoreId($id = '') 
	{
        // Compile the store id.
        $id.= ':' . $this->getState('filter.userid');
        $id.= ':' . $this->getState('filter.status');
		return parent::getStoreId($id);
    }

    /**
     * Build an SQL query to load the list data.
     *
     * @return	JDatabaseQuery
     * @since	1.6
     */
    protected function getListQuery() 
	{
        // Create a new query object.
        $db = $this->getDbo();
        $query = $db->getQuery(true);

        // Select the required fields from the table.
       $query->select(
                $this->getState(
                        'list.select', 'a.*,(select count(ul.id) from #__users_license ul where ul.parent_userid=a.userid and a.id = ul.idsubscription) as totaluser'
                )
        );
        $query->from('(select * from '.$db->quoteName('#__subscription').' order by id desc)as a');
		// Join over the user
		$query->select('u.name AS user_name');
		$query->join('LEFT', $db->quoteName('#__users').' AS u ON u.id = a.userid');
		// Join over the created by
		$query->select('us.name AS createdby_name');
		$query->join('LEFT', $db->quoteName('#__users').' AS us ON us.id = a.createdby');
		$query->where('a.usertype = 0');
		// Filter by userid in title
		$userid = $this->getState('filter.userid');
		if (!empty($userid)) 
		{	
			$query->where('a.userid = '.$userid);
		}
		// Filter by payment status
		$status = $this->getState('filter.status');
		if (!empty($status)) 
		{
			$query->where('a.status = "'.$status.'"');
		}
		// Add the list group by clause.
		$query->group('a.userid');
		// Add the list ordering clause.
        $orderCol = $this->state->get('list.ordering');
        $orderDirn = $this->state->get('list.direction');
        if ($orderCol && $orderDirn) 
		{
            $query->order($db->escape($orderCol . ' ' . $orderDirn));
        }
		
		return $query;
    }

    public function getItems() 
	{
        $items = parent::getItems();
        return $items;
    }

	
	
}
