<?php
/**
 * @version     1.0.0
 * @package     com_subscription
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      intotality <pooja@intotalityinc.com> - http://intotalityinc.com
 */

// No direct access.
defined('_JEXEC') or die;

jimport('joomla.application.component.modeladmin');

/**
 * Subscription model.
 */
class SubscriptionModeladduser extends JModelAdmin
{
	/**
	 * @var		string	The prefix to use with controller messages.
	 * @since	1.6
	 */
	protected $text_prefix = 'COM_SUBSCRIPTION';


	/**
	 * Returns a reference to the a Table object, always creating it.
	 *
	 * @param	type	The table type to instantiate
	 * @param	string	A prefix for the table class name. Optional.
	 * @param	array	Configuration array for model. Optional.
	 * @return	JTable	A database object
	 * @since	1.6
	 */
	public function getTable($type = 'Adduser', $prefix = 'SubscriptionTable', $config = array())
	{
		return JTable::getInstance($type, $prefix, $config);
	}

	/**
	 * Method to get the record form.
	 *
	 * @param	array	$data		An optional array of data for the form to interogate.
	 * @param	boolean	$loadData	True if the form is to load its own data (default case), false if not.
	 * @return	JForm	A JForm object on success, false on failure
	 * @since	1.6
	 */
	public function getForm($data = array(), $loadData = true)
	{
		// Initialise variables.
		$app	= JFactory::getApplication();
		// Get the form.
		$form = $this->loadForm('com_subscription.adduser', 'adduser', array('control' => 'jform', 'load_data' => $loadData));
		if (empty($form)) 
		{
			return false;
		}
		return $form;
	}

	/**
	 * Method to get the data that should be injected in the form.
	 *
	 * @return	mixed	The data for the form.
	 * @since	1.6
	 */
	protected function loadFormData()
	{
		// Check the session for previously entered form data.
		$data = JFactory::getApplication()->getUserState('com_subscription.saveform.adduser.data', array());
		if (empty($data)) 
		{
			$data = $this->getItem();
			// Get the application
			$app = JFactory::getApplication();
			$idparent = $app->input->get('idparent', 0, 'int');

			if ($idparent != 0)
			{
				$data->idparent = $idparent;
			}
			
        }
		return $data;
	}

	/**
	 * Method to get a single record.
	 *
	 * @param	integer	The id of the primary key.
	 *
	 * @return	mixed	Object on success, false on failure.
	 * @since	1.6
	 */
	public function getItem($pk = null)
	{
		if ($item = parent::getItem($pk)) 
		{
//Do any procesing on fields here if needed
		}
		return $item;
	}
	
	public function save($data)
	{
		$pk			= (!empty($data['id'])) ? $data['id'] : (int) $this->getState('user.id');
		$user		= JUser::getInstance($pk);
		$loginuser = JFactory::getUser();
		if (!$user->bind($data))
		{
			$this->setError($user->getError());
			return false;
		}
		// Store the data.
		if (!$user->save())
		{
			
			$idparent = JRequest::getVar('userid');
			//$app =& JFactory::getApplication();
			//$app->redirect('index.php?option=com_subscription&view=adduser&layout=edit&userid='.$idparent);
			//$this->setRedirect(JRoute::_('index.php?option=com_subscription&view=adduser&layout=edit&userid='.$idparent, false));
			$this->setError($user->getError());
			return false;
		}
		
		if($user->id)
		{
			$idparent = isset($_POST['idparent'])?$_POST['idparent']:571;
			$db = JFactory::getDBO();
			//save user group map
			$grpdata =new stdClass();
			$infordata->id = null;
			$grpdata->user_id = $user->id;
			$grpdata->group_id = 2;
			$db->insertObject( '#__user_usergroup_map', $grpdata, id );
			//store general information
			$infordata =new stdClass();
			$infordata->id = null;
			$infordata->userid = $user->id;
			$infordata->companyname = $data['info']['companyname'];
			$infordata->streetname = $data['info']['streetname'];
			$infordata->city = $data['info']['city'];
			$infordata->state = $data['info']['state'];
			$infordata->zipcode = $data['info']['zipcode'];
			$infordata->ipaddress = $_SERVER['REMOTE_ADDR'];
			$db->insertObject( '#__userinformation', $infordata, id );
			//get parent user data and save subscription
			$parentuserdata = $this->getParentUserData($idparent);
			//save subscription
			$subdata =new stdClass();
			$subdata->id = null;
			$subdata->userid = $user->id;
			$subdata->numoflicense = 0;
			$subdata->price = 0;
			$subdata->purchasedate = $parentuserdata->purchasedate;
			$subdata->expirationdate = $parentuserdata->expirationdate;
			$subdata->licensekey = $parentuserdata->licensekey;
			$subdata->createdby = $loginuser->id;
			$subdata->ipaddress = $parentuserdata->ipaddress;
			$subdata->status = $parentuserdata->status;
			$subdata->modify_date = date('Y-m-d H:i:s');
			$db->insertObject( '#__subscription', $subdata, id );
			//save subscription log
			$insertdata =new stdClass();
			$insertdata->id = null;
			$insertdata->userid = $user->id;
			$insertdata->numoflicense = 0;
			$insertdata->price = 0;
			$insertdata->purchasedate = $parentuserdata->purchasedate;
			$insertdata->expirationdate = $parentuserdata->expirationdate;
			$insertdata->licensekey = $parentuserdata->licensekey;
			$insertdata->createdby = $loginuser->id;
			$insertdata->ipaddress = $parentuserdata->ipaddress;
			$insertdata->status = $parentuserdata->status;
			$insertdata->modify_date = $parentuserdata->modify_date;
			$db->insertObject( '#__subscription_log', $insertdata, id );
			//save user and parent user map
			$ulicensedata =new stdClass();
			$ulicensedata->id = null;
			$ulicensedata->userid = $user->id;
			$ulicensedata->parent_userid = $idparent;
			$db->insertObject( '#__users_license', $ulicensedata, id );
				
		}
		
		$this->setState('user.id', $user->id);

		return true;
	
	}
	/**
	 * Prepare and sanitise the table prior to saving.
	 *
	 * @since	1.6
	 */
	protected function prepareTable($table)
	{
		jimport('joomla.filter.output');

		if (empty($table->id)) {

			// Set ordering to the last item if not set
			if (@$table->ordering === '') {
				$db = JFactory::getDbo();
				$db->setQuery('SELECT MAX(ordering) FROM #__users_license');
				$max = $db->loadResult();
				$table->ordering = $max+1;
			}

		}
	}
	
	public function getParentUserData($id)
	{
		$db		= JFactory::getDbo();
		$query	= $db->getQuery(true);
		$query->select('a.*');
		$query->from($db->quoteName('#__subscription').' AS a');
		$query->where("a.userid = ".$id);
		$query->where("a.status = 'confirmed'");
		$db->setQuery($query);
		$results = $db->loadObjectList();
		return $results[0];
	}
	
	protected function populateState()
	{
		parent::populateState();

		$idparent = JFactory::getApplication()->input->get('idparent', 0, 'int');
		$this->setState('adduser.idparent', $idparent);
	}

}