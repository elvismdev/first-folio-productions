<?php
/**
 * @version     1.0.0
 * @package     com_subscription
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      intotality <pooja@intotalityinc.com> - http://intotalityinc.com
 */

// No direct access
defined('_JEXEC') or die;

/**
 * Subscription helper.
 */
class SubscriptionHelper
{
	/**
	 * Configure the Linkbar.
	 */
	public static function addSubmenu($vName = '')
	{
		JHtmlSidebar::addEntry(
			JText::_('COM_SUBSCRIPTION_TITLE_TRANSACTION'),
			'index.php?option=com_subscription&view=transaction',
			$vName == 'transactions'
		);

	}

	/**
	 * Gets a list of the actions that can be performed.
	 *
	 * @return	JObject
	 * @since	1.6
	 */
	public static function getActions()
	{
		$user	= JFactory::getUser();
		$result	= new JObject;
		$assetName = 'com_subscription';
		$actions = array('core.admin', 'core.manage', 'core.create', 'core.edit', 'core.edit.own', 'core.edit.state', 'core.delete');
		foreach ($actions as $action) 
		{
			$result->set($action, $user->authorise($action, $assetName));
		}

		return $result;
	}
	
	
	public static function getUserName($id)
	{
		
		$options = array();
		$db		= JFactory::getDbo();
		$query	= $db->getQuery(true);
		$query->select('a.name');
		$query->from('#__users AS a');
		$query->where('a.id = '.$id);
		// Get the options.
		$db->setQuery($query);
		$data = $db->loadObjectList();
		return $data[0]->name;
			
	}
	
	public static function getUserlistOptions()
	{
		
		$options = array();
		$db		= JFactory::getDbo();
		$query	= $db->getQuery(true);
		$query->select('a.id,a.name');
		$query->from('#__users AS a,#__userinformation AS u');
		$query->where('a.id != 570');
		$query->where('a.id = u.userid');
		$query->where('u.usertype = 0');
		$query->order('a.id');
		// Get the options.
		$db->setQuery($query);
		$data = $db->loadObjectList();
		$options[]=JHtml::_('select.option','', JText::_('COM_SUBSCRIPTION_TITLE_TRANCATION_USERLABLE_SELECT')); 
		foreach ($data as $i => $item) :
				$options[]=JHtml::_('select.option',$item->id, $item->name); 
		endforeach;
		return $options;
			
	}
	
	public static function getSelectstateOptions()
	{
			$options = array();
			$db		= JFactory::getDbo();
			
			$query	= $db->getQuery(true);
			$query->select('a.id,a.name');
			$query->from('#__state AS a');
			$query->order('a.id');
			$db->setQuery($query);
			$data = $db->loadObjectList();
			$options[]=JHtml::_('select.option','', JText::_('COM_SUBSCRIPTION_SELECT_STATE')); 
			foreach ($data as $i => $item) :
					$options[]=JHtml::_('select.option',$item->name,$item->name); 
			endforeach;
			return $options;
	}
	
	
	public static function getSelectparentOptions()
	{
		$id = JRequest::getVar('id');
		$idparent = JRequest::getVar('idparent');	
		
		$options = array();
		$db		= JFactory::getDbo();
		$query	= $db->getQuery(true);
		if(isset($id) && $id > 0)
		{
			$query->select('a.id,a.name');
			$query->from('#__users AS a,#__userinformation AS u');
			$query->where('a.id = u.idparent');
			$query->where('u.userid ='.$id);
			$db->setQuery($query);
			$data = $db->loadObjectList();
			if ($db->getErrorNum()) 
			{
				JError::raiseWarning(500, $db->getErrorMsg());
			}
			//array_unshift($options, JHtml::_('select.option', '', JText::_('COM_SUBSCRIPTION_INFO_PARENT_SELECT')));
			$options[]=JHtml::_('select.option',$data[0]->id, $data[0]->name); 
			return $options;
		}
		elseif(isset($idparent) && $idparent > 0)
		{
			$query->select('a.id,a.name');
			$query->from('#__users AS a');
			$query->where('a.id ='.$idparent);
			$db->setQuery($query);
			$data = $db->loadObjectList();
			if ($db->getErrorNum()) 
			{
				JError::raiseWarning(500, $db->getErrorMsg());
			}
			//array_unshift($options, JHtml::_('select.option', '', JText::_('COM_SUBSCRIPTION_INFO_PARENT_SELECT')));
			$options[]=JHtml::_('select.option',$data[0]->id, $data[0]->name); 
			return $options;
		}
		else
		{
			$query->select('a.id,a.name,s.numoflicense,(select count(ul.id) from #__users_license ul where ul.parent_userid=a.id) as totaluser');
			$query->from('#__users AS a,#__userinformation AS u,#__subscription s');
			$query->where('a.id != 570');
			$query->where('a.id = u.userid');
			$query->where('a.id = s.userid');
			$query->where('s.status ="'.JText::_('COM_SUBSCRIPTION_PAY_CONFIRMED').'"'); 
			$query->where('u.usertype = 0');
			$query->where('a.block = 0');
			$query->order('a.id');
			// Get the options.
			$db->setQuery($query);
			$data = $db->loadObjectList();
			$options[]=JHtml::_('select.option','', JText::_('COM_SUBSCRIPTION_INFO_PARENT_SELECT')); 
			foreach ($data as $i => $item) :
					if($item->numoflicense != 1)
					{
						$numoflicense = explode("-",$item->numoflicense);
						if($numoflicense[1] > $item->totaluser)
						{
							$options[]=JHtml::_('select.option',$item->id, $item->name); 
						}
					}
			endforeach;
			return $options;
		}
	}
	
	public static function getTotalLicenseSubscriptionUser($idsubscription)
	{
		$db		= JFactory::getDbo();
		$query	= $db->getQuery(true);
		$query->select('count(a.id) as tot');
		$query->from('#__users_license AS a');
		$query->where('a.idsubscription ='.$idsubscription);
		//$query->group('ul.userid');
		//$query->where('ul.idsubscription ='.$idsubscription);
		$db->setQuery($query);
		$data = $db->loadObjectList();
	    return $data[0]->tot;
	}
	
	public static function getTotalCreatedUserByDate($id,$idsubscription,$purchasedate,$modify_date)
	{
		$p_date = explode(" ",$purchasedate);
		$m_date = explode(" ",$modify_date);
		$db		= JFactory::getDbo();
		$query	= $db->getQuery(true);
		$query->select('count(a.id) as tot');
		$query->from('#__users AS a,#__userinformation As ul');
		$query->where('a.id = ul.userid');
		$query->where('date(a.registerDate) BETWEEN "'.$p_date[0].'" and "'.$m_date[0].'"');
		$query->where('ul.idparent ='.$id);
		//$query->group('ul.userid');
		//$query->where('ul.idsubscription ='.$idsubscription);
		$db->setQuery($query);
		$data = $db->loadObjectList();
	    return $data[0]->tot;
	}
	
	public static function getTotalSubUser($id,$idsubscription)
	{
		// Create a new query object.           
		$db = JFactory::getDBO();
		$query = $db->getQuery(true);
		$query->select('count(ul.id) as tot');
		$query->from($db->quoteName('#__users_license').' As ul');
		$query->where('ul.parent_userid ='.$id);
		$query->where('ul.idsubscription = '.$idsubscription);
		$db->setQuery($query);
		$data = $db->loadObjectList();
		return $data[0]->tot;
	}
	public static function getSelectPlanOptions()
	{
		
		$options = array();
		$db		= JFactory::getDbo();
		$query	= $db->getQuery(true);
		//$query->select('price As value,plan As text');
		$query->select('plan As value,plan As text');
		$query->from('#__plan_price AS a');
		//$query->where('a.id != 1');
		$query->order('a.id');
		// Get the options.
		$db->setQuery($query);
		$options = $db->loadObjectList();
		// Check for a database error.
		if ($db->getErrorNum()) 
		{
			JError::raiseWarning(500, $db->getErrorMsg());
		}
		array_unshift($options, JHtml::_('select.option', '', JText::_('COM_SUBSCRIPTION_TITLE_TRANCATION_PLAN_SELECT')));
		
		return $options;
	}
	public static function getPaymentStatusOptions()
	{
		
		//$options = array();
		$options[]=JHtml::_('select.option','', JText::_('COM_SUBSCRIPTION_PAY_STATUS')); 
		$options[]=JHtml::_('select.option','confirmed',JText::_('COM_SUBSCRIPTION_PAY_CONFIRMED'));
		$options[]=JHtml::_('select.option','expired',JText::_('COM_SUBSCRIPTION_PAY_EXPIRED'));
		$options[]=JHtml::_('select.option','rejected',JText::_('COM_SUBSCRIPTION_PAY_REJECTED'));
		$options[]=JHtml::_('select.option','stop',JText::_('COM_SUBSCRIPTION_PAY_STOP'));
		return $options;
	}
	
	
	public static function getPlanpriceOptions()
	{
		$options = array();
		$db		= JFactory::getDbo();
		$query	= $db->getQuery(true);
		$query->select("id,price,plan");
		$query->from('#__plan_price');
		$query->order('id asc');
		// Get the options.
		$db->setQuery($query);
		$result = $db->loadObjectList();
		
		foreach ($result as $i => $item) :
			$item->plan = "$".($item->price)." (".$item->plan.")";
		endforeach;
		return $result;
	}
	
	
	public static function getMonthlistingOptions()
	{
		$currentmonth = date("d");
		$months = 12;
		$options[]=JHtml::_('select.option','', JText::_('COM_USER_CARD_EXPIRE_MONTH_SELECT'));
		for($i=1;$i<=$months;$i++)
		{		
				if($i > 9)
				{
				 	$options[]=JHtml::_('select.option', $i, $i);
				}
				else
				{
					$options[]=JHtml::_('select.option', "0".$i, "0".$i);
				}
		}
		return $options;
	}
	
	public static function getYearlistingOptions()
	{
		$currentyear = date("Y");
		$FromYear = 2013;
		$ToYear   = 2050;
		$options[]=JHtml::_('select.option','', JText::_('COM_USER_CARD_EXPIRE_YEAR_SELECT'));
		for($i=$FromYear;$i<=$ToYear;$i++)
		{		
			$options[]=JHtml::_('select.option', $i,$i);
		}
		return $options;
	}
	
	public static function getCardtypelistingOptions()
	{
		
		$options[]=JHtml::_('select.option','', JText::_('COM_USER_CARD_TYPE_SELECT'));
		$options[]=JHtml::_('select.option','American Express','American Express');
		//$options[]=JHtml::_('select.option','Diners Club Carte Blanche','Diners Club Carte Blanche');
		//$options[]=JHtml::_('select.option','Diners Club','Diners Club');
		$options[]=JHtml::_('select.option','Discover','Discover');
		//$options[]=JHtml::_('select.option','Diners Club Enroute','Diners Club Enroute');
		//$options[]=JHtml::_('select.option','JCB','JCB');
		//$options[]=JHtml::_('select.option','Maestro','Maestro');
		$options[]=JHtml::_('select.option','MasterCard','MasterCard');
		//$options[]=JHtml::_('select.option','Solo','Solo');
		//$options[]=JHtml::_('select.option','Switch','Switch');
		$options[]=JHtml::_('select.option','VISA','VISA');
		//$options[]=JHtml::_('select.option','VISA Electron','VISA Electron');
		//$options[]=JHtml::_('select.option','LaserCard','LaserCard');
		return $options;
	}
}
