<?php
/**
 * @version     1.0.0
 * @package     com_subscription
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      intotality <pooja@intotalityinc.com> - http://intotalityinc.com
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

/**
 * View class for a list of Subscription.
 */
class SubscriptionViewTransactions extends JViewLegacy
{
	protected $items;
	protected $pagination;
	protected $state;

	/**
	 * Display the view
	 */
	public function display($tpl = null)
	{
		 $model 			= JModelLegacy::getInstance('transaction', 'SubscriptionModel');
		 $updaterecord		= $model->getUpdateAllExpireUsers();
		 
		 $this->state		= $this->get('State');
		 $this->items		= $this->get('Items');
		 $this->pagination	= $this->get('Pagination');

		// Check for errors.
		if (count($errors = $this->get('Errors'))) {
			throw new Exception(implode("\n", $errors));
		}
        
		SubscriptionHelper::addSubmenu('transactions');
        
		$this->addToolbar();
        
        //$this->sidebar = JHtmlSidebar::render();
		parent::display($tpl);
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @since	1.6
	 */
	protected function addToolbar()
	{
		require_once JPATH_COMPONENT.'/helpers/subscription.php';

		$state	= $this->get('State');
		$canDo	= SubscriptionHelper::getActions($state->get('filter.category_id'));

		JToolBarHelper::title(JText::_('COM_SUBSCRIPTION_TITLE_TRANSACTIONS'), 'l_transactions.png');

        //Check if the form exists before showing the add/edit buttons
        $formPath = JPATH_COMPONENT_ADMINISTRATOR.'/views/transaction';
        if (file_exists($formPath)) 
		{
			JToolBarHelper::addNew('transaction.add','JTOOLBAR_NEW');
		}
		if ($canDo->get('core.admin')) 
		{
			JToolBarHelper::preferences('com_subscription');
		}
        
        //Set sidebar action - New in 3.0
		JHtmlSidebar::setAction('index.php?option=com_subscription&view=transactions');
        
        $this->extra_sidebar = '';
        
		JHtmlSidebar::addFilter(

			JText::_('JOPTION_SELECT_PUBLISHED'),

			'filter_published',

			JHtml::_('select.options', JHtml::_('jgrid.publishedOptions'), "value", "text", $this->state->get('filter.state'), true)

		);

        
	}
    
	protected function getSortFields()
	{
		return array(
		'a.id' => JText::_('JGRID_HEADING_ID'),
		'a.ordering' => JText::_('JGRID_HEADING_ORDERING'),
		'a.state' => JText::_('JSTATUS'),
		'a.checked_out' => JText::_('COM_SUBSCRIPTION_TRANCATIONS_CHECKED_OUT'),
		'a.checked_out_time' => JText::_('COM_SUBSCRIPTION_TRANCATIONS_CHECKED_OUT_TIME'),
		'a.created_by' => JText::_('COM_SUBSCRIPTION_TRANCATIONS_CREATED_BY'),
		);
	}

    
}
