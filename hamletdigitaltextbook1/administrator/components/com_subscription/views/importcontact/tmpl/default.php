<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted Access');
require_once JPATH_ADMINISTRATOR . '/components/com_subscription/helpers/subscription.php';
// load tooltip behavior
JHtml::_('behavior.tooltip');
$record = "";
?>
<form action="<?php echo JRoute::_('index.php?option=com_subscription&view=importcontact'); ?>" method="post" name="adminForm" id="adminForm">
    <h2>
    <?php echo JText::_('COM_SUBSCRIPTION_IMPORTCONTACT'); ?>
    <button type="submit" class="btn pull-right"><?php echo JText::_('COM_SUBSCRIPTION_IMPORTCONTACT_BTN'); ?></button>
    </h2> 
    <table class="table table-striped">
    <?php if(count($this->items) > 0)
    {?>
        <tr>
            <td width="1%">
          <input type="checkbox" name="checkall-toggle" value="" title="<?php echo JText::_('JGLOBAL_CHECK_ALL'); ?>" onclick="Joomla.checkAll(this)" />
          
            </td>
            <td>
            <?php echo '<b>'.JText::_('Users').'</b>'; ?>
            </td>
        </tr>
    
    <?php  
    foreach ($this->items as $i => $item) : 
    if (!in_array($item->id, $this->importedusers)) 
    {
        $record = $item->id;				
    ?>
            <tr>
                 <td class="center" width="1%">
                    <?php echo JHtml::_('grid.id', $i, $item->id); ?>
                </td>
                <td> <label for="cb<?php echo $i;?>"><?php echo $item->name?></label></td>
            </tr>
    <?php }
    endforeach; ?>
    <?php } ?>
    
    <?php if($record == ""){?>
    <tr>
        <td colspan="2" align="center">
            <?php echo JText::_('COM_SUBSCRIPTION_CONTACT_NOT_FOUND'); ?>
        </td>
    </tr>
    <?php } ?>
    
    
    </table>
    <input type="hidden" name="option" value="com_subscription" />
    <input type="hidden" name="task" value="importcontact.save" />
    <input type="hidden" name="idparent" id="idparent" value="<?php echo $this->iduser?>"/>
     <input type="hidden" name="idsubscription" id="idsubscription" value="<?php echo $this->idsubscription?>"/>
    <?php echo JHtml::_('form.token');?> 
</form>
    
