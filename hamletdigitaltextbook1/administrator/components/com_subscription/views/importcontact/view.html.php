<?php
// No direct access to this file
defined('_JEXEC') or die;
jimport('joomla.application.component.view');

class SubscriptionViewimportcontact extends JViewLegacy
{
	
	protected $state;
	protected $items;
	protected $form;
	protected $importedusers;
	protected $iduser;
	protected $idsubscription;
		
	
	function display($tpl = null) 
        {
				$impcmodel 				= JModelLegacy::getInstance('importcontact', 'SubscriptionModel');
				$this->importedusers	= $impcmodel->TotalSubUserList();
				$this->iduser 			= JRequest::getVar('idparent');
			    $this->idsubscription 	= $impcmodel->getParentSubId($this->iduser);
				$this->items			= $impcmodel->TotalRecord();
				
				if (count($errors = $this->get('Errors'))) 
				{
				throw new Exception(implode("\n", $errors));
				}
				SubscriptionHelper::addSubmenu('importcontact');
        		$this->addToolbar();
        		parent::display($tpl);
		}
	protected function addToolbar()
	{
		require_once JPATH_COMPONENT.'/helpers/subscription.php';
		JToolBarHelper::title(JText::_('COM_SUBSCRIPTION_IMPORTCONTACT'), 'l_history.png');
		$formPath = JPATH_COMPONENT_ADMINISTRATOR.'/views/importcontact';
        JHtmlSidebar::setAction('index.php?option=com_subscription&view=importcontact');
		//Check if the form exists before showing the add/edit buttons
        if (file_exists($formPath)) 
		{
			JToolBarHelper::addNew('importcontact.save',JText::_('COM_SUBSCRIPTION_IMPORTCONTACT_BTN'));
			JToolBarHelper::custom('importcontact.pageback', 'back', 'back', 'Back',false);
		}
	}
    
	
}
