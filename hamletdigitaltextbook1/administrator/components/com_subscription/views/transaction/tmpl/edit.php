<?php
/**
 * @version     1.0.0
 * @package     com_subscription
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      intotality <pooja@intotalityinc.com> - http://intotalityinc.com
 */
// no direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.keepalive');

// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet('components/com_subscription/assets/css/subscription.css');
?>
<script type="text/javascript">
    js = jQuery.noConflict();
    js(document).ready(function()
	{
		
		
    });
    
    Joomla.submitbutton = function(task)
    {
        if(task == 'transaction.cancel'){
            Joomla.submitform(task, document.getElementById('transaction-form'));
        }
        else{
            
            if (task != 'transaction.cancel' && document.formvalidator.isValid(document.id('transaction-form'))) {
                
                Joomla.submitform(task, document.getElementById('transaction-form'));
            }
            else {
                alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED')); ?>');
            }
        }
    }
</script>

<form action="<?php echo JRoute::_('index.php?option=com_subscription&layout=edit&id=' . (int) $this->item->id); ?>" method="post" enctype="multipart/form-data" name="adminForm" id="transaction-form" class="form-validate">

    <div class="row-fluid">
        <div class="span10 form-horizontal">
            
            <fieldset class="adminform">
			    
                <div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('userid'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('userid'); ?></div>
				</div>
                
                <div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('plan'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('plan'); ?></div>
				</div>
                
                <div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('price'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('price'); ?></div>
				</div>
                
                <div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('status'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('status'); ?></div>
				</div>
                
                
                    
            </fieldset>
         </div>
		<input type="hidden" name="task" value="" />
        <?php echo JHtml::_('form.token'); ?>

    </div>
   
</form>


<script type="application/javascript">
function getPrice(obj)
{ 
	
	var value = obj.value;
	
	if(value != "")
	{		
			jQuery.ajax({ url:'index.php?option=com_subscription&task=ajax.getprice&plan='+value,
			type: 'get',
			success: function(output) 
			{
				jQuery('#jform_price').val(output);
			}
			});
	}
	else
	{
		jQuery('#jform_price').val("");
	}
}
</script>