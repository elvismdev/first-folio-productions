<?php
/**
 * @version     1.0.0
 * @package     com_subscription
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      intotality <pooja@intotalityinc.com> - http://intotalityinc.com
 */

// no direct access
defined('_JEXEC') or die;
JHtml::addIncludePath(JPATH_COMPONENT.'/helpers/html');
JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('formbehavior.chosen', 'select');

// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet('components/com_subscription/assets/css/subscription.css');

$user	= JFactory::getUser();
$userId	= $user->get('id');
$listOrder	= $this->state->get('list.ordering');
$listDirn	= $this->state->get('list.direction');
$canOrder	= $user->authorise('core.edit.state', 'com_subscription');
$saveOrder	= $listOrder == 'a.ordering';
if ($saveOrder)
{
	$saveOrderingUrl = 'index.php?option=com_subscription&task=history.saveOrderAjax&tmpl=component';
	JHtml::_('sortablelist.sortable', 'detailList', 'adminForm', strtolower($listDirn), $saveOrderingUrl);
}
$sortFields = $this->getSortFields();
$idparent = JRequest::getVar('userid');

?>
<script type="text/javascript">
	Joomla.orderTable = function() {
		table = document.getElementById("sortTable");
		direction = document.getElementById("directionTable");
		order = table.options[table.selectedIndex].value;
		if (order != '<?php echo $listOrder; ?>') {
			dirn = 'asc';
		} else {
			dirn = direction.options[direction.selectedIndex].value;
		}
		Joomla.tableOrdering(order, dirn, '');
	}
</script>

<?php
//Joomla Component Creator code to allow adding non select list filters
if (!empty($this->extra_sidebar)) 
{
    $this->sidebar .= $this->extra_sidebar;
}
?>

<form action="<?php echo JRoute::_('index.php?option=com_subscription&view=history&userid='.$idparent); ?>" method="post" name="adminForm" id="adminForm">
<?php if(!empty($this->sidebar)): ?>
	<div id="j-sidebar-container" class="span2">
		<?php echo $this->sidebar; ?>
	</div>
	<div id="j-main-container" class="span10">
<?php else : ?>
	<div id="j-main-container">
<?php endif;?>
    
		<div id="filter-bar" class="btn-toolbar">
        	
            <div class="filter-search btn-group pull-left">
            <input type="hidden" name="filter_userid" id="filter_userid" value="<?php echo $idparent;?>" />
            <select name="filter_status" class="inputbox" id="filter_status">
            <?php echo JHtml::_('select.options', SubscriptionHelper::getPaymentStatusOptions(), 'value', 'text', $this->escape($this->state->get('filter.status')));?>
            </select>
            </div>
            
            <div class="btn-group pull-left">
            <button class="btn hasTooltip" type="submit" title="<?php echo JText::_('JSEARCH_FILTER_SUBMIT'); ?>"><i class="icon-search"></i></button>
            <button class="btn hasTooltip" type="button" title="<?php echo JText::_('JSEARCH_FILTER_CLEAR'); ?>" onclick="document.id('filter_userid').value='';document.id('filter_status').value='';this.form.submit();"><i class="icon-remove"></i></button>
            </div>
            
            <div class="btn-group pull-right hidden-phone">
            <label for="limit" class="element-invisible"><?php echo JText::_('JFIELD_PLG_SEARCH_SEARCHLIMIT_DESC');?></label>
            <?php echo $this->pagination->getLimitBox(); ?>
            </div>
            
            <div class="btn-group pull-right hidden-phone">
            <label for="directionTable" class="element-invisible"><?php echo JText::_('JFIELD_ORDERING_DESC');?></label>
            <select name="directionTable" id="directionTable" class="input-medium" onchange="Joomla.orderTable()">
            <option value=""><?php echo JText::_('JFIELD_ORDERING_DESC');?></option>
            <option value="asc" <?php if ($listDirn == 'asc') echo 'selected="selected"'; ?>><?php echo JText::_('JGLOBAL_ORDER_ASCENDING');?></option>
            <option value="desc" <?php if ($listDirn == 'desc') echo 'selected="selected"'; ?>><?php echo JText::_('JGLOBAL_ORDER_DESCENDING');?></option>
            </select>
            </div>
            
			
		</div>        
		<div class="clearfix"> </div>
		<table class="table table-striped" id="detailList">
			<thead>
				<tr>
               
                <th class='left'>
				<?php echo JHtml::_('grid.sort',  'COM_SUBSCRIPTION_TRANCATION_LABLE_PURCHASEDATE', 'a.purchasedate', $listDirn, $listOrder); ?>
				</th>
                <th class='left'>
				<?php echo JHtml::_('grid.sort',  'COM_SUBSCRIPTION_TITLE_MODIFY_DATE', 'a.modify_date', $listDirn, $listOrder); ?>
				</th>
                <th class='left'>
				<?php echo JHtml::_('grid.sort',  'COM_SUBSCRIPTION_TRANCATION_LABLE_EXPIREDATE', 'a.expirationdate', $listDirn, $listOrder); ?>
				</th>
                <th class='left'>
				<?php echo JHtml::_('grid.sort',  'COM_SUBSCRIPTION_TRANCATION_LABLE_NO_OF_SEAT', 'a.numoflicense', $listDirn, $listOrder); ?>
                </th>
                 <th class='left'>
                 		Created User
                 </th>
               
                
                <th class='left'>
				<?php echo JHtml::_('grid.sort',  'COM_SUBSCRIPTION_TRANCATION_LABLE_PRICE', 'a.price', $listDirn, $listOrder); ?>
                </th>
                
                <th class='left' colspan="2">
				<?php echo JHtml::_('grid.sort',  'COM_SUBSCRIPTION_TRANCATION_LABLE_PAYMENTSTATUS', 'a.status', $listDirn, $listOrder); ?>
				</th>
                
                <th class='left'>
				<?php echo JHtml::_('grid.sort',  'COM_SUBSCRIPTION_TRANSACTIONS_CREATED_BY', 'a.createdby', $listDirn, $listOrder); ?>
				</th>
                
                </tr>
			</thead>
			<tfoot>
                <?php 
                if(isset($this->items[0]))
				{
                    $colspan = count(get_object_vars($this->items[0]));
                }
                else{
                    $colspan = 10;
                }
            ?>
			<tr>
				<td colspan="<?php echo $colspan ?>">
					<?php echo $this->pagination->getListFooter(); ?>
				</td>
			</tr>
			</tfoot>
			<tbody>
			<?php foreach ($this->items as $i => $item) :
				$ordering   = ($listOrder == 'a.ordering');
                $canCreate	= $user->authorise('core.create',		'com_subscription');
                $canEdit	= $user->authorise('core.edit',			'com_subscription');
                $canCheckin	= $user->authorise('core.manage',		'com_subscription');
                $canChange	= $user->authorise('core.edit.state',	'com_subscription');
				?>
				<tr class="row<?php echo $i % 2; ?>">
                    
               	<td>
					<?php echo date('M d,Y',strtotime($item->purchasedate)); ?>
				</td> 
                 <td>
					<?php echo date('M d,Y',strtotime($item->modify_date)); ?>
				</td> 
                <td>
					<?php echo date('M d,Y',strtotime($item->expirationdate)); ?>
				</td> 
               <td>
					<?php echo $item->numoflicense; ?>
				</td>
                <td>
					<?php echo SubscriptionHelper::getTotalCreatedUserByDate($idparent,$item->id,$item->purchasedate,$item->modify_date); ?>
				</td>
                 
                 
                <td >
               		<?php echo "$".$item->price; ?>
               </td> 
                <td>
               <img src="<?php echo JURI::root();?>administrator/components/com_subscription/assets/images/<?php echo $item->status;?>.png" title="<?php echo $item->status;?>">
               </td>
               
                <td>
                	<?php echo $item->status; ?>
                </td> 
                <td>
					<?php echo $item->createdby_name; ?>
				</td>
            
            </tr>
				<?php endforeach; ?>
			</tbody>
		</table>

		<input type="hidden" name="task" value="" />
		<input type="hidden" name="boxchecked" value="0" />
		<input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>" />
		<input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>" />
		<?php echo JHtml::_('form.token'); ?>
	</div>
</form>



<script type="text/javascript">
function UpdatePaymentStatus(id)
{
	var status = jQuery('#update_payment_status_'+id).val();
	var mail = "";
	if (jQuery('#status_checkbox_'+id).attr('checked')) 
	{
       mail = "yes";
    }
	else
	{
		mail = "no";
	}
	
	var r=confirm("Are you sure you want to change payment status of user");
    if (r==true)
  	{
		  if(status != "")
		 {
		   window.location="index.php?option=com_subscription&task=history.updatepaymentstatus&id="+id+"&status="+status+"&mail="+mail;
		 }
    }
}        
</script>
		
