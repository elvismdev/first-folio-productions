<?php
defined('_JEXEC') or die;
class ReportHelper
{
	public static function addSubmenu($vName = '')
	{
		JHtmlSidebar::addEntry(
			JText::_('COM_REPORT_TITLE_STATES'),
			'index.php?option=com_report&view=states',
			$vName == 'states'
		);

	}
	
	public static function getActions()
	{
		$user	= JFactory::getUser();
		$result	= new JObject;
		$assetName = 'com_report';
		$actions = array('core.admin', 'core.manage', 'core.create', 'core.edit', 'core.edit.own', 'core.edit.state', 'core.delete');
		foreach ($actions as $action) 
		{
			$result->set($action, $user->authorise($action, $assetName));
		}

		return $result;
	}
	public static function getTimePeriodOptions()
	{
			$options = array();
			$options[]=JHtml::_('select.option','1 day',JText::_('COM_REPORT_DAILY')); 
			$options[]=JHtml::_('select.option','7 day',JText::_('COM_REPORT_WEEKLY')); 
			$options[]=JHtml::_('select.option','30 day',JText::_('COM_REPORT_MONTHLY')); 
			return $options;
	}
	public static function getActListOptions()
	{
			$options = array();
			$options[]=JHtml::_('select.option',JText::_('COM_REPORT_ACT_1'),JText::_('COM_REPORT_ACT_1')); 
			$options[]=JHtml::_('select.option',JText::_('COM_REPORT_ACT_2'),JText::_('COM_REPORT_ACT_2')); 
			$options[]=JHtml::_('select.option',JText::_('COM_REPORT_ACT_3'),JText::_('COM_REPORT_ACT_3')); 
			$options[]=JHtml::_('select.option',JText::_('COM_REPORT_ACT_4'),JText::_('COM_REPORT_ACT_4')); 
			$options[]=JHtml::_('select.option',JText::_('COM_REPORT_ACT_5'),JText::_('COM_REPORT_ACT_5')); 
			return $options;
	}
	public static function getSelectstateOptions()
	{
			$options = array();
			$db		= JFactory::getDbo();
			
			$query	= $db->getQuery(true);
			$query->select('a.id,a.name');
			$query->from('#__state AS a');
			$query->order('a.id');
			$db->setQuery($query);
			$data = $db->loadObjectList();
			foreach ($data as $i => $item) :
					$options[]=JHtml::_('select.option',$item->name,$item->name); 
			endforeach;
			return $options;
	}
	
	public static function getUserlistOptions()
	{
		
		$options = array();
		$db		= JFactory::getDbo();
		$query	= $db->getQuery(true);
		$query->select('a.id,a.name');
		$query->from('#__users AS a,#__userinformation AS u');
		$query->where('a.id != 570');
		$query->where('a.id = u.userid');
		//$query->where('u.usertype = 0');
		$query->order('a.id');
		// Get the options.
		$db->setQuery($query);
		$data = $db->loadObjectList();
		foreach ($data as $i => $item) :
				$options[]=JHtml::_('select.option',$item->id, $item->name); 
		endforeach;
		return $options;
			
	}
	
}
