<?php

/**
 * @version     1.0.0
 * @package     com_subscription
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      intotality <pooja@intotalityinc.com> - http://intotalityinc.com
 */
defined('_JEXEC') or die;

jimport('joomla.application.component.modellist');

/**
 * Methods supporting a list of Report records.
 */
class ReportModelstates extends JModelList {

    /**
     * Constructor.
     *
     * @param    array    An optional associative array of configuration settings.
     * @see        JController
     * @since    1.6
     */
    public function __construct($config = array()) {
        if (empty($config['filter_fields'])) {
            $config['filter_fields'] = array(
                'id', 'a.id',
				'user_id', 'a.user_id',
				'name', 'u.name',
				'ip', 'a.ip',
				'page', 'a.page',
				'username', 'u.username',
				'timestamp', 'a.timestamp',
				'state', 'uf.state',
				'city', 'uf.city',
				'title', 'c.title',
			);
        }

        parent::__construct($config);
    }

    /**
     * Method to auto-populate the model state.
     *
     * Note. Calling getState in this method will result in recursion.
     */
    protected function populateState($ordering = null, $direction = null) 
	{
        // Initialise variables.
        $app = JFactory::getApplication('administrator');
		
		$user_id = $app->getUserStateFromRequest($this->context . '.filter.user_id', 'filter_user_id', '', 'string');
        $this->setState('filter.user_id', $user_id);
		
		$state_id = $app->getUserStateFromRequest($this->context . '.filter.state_id', 'filter_state_id', '', 'string');
        $this->setState('filter.state_id', $state_id);
		
		$timestamp = $app->getUserStateFromRequest($this->context . '.filter.timestamp', 'filter_timestamp', '', 'string');
        $this->setState('filter.timestamp', $timestamp);
		
		$time_period = $app->getUserStateFromRequest($this->context . '.filter.time_period', 'filter_time_period', '', 'string');
        $this->setState('filter.time_period', $time_period);
		
		$act = $app->getUserStateFromRequest($this->context . '.filter.act', 'filter_act', '', 'string');
        $this->setState('filter.act', $act);
		
		
		
		// Load the parameters.
        $params = JComponentHelper::getParams('com_report');
        $this->setState('params', $params);

        // List state information.
        parent::populateState('a.id', 'desc');
    }

    /**
     * Method to get a store id based on model configuration state.
     *
     * This is necessary because the model is used by the component and
     * different modules that might need different sets of data or different
     * ordering requirements.
     *
     * @param	string		$id	A prefix for the store id.
     * @return	string		A store id.
     * @since	1.6
     */
    protected function getStoreId($id = '') 
	{
        // Compile the store id.
       // $id.= ':' . $this->getState('filter.id');
        $id.= ':' . $this->getState('filter.user_id');
		$id.= ':' . $this->getState('filter.timestamp');
		$id.= ':' . $this->getState('filter.state');
		$id.= ':' . $this->getState('filter.city');
		$id.= ':' . $this->getState('filter.name');
		$id.= ':' . $this->getState('filter.username');
		$id.= ':' . $this->getState('filter.title');
		$id.= ':' . $this->getState('filter.ip');
		$id.= ':' . $this->getState('filter.id');
		return parent::getStoreId($id);
    }

    /**
     * Build an SQL query to load the list data.
     *
     * @return	JDatabaseQuery
     * @since	1.6
     */
    protected function getListQuery() 
	{
        // Create a new query object.
        $db = $this->getDbo();
        $query = $db->getQuery(true);

        // Select the required fields from the table.
       $query->select(
               $this->getState(
                        'list.select', 'a.*'
                )
        );
        $query->from('`#__rokuserstats` as a');
		// Join over the user
		$query->select('u.name,u.username');
		$query->join('LEFT', $db->quoteName('#__users').' AS u ON u.id = a.user_id');
		// Join over the created by
		$query->select('uf.city,uf.state');
		$query->join('LEFT', $db->quoteName('#__userinformation').' AS uf ON uf.userid = a.user_id');
		// Join over the page title
		$query->select('c.title');
		$query->join('LEFT', $db->quoteName('#__content').' AS c ON c.id = a.page_id');
		//where
		$query->where('a.page_id != 0');
		// Filter by userid in title
		$user_id = $this->getState('filter.user_id');
		if (!empty($user_id)) 
		{
			$query->where('a.user_id = '.$user_id);
		}
		else
		{
			$query->where('a.user_id != 0');
		}
		// Filter by state in title
		$state_id = $this->getState('filter.state_id');
		if (!empty($state_id)) 
		{
			$query->where('uf.state = "'.$state_id.'"');
		}
		// Filter by state in title
		$timestamp = $this->getState('filter.timestamp');
		$time_period = $this->getState('filter.time_period');
		if (!empty($timestamp)) 
		{
			if (empty($time_period)) 
			{
				$query->where('date(a.timestamp) = "'.$timestamp.'"');
			}
		}
		
		if (!empty($time_period)) 
		{
			if (!empty($timestamp)) 
			{	
				if($time_period == "1 day")
				{
					$date = $timestamp; // 7 days ago
					$query->where('date(a.timestamp) = "'.$date.'"');
				}
				else if($time_period == "7 day")
				{
					$date = date('Y-m-d',strtotime($timestamp)-(7*86400)); // 7 days ago
					$query->where('date(a.timestamp) > "'.$date.'"');
				}
				else
				{
					$date = date('Y-m-d',strtotime($timestamp)-(30*86400)); // 7 days ago
					$query->where('date(a.timestamp) > "'.$date.'"');
				}
			}
			else
			{
				if($time_period == "1 day")
				{
					$date = date('Y-m-d'); // 7 days ago
					$query->where('date(a.timestamp) = "'.$date.'"');
				}
				else if($time_period == "7 day")
				{
					$date = date('Y-m-d',time()-(7*86400)); // 7 days ago
					$query->where('date(a.timestamp) > "'.$date.'"');
				}
				else
				{
					$date = date('Y-m-d',time()-(30*86400)); // 7 days ago
					$query->where('date(a.timestamp) > "'.$date.'"');
				}
			}
		}
		// Filter by act
		$act = $this->getState('filter.act');
		if (!empty($act)) 
		{
			$query->where('c.title like "%'.$act.'%"');
		}
		// Add the list ordering clause.
        $orderCol = $this->state->get('list.ordering');
        $orderDirn = $this->state->get('list.direction');
        if ($orderCol && $orderDirn) 
		{
            $query->order($db->escape($orderCol . ' ' . $orderDirn));
        }
		return $query;
    }

    public function getItems() 
	{
        $items = parent::getItems();
        return $items;
    }

	
	
}
