<?php
/**
 * @version     1.0.0
 * @package     com_subscription
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      intotality <pooja@intotalityinc.com> - http://intotalityinc.com
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

/**
 * View class for a list of Report.
 */
class ReportViewStates extends JViewLegacy
{
	protected $items;
	protected $pagination;
	protected $state;

	/**
	 * Display the view
	 */
	public function display($tpl = null)
	{
		 $this->state		= $this->get('State');
		 $this->items		= $this->get('Items');
		 $this->pagination	= $this->get('Pagination');
		
		// Check for errors.
		if (count($errors = $this->get('Errors'))) {
			throw new Exception(implode("\n", $errors));
		}
        
		ReportHelper::addSubmenu('states');
        
		$this->addToolbar();
        $this->sidebar = JHtmlSidebar::render();
		parent::display($tpl);
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @since	1.6
	 */
	protected function addToolbar()
	{
		require_once JPATH_COMPONENT.'/helpers/report.php';
		$state	= $this->get('State');
		JToolBarHelper::title(JText::_('COM_REPORT_TITLE_STATES'), 'l_states.png');
       //Set sidebar action - New in 3.0
		JHtmlSidebar::setAction('index.php?option=com_subscription&view=states');
        
        $this->extra_sidebar = '';
        
		JHtmlSidebar::addFilter(

			JText::_('COM_REPORT_USERLABLE_SELECT'),

			'filter_user_id',

			JHtml::_('select.options', ReportHelper::getUserlistOptions(), 'value', 'text', $this->escape($this->state->get('filter.user_id')), true)

		);
		
		JHtmlSidebar::addFilter(

			JText::_('COM_REPORT_SELECT_STATE'),

			'filter_state_id',

			JHtml::_('select.options', ReportHelper::getSelectstateOptions(), 'value', 'text', $this->escape($this->state->get('filter.state_id')), true)

		);
		
		JHtmlSidebar::addFilter(

			JText::_('COM_REPORT_TIME_PERIOD'),

			'filter_time_period',

			JHtml::_('select.options', ReportHelper::getTimePeriodOptions(), 'value', 'text', $this->escape($this->state->get('filter.time_period')), true)

		);
		
		JHtmlSidebar::addFilter(

			JText::_('COM_REPORT_ACT_SELECT'),

			'filter_act',

			JHtml::_('select.options', ReportHelper::getActListOptions(), 'value', 'text', $this->escape($this->state->get('filter.act')), true)

		);
		
		
        

        
	}
    
	protected function getSortFields()
	{
		return array(
		'a.id' => JText::_('JGRID_HEADING_ID'),
		'a.ordering' => JText::_('JGRID_HEADING_ORDERING'),
		'a.state' => JText::_('JSTATUS'),
		'a.checked_out' => JText::_('COM_REPORT_TRANCATIONS_CHECKED_OUT'),
		'a.checked_out_time' => JText::_('COM_REPORT_TRANCATIONS_CHECKED_OUT_TIME'),
		'a.created_by' => JText::_('COM_REPORT_TRANCATIONS_CREATED_BY'),
		);
	}

    
}
