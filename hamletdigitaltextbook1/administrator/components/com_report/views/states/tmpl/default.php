<?php
defined('_JEXEC') or die;
JHtml::addIncludePath(JPATH_COMPONENT.'/helpers/html');
JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('formbehavior.chosen', 'select');

// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet('components/com_report/assets/css/report.css');

$user	= JFactory::getUser();
$userId	= $user->get('id');
$listOrder	= $this->state->get('list.ordering');
$listDirn	= $this->state->get('list.direction');
$canOrder	= $user->authorise('core.edit.state', 'com_report');
$saveOrder	= $listOrder == 'a.ordering';
if ($saveOrder)
{
	$saveOrderingUrl = 'index.php?option=com_report&task=states.saveOrderAjax&tmpl=component';
	JHtml::_('sortablelist.sortable', 'detailList', 'adminForm', strtolower($listDirn), $saveOrderingUrl);
}
$sortFields = $this->getSortFields();
?>
<script type="text/javascript">
	Joomla.orderTable = function() {
		table = document.getElementById("sortTable");
		direction = document.getElementById("directionTable");
		order = table.options[table.selectedIndex].value;
		if (order != '<?php echo $listOrder; ?>') {
			dirn = 'asc';
		} else {
			dirn = direction.options[direction.selectedIndex].value;
		}
		Joomla.tableOrdering(order, dirn, '');
	}
</script>

<?php
//Joomla Component Creator code to allow adding non select list filters
if (!empty($this->extra_sidebar)) 
{
    $this->sidebar .= $this->extra_sidebar;
}
$startCal  = JHtml::_('calendar',$this->escape($this->state->get('filter.timestamp')), 'filter_timestamp', 'filter_timestamp'); 
?>

<form action="<?php echo JRoute::_('index.php?option=com_report&view=states'); ?>" method="post" name="adminForm" id="adminForm">
	<?php if (!empty( $this->sidebar)) : ?>
		<div id="j-sidebar-container" class="span2">
		<?php echo $this->sidebar; ?>
		</div>
		<div id="j-main-container" class="span10">
	<?php else : ?>
		<div id="j-main-container">
	<?php endif;?>
	<div id="filter-bar" class="btn-toolbar">
    		
            <div class="filter-search btn-group pull-left">
            	<?php echo $startCal;?>
            </div>
            <div class="btn-group pull-left">
            <button class="btn hasTooltip" type="submit" title="<?php echo JText::_('JSEARCH_FILTER_SUBMIT'); ?>"><i class="icon-search"></i></button>
            <button class="btn hasTooltip" type="button" title="<?php echo JText::_('JSEARCH_FILTER_CLEAR'); ?>" onclick="document.id('filter_user_id').value='';document.id('filter_state_id').value='';document.id('filter_timestamp').value='';document.id('filter_time_period').value='';document.id('filter_act').value='';this.form.submit();"><i class="icon-remove"></i></button>
            </div>
            
            <div class="btn-group pull-right hidden-phone">
            <label for="limit" class="element-invisible"><?php echo JText::_('JFIELD_PLG_SEARCH_SEARCHLIMIT_DESC');?></label>
            <?php echo $this->pagination->getLimitBox(); ?>
            </div>
        </div>        
		<div class="clearfix"> </div>
		<table class="table table-striped" id="detailList">
			<thead>
				<tr>
                <?php if (isset($this->items[0]->ordering)): ?>
					<th width="1%" class="nowrap center hidden-phone">
						<?php echo JHtml::_('grid.sort', '<i class="icon-menu-2"></i>', 'a.ordering', $listDirn, $listOrder, null, 'asc', 'JGRID_HEADING_ORDERING'); ?>
					</th>
                <?php endif; ?>
					<th width="1%">
						<input type="checkbox" name="checkall-toggle" value="" title="<?php echo JText::_('JGLOBAL_CHECK_ALL'); ?>" onclick="Joomla.checkAll(this)" />
					</th>
                
                <th width="15%" class="nowrap left">
				<?php echo JHtml::_('grid.sort',  'COM_REPORT_USER_NAME_LABLE', 'u.name', $listDirn, $listOrder); ?>
				</th>
                
                <th width="15%" class="nowrap left">
				<?php echo JHtml::_('grid.sort',  'COM_REPORT_PAGE_LABLE', 'c.title', $listDirn, $listOrder); ?>
				</th>
                
                <th width="10%" class="nowrap left">
				<?php echo JHtml::_('grid.sort',  'COM_REPORT_STATE_LABLE', 'uf.state', $listDirn, $listOrder); ?>
				</th>
                
                <th width="10%" class="nowrap left">
				<?php echo JHtml::_('grid.sort',  'COM_REPORT_CITY_LABLE', 'uf.city', $listDirn, $listOrder); ?>
				</th>
                
                <th width="17%" class="nowrap left">
				<?php echo JHtml::_('grid.sort',  'COM_REPORT_DATE_LABLE', 'a.timestamp', $listDirn, $listOrder); ?>
				</th>
                </tr>
			</thead>
			<tfoot>
                <?php 
                if(isset($this->items[0]))
				{
                    $colspan = count(get_object_vars($this->items[0]));
                }
                else{
                    $colspan = 8;
                }
            ?>
			<tr>
				<td colspan="<?php echo $colspan ?>">
					<?php echo $this->pagination->getListFooter(); ?>
				</td>
			</tr>
			</tfoot>
			<tbody>
			<?php foreach ($this->items as $i => $item) :
				$ordering   = ($listOrder == 'a.ordering');
                $canCreate	= $user->authorise('core.create',		'com_report');
                $canEdit	= $user->authorise('core.edit',			'com_report');
                $canCheckin	= $user->authorise('core.manage',		'com_report');
                $canChange	= $user->authorise('core.edit.state',	'com_report');
				?>
					<tr class="row<?php echo $i % 2; ?>">
                    	<?php if (isset($this->items[0]->ordering)): ?>
                            <td class="order nowrap center hidden-phone">
                            <?php if ($canChange) :
                                $disableClassName = '';
                                $disabledLabel	  = '';
                                if (!$saveOrder) :
                                    $disabledLabel    = JText::_('JORDERINGDISABLED');
                                    $disableClassName = 'inactive tip-top';
                                endif; ?>
                                <span class="sortable-handler hasTooltip <?php echo $disableClassName?>" title="<?php echo $disabledLabel?>">
                                    <i class="icon-menu"></i>
                                </span>
                                <input type="text" style="display:none" name="order[]" size="5" value="<?php echo $item->ordering;?>" class="width-20 text-area-order " />
                            <?php else : ?>
                                <span class="sortable-handler inactive" >
                                    <i class="icon-menu"></i>
                                </span>
                            <?php endif; ?>
                            </td>
                        <?php endif; ?>
                        <td class="center"><?php echo JHtml::_('grid.id', $i, $item->id); ?></td>
                        <td ><?php echo $item->name; ?></td>
                        <td ><?php echo $item->title; ?></td>
                        <td ><?php echo $item->state; ?></td>
                        <td ><?php echo $item->city; ?></td>
                    	<td ><?php echo date('M d, Y g:i a',strtotime($item->timestamp)); ?></td>
                      </tr>
				<?php endforeach; ?>
			</tbody>
		</table>

		<input type="hidden" name="task" value="" />
		<input type="hidden" name="boxchecked" value="0" />
		<input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>" />
		<input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>" />
		<?php echo JHtml::_('form.token'); ?>
	</div>
</form>




		
