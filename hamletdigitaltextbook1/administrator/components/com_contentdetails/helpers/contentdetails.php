<?php
/**
 * @version     1.0.0
 * @package     com_contentdetails
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      intotality <pooja@intotalityinc.com> - http://intotalityinc.com
 */

// No direct access
defined('_JEXEC') or die;

/**
 * Contentdetails helper.
 */
class ContentdetailsHelper
{
	/**
	 * Configure the Linkbar.
	 */
	public static function addSubmenu($vName = '')
	{
		JHtmlSidebar::addEntry(
			JText::_('COM_CONTENTDETAILS_TITLE_DETAILS'),
			'index.php?option=com_contentdetails&view=details',
			$vName == 'details'
		);

	}

	/**
	 * Gets a list of the actions that can be performed.
	 *
	 * @return	JObject
	 * @since	1.6
	 */
	public static function getActions()
	{
		$user	= JFactory::getUser();
		$result	= new JObject;

		$assetName = 'com_contentdetails';

		$actions = array(
			'core.admin', 'core.manage', 'core.create', 'core.edit', 'core.edit.own', 'core.edit.state', 'core.delete'
		);

		foreach ($actions as $action) {
			$result->set($action, $user->authorise($action, $assetName));
		}

		return $result;
	}
}
