<?php
/**
 * @version     1.0.0
 * @package     com_contentdetails
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      intotality <pooja@intotalityinc.com> - http://intotalityinc.com
 */
// no direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.keepalive');

// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet('components/com_contentdetails/assets/css/contentdetails.css');
?>
<script type="text/javascript">
    js = jQuery.noConflict();
    js(document).ready(function(){
		
		
    });
    
    Joomla.submitbutton = function(task)
    {
        if(task == 'detail.cancel'){
            Joomla.submitform(task, document.getElementById('detail-form'));
        }
        else{
            
            if (task != 'detail.cancel' && document.formvalidator.isValid(document.id('detail-form'))) {
                
                Joomla.submitform(task, document.getElementById('detail-form'));
            }
            else {
                alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED')); ?>');
            }
        }
    }
</script>

<form action="<?php echo JRoute::_('index.php?option=com_contentdetails&layout=edit&id=' . (int) $this->item->id); ?>" method="post" enctype="multipart/form-data" name="adminForm" id="detail-form" class="form-validate">

    <div class="row-fluid">
        <div class="span10 form-horizontal">
            <fieldset class="adminform">
			   <!-- <div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('id'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('id'); ?></div>
			</div>-->
            <div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('type'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('type'); ?></div>
			</div>
            <div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('selected_part'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('selected_part'); ?></div>
			</div>
            
             <div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('defination'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('defination'); ?></div>
			</div>
            <div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('analysis'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('analysis'); ?></div>
			</div>
            
            
             <div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('color'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('color'); ?></div>
			</div>
            
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('state'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('state'); ?></div>
			</div>
			<!--<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('created_by'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('created_by'); ?></div>
			</div>-->
            </fieldset>
        </div>

        

        <input type="hidden" name="task" value="" />
        <?php echo JHtml::_('form.token'); ?>

    </div>
   
</form>