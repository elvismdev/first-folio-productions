<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_content
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/*$script  = 'function insertLinenum() {'."\n\t";
// Get the Line #echo $this->eName;
$script .= 'var lineno = document.getElementById("lineno").value;'."\n\t";
//$script .= 'if (title != \'\') {'."\n\t\t";
//$script .= 'lineno = "lineno=\""+lineno+"\" ";'."\n\t";
//$script .= '}'."\n\t";
$script .= 'var tag = "<span class=\"linenum\" >"+ lineno + "</span>";'."\n\t";
$script .= 'window.parent.jInsertEditorText(tag, \''.$this->eName.'\');'."\n\t";
$script .= 'window.parent.SqueezeBox.close();'."\n\t";
$script .= 'return false;'."\n";
$script .= '}'."\n";

JFactory::getDocument()->addScriptDeclaration($script);*/
$app = JFactory::getApplication();
$function  = $app->input->getCmd('function', 'insertLinenum');

?>

		<form>
		<table width="100%" align="center">
			<tr width="40%">
				<td class="key" align="right">
					<label for="lineno">
						<?php echo JText::_('COM_CONTENTDETAILS_INSERTLINE_NUM'); ?>
					</label>
				</td>
				<td>
					<input type="text" id="lineno" name="lineno" />
				</td>
			</tr>
			</table>
		</form>
		<button onclick="if (window.parent) window.parent.<?php echo $this->escape($function);?>(document.getElementById('lineno').value);"><?php echo JText::_('COM_CONTENTDETAILS_INSERTLINE_INSERT_BUTTON'); ?></button>
