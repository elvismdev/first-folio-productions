<?php
/**
 * @version     1.0.0
 * @package     com_contentdetails
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      intotality <pooja@intotalityinc.com> - http://intotalityinc.com
 */
// no direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.keepalive');

// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet('components/com_contentdetails/assets/css/contentdetails.css');
$flag= JRequest::getVar('flag');
//echo $this->flag;

?>
<script type="text/javascript">
    js = jQuery.noConflict();
    js(document).ready(function(){
		
		
    });
    
    Joomla.submitbutton = function(task)
    {
		
        if(task == 'detail.cancel'){
            Joomla.submitform(task, document.getElementById('modal-form'));
        }
        else{
            
            if (task != 'detail.cancel' && document.formvalidator.isValid(document.id('modal-form'))) {
                
                Joomla.submitform(task, document.getElementById('modal-form'));
            }
            else {
                alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED')); ?>');
            }
        }
    }
</script>
 
<div class="popupform">    
		
<form action="<?php echo JRoute::_('index.php?option=com_contentdetails&layout=edit'); ?>" method="post" enctype="multipart/form-data" name="modalForm" id="modal-form" class="form-validate">

 
 <h2><?php echo JText::_('Create').'&nbsp;'.ucfirst($flag);?> <button name="save" class="btn float-right" type="submit"><?php echo JText::_('Save')?></button></h2> 
 
 
  <hr class="hr-condensed" />     
    <div class="row-fluid">
        <div class="span10 form-horizontal">
            <fieldset class="adminform">
			  <!--  <div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('id'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('id'); ?></div>
			</div>-->
          
             <div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('type'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('type'); ?>
                </div>
              
			</div>
            <div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('selected_part'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('selected_part'); ?></div>
			</div>
            <?php if($flag=="defination"){?>
             <div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('defination'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('defination'); ?></div>
			</div>
            <?php }else{?>
            <div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('analysis'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('analysis'); ?></div>
			</div>
            <?php }?>
            
             <div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('color'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('color'); ?></div>
			</div>
            
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('state'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('state'); ?></div>
			</div>
			<!--<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('created_by'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('created_by'); ?></div>
			</div>-->
          <!--  <div class="control-group">
            <button name="save" class="btn" type="submit"><?php echo JText::_('Save')?></button>
            </div>-->
            </fieldset>
        </div>

        
		 <input type="hidden" name="option" value="com_contentdetails" />
        <input type="hidden" name="task" value="detail.savedetails" />
         <input type="hidden" name="flag" value="<?php echo $flag;?>" />
        <?php echo JHtml::_('form.token'); ?>

    </div>
   
</form>
</div>