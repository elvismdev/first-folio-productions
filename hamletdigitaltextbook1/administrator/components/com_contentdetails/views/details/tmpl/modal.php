<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_content
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

$app = JFactory::getApplication();
$flag= JRequest::getVar('flag'); 
if ($app->isSite())
{
	JSession::checkToken('get') or die(JText::_('JINVALID_TOKEN'));
}

//require_once JPATH_ROOT . '/components/com_content/helpers/route.php';

JHtml::addIncludePath(JPATH_COMPONENT.'/helpers/html');
JHtml::_('bootstrap.tooltip');

if($flag=="defination"){
	$function  = $app->input->getCmd('function', 'jSelectDefination');
}
else
{
	$function  = $app->input->getCmd('function', 'jSelectAnalysis');
}
$listOrder = $this->escape($this->state->get('list.ordering'));
$listDirn  = $this->escape($this->state->get('list.direction'));

?>
<form action="index.php?option=com_contentdetails&view=detail" method="post" name="addDetail" id="addDetail">

<input type="hidden" name="option" value="com_contentdetails"/>
<input type="hidden" name="view" value="detail" />
<input type="hidden" name="layout" value="modal" />
<input type="hidden" name="tmpl" value="component" />
<input type="hidden" name="flag" value="<?php echo $flag?>"/>
 <?php echo JHtml::_('form.token'); ?>
</form>
<div class="popupform"> 
<form action="<?php echo JRoute::_('index.php?option=com_contentdetails&view=details&layout=modal&tmpl=component&function='.$function.'&'.JSession::getFormToken().'=1');?>" method="post" name="adminForm" id="adminForm" class="form-inline">
	
<fieldset class="filter clearfix">
		<div class="btn-toolbar">
			<div class="btn-group pull-left">
				<label for="filter_search">
					<?php echo JText::_('JSEARCH_FILTER_LABEL'); ?>
				</label>
			</div>
			<div class="btn-group pull-left">
				<input type="text" name="filter_search" id="filter_search" placeholder="<?php echo JText::_('JSEARCH_FILTER'); ?>" value="<?php echo $this->escape($this->state->get('filter.search')); ?>" title="<?php echo JText::_('JSEARCH_FILTER'); ?>" />
                
			</div>
			<div class="btn-group pull-left">
				<button type="submit" class="btn hasTooltip" title="<?php echo JHtml::tooltipText('JSEARCH_FILTER_SUBMIT'); ?>" data-placement="bottom">
                <span class="icon-search"></span><?php echo '&#160;' . JText::_('JSEARCH_FILTER_SUBMIT'); ?></button>
				<button type="button" class="btn hasTooltip" title="<?php echo JHtml::tooltipText('JSEARCH_FILTER_CLEAR'); ?>" data-placement="bottom" onclick="document.getElementById('filter_search').value='';this.form.submit();">
					<span class="icon-remove"></span><?php echo '&#160;' . JText::_('JSEARCH_FILTER_CLEAR'); ?></button>
                    
                    
                     
			</div>
             
			
            <div class="btn-group pull-left"><button type="button" name="add" class="btn" onclick="document.getElementById('addDetail').submit();"><?php echo JText::_('Add')?></button></div>
          <div class="clearfix"></div> 
		</div>
		<hr class="hr-condensed" />
		
	</fieldset>
	<table class="table table-striped">
		<thead>
			<tr>
				<th  class="center nowrap" width="20%">
					<?php echo JHtml::_('grid.sort', 'COM_CONTENTDETAILS_DETAILS_SELECTED_PART', 'a.selected_part', $listDirn, $listOrder); ?>
				</th>
               
				<th  class="center nowrap" width="25%">
					<?php echo JHtml::_('grid.sort', 'COM_CONTENTDETAILS_DETAILS_DETAILS', 'a.defination', $listDirn, $listOrder); ?>
				</th>
               	<th  class="center nowrap" width="15%">
					<?php echo JText::_('COM_CONTENTDETAILS_DETAILS_COLOR'); ?>
				</th>
				
				
			</tr>
		</thead>
		<tfoot>
			<tr>
				<td colspan="15">
					<?php echo $this->pagination->getListFooter(); ?>
				</td>
			</tr>
		</tfoot>
		<tbody>
       	<?php 
			foreach ($this->details as $i => $item) : ?>
			<?php if ($item->language && JLanguageMultilang::isEnabled())
			{
				$tag = strlen($item->language);
				if ($tag == 5)
				{
					$lang = substr($item->language, 0, 2);
				}
				elseif ($tag == 6)
				{
					$lang = substr($item->language, 0, 3);
				}
				else {
					$lang = "";
				}
			}
			elseif (!JLanguageMultilang::isEnabled())
			{
				$lang = "";
			}
			
			?>
			<tr class="row<?php echo $i % 2; ?>">
                <td class="center">
                <?php if($flag=='defination')
			{?>
                <a href="javascript:void(0)" onclick="if (window.parent) window.parent.<?php echo $this->escape($function);?>('<?php echo $item->id; ?>', '<?php echo $this->escape(addslashes($item->selected_part)); ?>', '<?php echo $this->escape(addslashes($item->defination)); ?>', '<?php echo $this->escape($item->color); ?>', '#', '<?php echo $this->escape($lang); ?>', null);">
            <?php }else{ ?>
             <a href="javascript:void(0)" onclick="if (window.parent) window.parent.<?php echo $this->escape($function);?>('<?php echo $item->id; ?>', '<?php echo $this->escape(addslashes($item->selected_part)); ?>', '<?php echo $this->escape(addslashes($item->analysis)); ?>', '<?php echo $this->escape($item->color); ?>', '#', '<?php echo $this->escape($lang); ?>', null);">
            <?php }?>   
					<?php echo $this->escape($item->selected_part); ?>
                   </a> 
				</td>
                <td class="center" width="20%">
				
					 <?php 
					 
					 if($item->type=='defination')
					 echo $item->defination;
					 if($item->type=='analysis')
					 echo $item->analysis; ?>
				</td>
                         
				
				<td class="center" style="background-color:#<?php echo $this->escape($item->color); ?>">
					<?php echo $this->escape($item->color); ?>
				</td>
				
				</tr>
			<?php endforeach; ?>
		</tbody>
	</table>

	<div>
		<input type="hidden" name="task" value="" />
		<input type="hidden" name="boxchecked" value="0" />
		<input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>" />
		<input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>" />
        <input type="hidden" name="flag" value="<?php echo $flag;?>" />
		<?php echo JHtml::_('form.token'); ?>
	</div>
</form>
</div>