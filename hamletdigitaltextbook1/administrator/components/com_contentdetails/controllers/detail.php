<?php
/**
 * @version     1.0.0
 * @package     com_contentdetails
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      intotality <pooja@intotalityinc.com> - http://intotalityinc.com
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controllerform');

/**
 * Detail controller class.
 */
class ContentdetailsControllerDetail extends JControllerForm
{

    function __construct() {
        $this->view_list = 'details';
        parent::__construct();
    }
	protected function allowAdd($data = array())
	{
		$flag=JRequest::getVar('flag');
		$user = JFactory::getUser();
		$allow = $user->authorise('core.create', 'com_contentdetails.detail' . $flag);
		//print_r($allow);exit;
		return $allow;
	}
	function savedetails()
	{
		$app =& JFactory::getApplication();
		$post = JRequest::get('post');
		$row  = $post['jform'];
		$db = JFactory::getDBO();
		$data = new stdClass();
			$data->id = null;
			$data->type = $post['jform']['type'];
			$data->selected_part = $post['jform']['selected_part'];
			$data->defination = $post['jform']['defination'];
			$data->analysis = $post['jform']['analysis'];
			$data->color = $post['jform']['color'];
			$data->	state =$post['jform']['state'];
			
			if($db->insertObject( '#__content_details', $data, id ))
			{
							
					$app->redirect('index.php?option=com_contentdetails&view=details&layout=modal&tmpl=component&flag='.$data->type);	
				
			}
	}


}