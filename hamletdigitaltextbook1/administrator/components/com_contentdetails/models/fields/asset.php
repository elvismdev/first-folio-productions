<?php
/**
* Asset Element
* @package Gems of Joomla Showcase
* @Copyright (C) 2009-2011 Gavick.com
* @ All rights reserved
* @ Joomla! is Free Software
* @ Released under GNU/GPL License : http://www.gnu.org/copyleft/gpl.html
**/
defined('JPATH_BASE') or die;
jimport('joomla.form.formfield');

class JFormFieldAsset extends JFormField {
	
    protected $type = 'Asset';
	protected function getInput() { 
	    $flag= JRequest::getVar('flag');
		$doc = JFactory::getDocument();
	    $doc->addScript(JURI::root().'administrator/components/com_contentdetails/assets/js/script.js');
		//'<option value="none">'.JText::_('Select Type').'</option>'.
		/*$html[]='<select id="'.$this->id.'" name="'.$this->name.'">'.
				'<option value="defination">'.JText::_('COM_CONTENTDETAILS_TYPE_DEFINATION').'</option>'.
		        '<option value="analysis">'.JText::_('COM_CONTENTDETAILS_TYPE_ANALYSIS').'</option>'.
				'<option value="quotation">'.JText::_('COM_CONTENTDETAILS_TYPE_QUOTATION').'</option>'.
 				'</select>';*/
		if($flag=='defination')
		{
			$options[] = JHTML::_('select.option', "defination",JText::_('COM_CONTENTDETAILS_TYPE_DEFINATION'));
		}
		else if($flag=='analysis')
		{
			$options[] = JHTML::_('select.option', "analysis",JText::_('COM_CONTENTDETAILS_TYPE_ANALYSIS'));
		}
		else
		{
			$options[] = JHTML::_('select.option', "defination",JText::_('COM_CONTENTDETAILS_TYPE_DEFINATION'));
			$options[] = JHTML::_('select.option', "analysis",JText::_('COM_CONTENTDETAILS_TYPE_ANALYSIS'));
			//$options[] = JHTML::_('select.option', "quotation",JText::_('COM_CONTENTDETAILS_TYPE_QUOTATION'));
		}
			
		$html=JHTML::_('select.genericlist', $options,$this->name, 
'class="inputbox" onchange="changedropvalue()"', 'value', 'text', $this->value,$this->id);
       // $doc->addStyleSheet(JURI::root().$this->element['path'].'style.css');        
       return $html;
    }
	
}
/* eof */