<?php

/**
 * @version     1.0.0
 * @package     com_contentdetails
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      intotality <pooja@intotalityinc.com> - http://intotalityinc.com
 */
defined('_JEXEC') or die;

jimport('joomla.application.component.modellist');

/**
 * Methods supporting a list of Contentdetails records.
 */
class ContentdetailsModeldetails extends JModelList {

    /**
     * Constructor.
     *
     * @param    array    An optional associative array of configuration settings.
     * @see        JController
     * @since    1.6
     */
    public function __construct($config = array()) {
		
        if (empty($config['filter_fields'])) {
            $config['filter_fields'] = array(
                                'id', 'a.id',
				'type','a.type',
				'selected_part','a.selected_part',
				'defination','a.defination',
				'analysis','a.analysis',
				'color','a.color',
                'ordering', 'a.ordering',
                'state', 'a.state',
                'created_by', 'a.created_by',

            );
        }

        parent::__construct($config);
    }

    /**
     * Method to auto-populate the model state.
     *
     * Note. Calling getState in this method will result in recursion.
     */
    protected function populateState($ordering = null, $direction = null) {
        // Initialise variables.
        $app = JFactory::getApplication('administrator');

        // Load the filter state.
        $search = $app->getUserStateFromRequest($this->context . '.filter.search', 'filter_search');
        $this->setState('filter.search', $search);

        $published = $app->getUserStateFromRequest($this->context . '.filter.state', 'filter_published', '', 'string');
        $this->setState('filter.state', $published);
		
		$type = $this->getUserStateFromRequest($this->context . '.filter.type', 'filter_type', '');
		$this->setState('filter.type', $type);

        

        // Load the parameters.
        $params = JComponentHelper::getParams('com_contentdetails');
        $this->setState('params', $params);

        // List state information.
        parent::populateState('a.id', 'asc');
    }

    /**
     * Method to get a store id based on model configuration state.
     *
     * This is necessary because the model is used by the component and
     * different modules that might need different sets of data or different
     * ordering requirements.
     *
     * @param	string		$id	A prefix for the store id.
     * @return	string		A store id.
     * @since	1.6
     */
    protected function getStoreId($id = '') {
        // Compile the store id.
        $id.= ':' . $this->getState('filter.search');
        $id.= ':' . $this->getState('filter.state');

        return parent::getStoreId($id);
    }

    /**
     * Build an SQL query to load the list data.
     *
     * @return	JDatabaseQuery
     * @since	1.6
     */
    protected function getListQuery() {
        // Create a new query object.
        $db = $this->getDbo();
        $query = $db->getQuery(true);

        // Select the required fields from the table.
        $query->select(
                $this->getState(
                        'list.select', 'a.*'
                )
        );
        $query->from('`#__content_details` AS a');

        
    // Join over the users for the checked out user.
    $query->select('uc.name AS editor');
    $query->join('LEFT', '#__users AS uc ON uc.id=a.checked_out');
    
		// Join over the user field 'created_by'
		$query->select('created_by.name AS created_by');
		$query->join('LEFT', '#__users AS created_by ON created_by.id = a.created_by');

        
    // Filter by published state
    $published = $this->getState('filter.state');
    if (is_numeric($published)) {
        $query->where('a.state = '.(int) $published);
    } else if ($published === '') {
        $query->where('(a.state IN (0, 1))');
    }
    
		// Filter on the type.
		if ($type = $this->getState('filter.type'))
		{
			$query->where('a.type = ' . $db->quote($type));
		}
       
	    // Filter by search in title
        $search = $this->getState('filter.search');
        if (!empty($search)) {
			
            if (stripos($search, 'id:') === 0) {
              $query->where('a.id = ' . (int) substr($search, 3));
            } else {
               $search = $db->Quote('%' . $db->escape($search, true) . '%');
			   $query->where('(a.selected_part LIKE ' . $search . ' OR a.defination LIKE ' . $search . ' OR a.analysis LIKE ' . $search . ')');
                
            }
			
        }

        


        // Add the list ordering clause.
        $orderCol = $this->state->get('list.ordering');
        $orderDirn = $this->state->get('list.direction');
        if ($orderCol && $orderDirn) {
            $query->order($db->escape($orderCol . ' ' . $orderDirn));
        }
		
        return $query;
    }

    public function getItems() {
		
        $items = parent::getItems();
		return $items;
    }
	public function getDefination()
	{
		$id.= ':' . $this->getState('filter.search');
		$db = $this->getDbo();
        $query = $db->getQuery(true);
		$query->select('*');
		$query->from('`#__content_details`');
		$query->where('type ="defination"');
		// Filter by search in title
        $search = $this->getState('filter.search');
        if (!empty($search)) {
			
            if (stripos($search, 'id:') === 0) {
              $query->where('id = ' . (int) substr($search, 3));
            } else {
               $search = $db->Quote('%' . $db->escape($search, true) . '%');
			   $query->where('(selected_part LIKE ' . $search . ' OR defination LIKE ' . $search . ')');
                
            }
			
        }
		$db->setQuery($query);
		$results = $db->loadObjectList();
		return $results;
	}
	public function getAnalysis()
	{
		$id.= ':' . $this->getState('filter.search');
		$db = $this->getDbo();
        $query = $db->getQuery(true);
		$query->select('*');
		$query->from('`#__content_details`');
		$query->where('type ="analysis"');
		// Filter by search in title
        $search = $this->getState('filter.search');
        if (!empty($search)) {
			
            if (stripos($search, 'id:') === 0) {
              $query->where('id = ' . (int) substr($search, 3));
            } else {
               $search = $db->Quote('%' . $db->escape($search, true) . '%');
			   $query->where('(selected_part LIKE ' . $search . ' OR analysis LIKE ' . $search . ')');
                
            }
			
        }
		$db->setQuery($query);
		$results = $db->loadObjectList();
		return $results;
	}
}
