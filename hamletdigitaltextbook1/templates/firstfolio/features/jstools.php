<?php
/**
 * @package     gantry
 * @subpackage  features
 * @version		3.2.8 August 1, 2011
 * @author		RocketTheme http://www.rockettheme.com
 * @copyright 	Copyright (C) 2007 - 2011 RocketTheme, LLC
 * @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
 *
 * Gantry uses the Joomla Framework (http://www.joomla.org), a GNU/GPLv2 content management system
 *
 */

defined('JPATH_BASE') or die();

gantry_import('core.gantryfeature');

/**
 * @package     gantry
 * @subpackage  features
 */
class GantryFeatureJSTools extends GantryFeature {
    var $_feature_name = 'jstools';

    function isEnabled(){
        return true; 
    }
    function isInPosition($position) {
        return false;
    } 
	function isOrderable(){
		return false;
	}
	

	function init() {
        global $gantry;
		
		// date
		if ($gantry->get('date-enabled') && $gantry->get('date-clientside')) {
			$gantry->addScript('gantry-date.js');
			$gantry->addInlineScript($this->_dateFormat());
		}
		// build spans
		if ($gantry->get('buildspans-enabled')) {
			$modules = "['rt-block']";
			$headers = "['h3','h2','h1']";
			
			$gantry->addScript('gantry-buildspans.js');
			$gantry->addInlineScript($this->_buildSpans($modules, $headers));
		}
		// inputs
		if ($gantry->get('inputstyling-enabled') && !($gantry->browser->name == 'ie' && $gantry->browser->shortversion == '6')) {
			$exclusions = $gantry->get('inputstyling-exclusions');
			$gantry->addScript('gantry-inputs.js');
			$gantry->addInlineScript("InputsExclusion.push($exclusions)");
		}
		
		
		
		// start custom code
		//////////////////////////
		$document = JFactory::getDocument();
		$getapps= & JFactory::getApplication();
		$template = $getapps->getTemplate();
		$template_dir = JURI::root(true).'/templates/'.$template;
		jimport('joomla.environment.browser');
		$browser = JBrowser::getInstance();
		$agent = $browser->getAgentString();
		
		
		// add Load jQuery
		
	 // load jQuery, if not loaded before
 		JLoader::import( 'joomla.version' );
		$version = new JVersion();
		if (version_compare( $version->RELEASE, '2.5', '<=')) {
    	if(JFactory::getApplication()->get('jquery') !== true) {
        $document->addScript("http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js");
        JFactory::getApplication()->set('jquery', true);
    	}
		} else {
   				JHtml::_('jquery.framework');
		}
					
		// add noconflict so it won't conflict with mootools
			
				//$document->addScript($template_dir."/js/jquery.noconflict.js");
			
		$document->addScript($template_dir."/js/scrolltopcontrol.js");
		
		//load powertip
		$document->addScript($template_dir."/js/jquery.tooltipster.js");
		//checking for mobile device
		if($browser->isMobile() || stristr($agent, 'mobile') && !(stristr($agent, 'iPad')) )
		{	
		$document->addScriptDeclaration('jQuery(document).ready(function($) {jQuery(".tooltip").tooltipster({position:"top"});});');
		}
		else
		{
			$document->addScriptDeclaration('jQuery(document).ready(function($) {jQuery(".tooltip").tooltipster();});');
		}
		$document->addScript($template_dir."/js/functions.js");	
		
		//$document->addScript($template_dir."/js/highlight.js");
		//added by pooja for hide user menu to super user and manager
		$user = JFactory::getUser(); $groups = $user->get('groups');
		if($groups['8']=='8' or $groups['6']=='6')
		{
			$document->addScriptDeclaration('jQuery(document).ready(function($) {jQuery( ".login-menu" ).css( "display", "none" )});');
		}
		
	}
	
	function _dateLanguage() {
		
		$days = array(
			'Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat',
			'Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'
		);
		
		$months = array(
			'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec',
			'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'		
		);
		
		return "dayNames:['" . implode("', '", $days) . "'], monthNames:['" . implode("', '", $months) . "']";
	}
	
	function _dateFormat() {
		global $gantry;
		
		$formats = str_replace("%", "$", $gantry->get('date-formats'));

		$js = "
			dateFormat.i18n = {
				".$this->_dateLanguage()."
			};
			var dateFeature = new Date().format('$formats');
			window.addEvent('domready', function() {
				var dates = $$('.date-block .date, .date, .rt-date-feature');
				if (dates.length) {
					dates.each(function(date) {
						date.set('text', dateFeature);
					});
				}
			});
		";
		
		return $js;
	}
	
	function _buildSpans($modules, $headers) {
		global $gantry;
		
		$js = "
			window.addEvent('domready', function() {
				var modules = $modules;
				var header = $headers;
				GantryBuildSpans(modules, header);
			});
		";
		
		return $js;
	}
}