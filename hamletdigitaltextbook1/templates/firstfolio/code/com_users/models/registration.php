<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
jimport('joomla.log.log');
/**
 * Registration model class for Users.
 *
 * @package     Joomla.Site
 * @subpackage  com_users
 * @since       1.6
 */
class UsersModelRegistration extends JModelForm
{
	/**
	 * @var    object  The user registration data.
	 * @since  1.6
	 */
	protected $data;

	/**
	 * Method to activate a user account.
	 *
	 * @param   string  $token  The activation token.
	 *
	 * @return  mixed    False on failure, user object on success.
	 *
	 * @since   1.6
	 */
	public function activate($token)
	{
		$config = JFactory::getConfig();
		$userParams = JComponentHelper::getParams('com_users');
		$db = $this->getDbo();

		// Get the user id based on the token.
		$query = $db->getQuery(true);
		$query->select($db->quoteName('id'))
			->from($db->quoteName('#__users'))
			->where($db->quoteName('activation') . ' = ' . $db->quote($token))
			->where($db->quoteName('block') . ' = ' . 1)
			->where($db->quoteName('lastvisitDate') . ' = ' . $db->quote($db->getNullDate()));
		$db->setQuery($query);

		try
		{
			$userId = (int) $db->loadResult();
		}
		catch (RuntimeException $e)
		{
			$this->setError(JText::sprintf('COM_USERS_DATABASE_ERROR', $e->getMessage()), 500);
			return false;
		}

		// Check for a valid user id.
		if (!$userId)
		{
			$this->setError(JText::_('COM_USERS_ACTIVATION_TOKEN_NOT_FOUND'));
			return false;
		}

		// Load the users plugin group.
		JPluginHelper::importPlugin('user');

		// Activate the user.
		$user = JFactory::getUser($userId);

		// Admin activation is on and user is verifying their email
		if (($userParams->get('useractivation') == 2) && !$user->getParam('activate', 0))
		{
			$uri = JUri::getInstance();

			// Compile the admin notification mail values.
			$data = $user->getProperties();
			$data['activation'] = JApplication::getHash(JUserHelper::genRandomPassword());
			$user->set('activation', $data['activation']);
			$data['siteurl'] = JUri::base();
			$base = $uri->toString(array('scheme', 'user', 'pass', 'host', 'port'));
			$data['activate'] = $base . JRoute::_('index.php?option=com_users&task=registration.activate&token=' . $data['activation'], false);
			$data['fromname'] = $config->get('fromname');
			$data['mailfrom'] = $config->get('mailfrom');
			$data['sitename'] = $config->get('sitename');
			$user->setParam('activate', 1);
			$emailSubject = JText::sprintf(
				'COM_USERS_EMAIL_ACTIVATE_WITH_ADMIN_ACTIVATION_SUBJECT',
				$data['name'],
				$data['sitename']
			);

			$emailBody = JText::sprintf(
				'COM_USERS_EMAIL_ACTIVATE_WITH_ADMIN_ACTIVATION_BODY',
				$data['sitename'],
				$data['name'],
				$data['email'],
				$data['username'],
				$data['activate']
			);

			// get all admin users
			$query->clear()
				->select($db->quoteName(array('name', 'email', 'sendEmail', 'id')))
				->from($db->quoteName('#__users'))
				->where($db->quoteName('sendEmail') . ' = ' . 1);

			$db->setQuery($query);

			try
			{
				$rows = $db->loadObjectList();
			}
			catch (RuntimeException $e)
			{
				$this->setError(JText::sprintf('COM_USERS_DATABASE_ERROR', $e->getMessage()), 500);
				return false;
			}

			// Send mail to all users with users creating permissions and receiving system emails
			foreach ($rows as $row)
			{
				$usercreator = JFactory::getUser($row->id);

				if ($usercreator->authorise('core.create', 'com_users'))
				{
					$return = JFactory::getMailer()->sendMail($data['mailfrom'], $data['fromname'], $row->email, $emailSubject, $emailBody);

					// Check for an error.
					if ($return !== true)
					{
						$this->setError(JText::_('COM_USERS_REGISTRATION_ACTIVATION_NOTIFY_SEND_MAIL_FAILED'));
						return false;
					}
				}
			}
		}
		// Admin activation is on and admin is activating the account
		elseif (($userParams->get('useractivation') == 2) && $user->getParam('activate', 0))
		{
			$user->set('activation', '');
			$user->set('block', '0');

			// Compile the user activated notification mail values.
			$data = $user->getProperties();
			$user->setParam('activate', 0);
			$data['fromname'] = $config->get('fromname');
			$data['mailfrom'] = $config->get('mailfrom');
			$data['sitename'] = $config->get('sitename');
			$data['siteurl'] = JUri::base();
			$emailSubject = JText::sprintf(
				'COM_USERS_EMAIL_ACTIVATED_BY_ADMIN_ACTIVATION_SUBJECT',
				$data['name'],
				$data['sitename']
			);

			$emailBody = JText::sprintf(
				'COM_USERS_EMAIL_ACTIVATED_BY_ADMIN_ACTIVATION_BODY',
				$data['name'],
				$data['siteurl'],
				$data['username']
			);

			$return = JFactory::getMailer()->sendMail($data['mailfrom'], $data['fromname'], $data['email'], $emailSubject, $emailBody);

			// Check for an error.
			if ($return !== true)
			{
				$this->setError(JText::_('COM_USERS_REGISTRATION_ACTIVATION_NOTIFY_SEND_MAIL_FAILED'));
				return false;
			}
		}
		else
		{
			$user->set('activation', '');
			$user->set('block', '0');
		}

		// Store the user object.
		if (!$user->save())
		{
			$this->setError(JText::sprintf('COM_USERS_REGISTRATION_ACTIVATION_SAVE_FAILED', $user->getError()));
			return false;
		}

		return $user;
	}

	/**
	 * Method to get the registration form data.
	 *
	 * The base form data is loaded and then an event is fired
	 * for users plugins to extend the data.
	 *
	 * @return  mixed  Data object on success, false on failure.
	 *
	 * @since   1.6
	 */
	public function getData()
	{
		if ($this->data === null)
		{
			$this->data = new stdClass;
			$app = JFactory::getApplication();
			$params = JComponentHelper::getParams('com_users');

			// Override the base user data with any data in the session.
			$temp = (array) $app->getUserState('com_users.registration.data', array());
			foreach ($temp as $k => $v)
			{
				$this->data->$k = $v;
			}

			// Get the groups the user should be added to after registration.
			$this->data->groups = array();

			// Get the default new user group, Registered if not specified.
			$system = $params->get('new_usertype', 2);

			$this->data->groups[] = $system;
			
			// Unset the passwords.
			unset($this->data->password1);
			unset($this->data->password2);
			unset($this->data->payment['card_type']);
			unset($this->data->payment['cardholder_name']);
			unset($this->data->payment['card_no']);
			unset($this->data->payment['card_cvc']);
			unset($this->data->payment['card_expire_month']);
			unset($this->data->payment['card_expire_year']);
			

			// Get the dispatcher and load the users plugins.
			$dispatcher = JEventDispatcher::getInstance();
			JPluginHelper::importPlugin('user');

			// Trigger the data preparation event.
			$results = $dispatcher->trigger('onContentPrepareData', array('com_users.registration', $this->data));

			// Check for errors encountered while preparing the data.
			if (count($results) && in_array(false, $results, true))
			{
				$this->setError($dispatcher->getError());
				$this->data = false;
			}
		}

		return $this->data;
	}

	/**
	 * Method to get the registration form.
	 *
	 * The base form is loaded from XML and then an event is fired
	 * for users plugins to extend the form with extra fields.
	 *
	 * @param   array    $data      An optional array of data for the form to interogate.
	 * @param   boolean  $loadData  True if the form is to load its own data (default case), false if not.
	 *
	 * @return  JForm  A JForm object on success, false on failure
	 *
	 * @since   1.6
	 */
	public function getForm($data = array(), $loadData = true)
	{
		
		// Get the form.
		$form = $this->loadForm('com_users.registration', 'registration', array('control' => 'jform', 'load_data' => $loadData));
		if (empty($form))
		{
			return false;
		}

		return $form;
	}

	/**
	 * Method to get the data that should be injected in the form.
	 *
	 * @return  mixed  The data for the form.
	 *
	 * @since   1.6
	 */
	protected function loadFormData()
	{
		$data = $this->getData();

		$this->preprocessData('com_users.registration', $data);

		return $data;
	}

	/**
	 * Override preprocessForm to load the user plugin group instead of content.
	 *
	 * @param   JForm   $form   A JForm object.
	 * @param   mixed   $data   The data expected for the form.
	 * @param   string  $group  The name of the plugin group to import (defaults to "content").
	 *
	 * @return  void
	 *
	 * @since   1.6
	 * @throws  Exception if there is an error in the form event.
	 */
	protected function preprocessForm(JForm $form, $data, $group = 'user')
	{
		$userParams = JComponentHelper::getParams('com_users');

		//Add the choice for site language at registration time
		if ($userParams->get('site_language') == 1 && $userParams->get('frontend_userparams') == 1)
		{
			$form->loadFile('sitelang', false);
		}

		parent::preprocessForm($form, $data, $group);
	}

	/**
	 * Method to auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @since   1.6
	 */
	protected function populateState()
	{
		// Get the application object.
		$app = JFactory::getApplication();
		$params = $app->getParams('com_users');

		// Load the parameters.
		$this->setState('params', $params);
	}

	/**
	 * Method to save the form data.
	 *
	 * @param   array  $temp  The form data.
	 *
	 * @return  mixed  The user id on success, false on failure.
	 *
	 * @since   1.6
	 */
	public function register($temp)
	{
		
		$params = JComponentHelper::getParams('com_users');
		// Initialise the table with JUser.
		$user = new JUser;
		$data = (array) $this->getData();
		
		
		// Merge in the registration data.
		foreach ($temp as $k => $v)
		{
			$data[$k] = $v;
		}
		
		// Prepare the data for the user object.
		$data['email'] = JStringPunycode::emailToPunycode($data['email1']);
		$data['password'] = $data['password1'];
		$useractivation = $params->get('useractivation');
		$sendpassword = $params->get('sendpassword', 1);

		// Check if the user needs to activate their account.
		if (($useractivation == 1) || ($useractivation == 2))
		{
			$data['activation'] = JApplication::getHash(JUserHelper::genRandomPassword());
			$data['block'] = 1;
		}

		// Bind the data.
		if (!$user->bind($data))
		{
			$this->setError(JText::sprintf('COM_USERS_REGISTRATION_BIND_FAILED', $user->getError()));
			return false;
		}
		
		//create user logs file
		$userlog = $this->getStoraDataTemp($data);
		
		$result = $this->getPaymentUser($data);

		if($result['ACK'] != "Success")
		{
			$errorTextLog = $result['L_ERRORCODE0'].' : '.$result['L_SHORTMESSAGE0'].' : '.$result['L_LONGMESSAGE0'];
			JLog::add($_SERVER['REMOTE_ADDR'].' '.$errorTextLog,JLog::ERROR,'gantry');
			
			$errorText = $result['L_LONGMESSAGE0'];
			if($result['L_ERRORCODE0']=='10002' || $result['L_ERRORCODE0']=='10008')
				$errorText = JText::_('PAYPAL_TMP_ERROR');
			
			$this->setError(JText::sprintf($errorText, $user->getError()),'error');
			//$this->setError(JText::sprintf('COM_USER_CARD_INVALID', $user->getError()));
			return false;
		}
		// Load the users plugin group.
		JPluginHelper::importPlugin('user');
		// Store the data.
		
		
		if($data['plan'][0]!=1 && $data['info']['companyname']=='')
		{
			$this->setError(JText::_('COM_USERS_SCHOOL_ORGANIZATION'));
			return false;
			
		}
		if (!$user->save())
		{
			$this->setError(JText::sprintf('COM_USERS_REGISTRATION_SAVE_FAILED', $user->getError()));
			return false;
		}
		
		
		$config = JFactory::getConfig();
		$db = $this->getDbo();
		$query = $db->getQuery(true);

		// Compile the notification mail values.
		$data = $user->getProperties();
		$data['fromname'] = $config->get('fromname');
		$data['mailfrom'] = $config->get('mailfrom');
		$data['sitename'] = $config->get('sitename');
		$data['siteurl'] = JUri::root();
		
		if($user->id)
		{
			$sub = $this->SaveSubscription($user);
			//send subscription email
			if($user->id)
			{
				$subject        =  JText::_('COM_PAYMENT_SUCCESSFULLY_SUBJECT');
				$msg 			= JText::sprintf('COM_PAYMENT_SUCCESSFULLY_MSG',$user->name,$sub->numoflicense,date('M d,Y',strtotime($sub->purchasedate)),date('M d,Y',strtotime($sub->expirationdate)),'$'.number_format($sub->price, 2),$data['sitename']);
				// Send the registration email.
				$mode 			= 1;
				$sendmail = JFactory::getMailer()->sendMail($data['mailfrom'], $data['fromname'], $user->email, $subject, $msg,$mode);
			}
		}
		

		// Handle account activation/confirmation emails.
		if ($useractivation == 2)
		{
			// Set the link to confirm the user email.
			$uri = JUri::getInstance();
			$base = $uri->toString(array('scheme', 'user', 'pass', 'host', 'port'));
			$data['activate'] = $base . JRoute::_('index.php?option=com_users&task=registration.activate&token=' . $data['activation'], false);

			$emailSubject = JText::sprintf(
				'COM_USERS_EMAIL_ACCOUNT_DETAILS',
				$data['name'],
				$data['sitename']
			);

			if ($sendpassword)
			{
				$emailBody = JText::sprintf(
					'COM_USERS_EMAIL_REGISTERED_WITH_ADMIN_ACTIVATION_BODY',
					$data['name'],
					$data['sitename'],
					$data['activate'],
					$data['siteurl'],
					$data['username'],
					$data['password_clear']
				);
			}
			else
			{
				$emailBody = JText::sprintf(
					'COM_USERS_EMAIL_REGISTERED_WITH_ADMIN_ACTIVATION_BODY_NOPW',
					$data['name'],
					$data['sitename'],
					$data['activate'],
					$data['siteurl'],
					$data['username']
				);
			}
		}
		elseif ($useractivation == 1)
		{
			// Set the link to activate the user account.
			$uri = JUri::getInstance();
			$base = $uri->toString(array('scheme', 'user', 'pass', 'host', 'port'));
			$data['activate'] = $base . JRoute::_('index.php?option=com_users&task=registration.activate&token=' . $data['activation'], false);

			$emailSubject = JText::sprintf(
				'COM_USERS_EMAIL_ACCOUNT_DETAILS',
				$data['sitename']
			);

			if ($sendpassword)
			{
				$emailBody = JText::sprintf(
					'COM_USERS_EMAIL_REGISTERED_WITH_ACTIVATION_BODY',
					$data['name'],
					$data['sitename'],
					$data['activate'],
					$data['siteurl'],
					$data['username'],
					$data['password_clear']
				);
			}
			else
			{
				$emailBody = JText::sprintf(
					'COM_USERS_EMAIL_REGISTERED_WITH_ACTIVATION_BODY_NOPW',
					$data['name'],
					$data['sitename'],
					$data['activate'],
					$data['siteurl'],
					$data['username']
				);
			}
		}
		else
		{

			$emailSubject = JText::sprintf(
				'COM_USERS_EMAIL_ACCOUNT_DETAILS',
				$data['sitename']
			);

			if ($sendpassword)
			{
				$emailBody = JText::sprintf(
					'COM_USERS_EMAIL_REGISTERED_BODY',
					$data['name'],
					$data['sitename'],
					$data['siteurl'],
					$data['username'],
					$data['password_clear'],
					$data['sitename']
				);
			}
			else
			{
				$emailBody = JText::sprintf(
					'COM_USERS_EMAIL_REGISTERED_BODY_NOPW',
					$data['name'],
					$data['sitename'],
					$data['siteurl']
				);
			}
		}

		// Send the registration email.
		$return = JFactory::getMailer()->sendMail($data['mailfrom'], $data['fromname'], $data['email'], $emailSubject, $emailBody);

		// Send Notification mail to administrators
		if (($params->get('useractivation') < 2) && ($params->get('mail_to_admin') == 1))
		{
			$emailSubject = JText::sprintf(
				'COM_USERS_EMAIL_ACCOUNT_DETAILS',
				$data['name'],
				$data['sitename']
			);

			$emailBodyAdmin = JText::sprintf(
				'COM_USERS_EMAIL_REGISTERED_NOTIFICATION_TO_ADMIN_BODY',
				$data['name'],
				$data['siteurl']
			);

			// Get all admin users
			$query->clear()
				->select($db->quoteName(array('name', 'email', 'sendEmail')))
				->from($db->quoteName('#__users'))
				->where($db->quoteName('sendEmail') . ' = ' . 1);

			$db->setQuery($query);

			try
			{
				$rows = $db->loadObjectList();
			}
			catch (RuntimeException $e)
			{
				$this->setError(JText::sprintf('COM_USERS_DATABASE_ERROR', $e->getMessage()), 500);
				return false;
			}

			// Send mail to all superadministrators id
			foreach ($rows as $row)
			{
				$return = JFactory::getMailer()->sendMail($data['mailfrom'], $data['fromname'], $row->email, $emailSubject, $emailBodyAdmin);
				// Check for an error.
				if ($return !== true)
				{
					$this->setError(JText::_('COM_USERS_REGISTRATION_ACTIVATION_NOTIFY_SEND_MAIL_FAILED'));
					return false;
				}
			}
		}

		// Check for an error.
		if ($return !== true)
		{
			$this->setError(JText::_('COM_USERS_REGISTRATION_SEND_MAIL_FAILED'));

			// Send a system message to administrators receiving system mails
			$db = JFactory::getDbo();
			$query->clear()
				->select($db->quoteName(array('name', 'email', 'sendEmail', 'id')))
				->from($db->quoteName('#__users'))
				->where($db->quoteName('block') . ' = ' . (int) 0)
				->where($db->quoteName('sendEmail') . ' = ' . (int) 1);
			$db->setQuery($query);

			try
			{
				$sendEmail = $db->loadColumn();
			}
			catch (RuntimeException $e)
			{
				$this->setError(JText::sprintf('COM_USERS_DATABASE_ERROR', $e->getMessage()), 500);
				return false;
			}

			if (count($sendEmail) > 0)
			{
				$jdate = new JDate;

				// Build the query to add the messages
				foreach ($sendEmail as $userid)
				{
					$values = array($db->quote($userid), $db->quote($userid), $db->quote($jdate->toSql()), $db->quote(JText::_('COM_USERS_MAIL_SEND_FAILURE_SUBJECT')), $db->quote(JText::sprintf('COM_USERS_MAIL_SEND_FAILURE_BODY', $return, $data['username'])));
					$query->clear()
						->insert($db->quoteName('#__messages'))
						->columns($db->quoteName(array('user_id_from', 'user_id_to', 'date_time', 'subject', 'message')))
						->values(implode(',', $values));
					$db->setQuery($query);

					try
					{
						$db->execute();
					}
					catch (RuntimeException $e)
					{
						$this->setError(JText::sprintf('COM_USERS_DATABASE_ERROR', $e->getMessage()), 500);
						return false;
					}
				}
			}
			return false;
		}
		
		if ($useractivation == 1)
		{
			return "useractivate";
		}
		elseif ($useractivation == 2)
		{
			return "adminactivate";
		}
		else
		{
			return $user->id;
		}
	}
	
	
	public function NVPToArray($NVPString)
	{ 
		$proArray = array();
		while(strlen($NVPString))
		{
			// name
			$keypos= strpos($NVPString,'=');
			$keyval = substr($NVPString,0,$keypos);
			// value
			$valuepos = strpos($NVPString,'&') ? strpos($NVPString,'&'): strlen($NVPString);
			$valval = substr($NVPString,$keypos+1,$valuepos-$keypos-1);
			// decoding the respose
			$proArray[$keyval] = urldecode($valval);
			$NVPString = substr($NVPString,$valuepos+1,strlen($NVPString));
		}
		return $proArray;
	}
	
	
	public function getPaymentUser($data)
	{
		//delete user
		$db = JFactory::getDbo();
		$params = &JComponentHelper::getParams('com_subscription');
		//$planpricedata = $this->getplanPriceSub($data['selectplan']['plan'][0]);
		$planpricedata = $this->getplanPriceSub($data['plan'][0]);
			
			
			$api_endpoint = "https://api-3t.paypal.com/nvp"	;
			
			/*
			$request_params = array
							(
							'METHOD' => 'DoDirectPayment', 
							'USER' => $params->get('api_username'), 
							'PWD' => $params->get('api_password'), 
							'SIGNATURE' => $params->get('api_signature'), 
							'VERSION' => 85.0, 
							'PAYMENTACTION' => 'Sale', 					
							'IPADDRESS' => $_SERVER['REMOTE_ADDR'],
							'CREDITCARDTYPE' => $data['payment']['card_type'], 
							'ACCT' => $data['payment']['card_no'], 						
							'EXPDATE' =>$data['payment']['card_expire_month'].$data['payment']['card_expire_year'], 			
							'CVV2' => $data['payment']['card_cvc'], 
							'AMT' => $planpricedata->price
							);
				*/			
			$request_params = array
								(
								'METHOD' => 'DoDirectPayment', 
								'USER' => $params->get('api_username'), 
								'PWD' => $params->get('api_password'), 
								'SIGNATURE' => $params->get('api_signature'), 
								'VERSION' => 85.0, 
								'PAYMENTACTION' => 'Sale', 					
								'IPADDRESS' => $_SERVER['REMOTE_ADDR'],
								'CREDITCARDTYPE' => $data['payment']['card_type'], 
								'ACCT' => $data['payment']['card_no'], 						
								'EXPDATE' =>$data['payment']['card_expire_month'].$data['payment']['card_expire_year'], 			
								'CVV2' => $data['payment']['card_cvc'], 
								'AMT' => $planpricedata->price, 
								'CURRENCYCODE' => 'USD', 
								'FIRSTNAME' => $data['name'], 
								'EMAIL' => $data['email1'], 
								'STREET' => $data['info']['streetname'], 
								'CITY' => $data['info']['city'], 
								'STATE' => $data['info']['state'], 					
								'COUNTRYCODE' => 'US', 
								'ZIP' => $data['info']['zipcode'], 
								'DESC' => 'Hamlet digital textbook Subscription Plan - $'.$planpricedata->price 
								);	
				
		//echo "<pre>";print_r($request_params);exit;				
		// Loop through $request_params array to generate the NVP string.
		$nvp_string = '';
		foreach($request_params as $var=>$val)
		{
			$nvp_string .= '&'.$var.'='.urlencode($val);	
		}
		
		// Send NVP string to PayPal and store response
		$curl = curl_init();
				curl_setopt($curl, CURLOPT_VERBOSE, 1);
				curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
				curl_setopt($curl, CURLOPT_TIMEOUT, 30);
				curl_setopt($curl, CURLOPT_URL, $api_endpoint);
				curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($curl, CURLOPT_POSTFIELDS, $nvp_string);
		
		$result = curl_exec($curl);
		curl_close($curl);
		// Parse the API response
		$result_array = $this->NVPToArray($result);
		//$result_array = parse_str($result,$resultarray);
		return $result_array;
		
		
	}
	/*function getPaymentUser($data)
	{
		//delete user
		$db = JFactory::getDbo();
		$params = &JComponentHelper::getParams( 'com_subscription');
		$post_url = $params->get('post_url');
		$login_id=$params->get('loginid');
		$transaction_key=$params->get('transactionkey');
		//$planpricedata = $this->getplanPriceSub($data['selectplan']['plan'][0]);
		$planpricedata = $this->getplanPriceSub($data['plan'][0]);
		
		$post_values = array(
		// the API Login ID and Transaction Key must be replaced with valid values
		"x_login"			=> $login_id,
		"x_tran_key"		=> $transaction_key,
		
		"x_version"			=> "3.1",
		"x_delim_data"		=> "TRUE",
		"x_delim_char"		=> "|",
		"x_relay_response"	=> "FALSE",
		
		"x_type"			=> "AUTH_CAPTURE",
		"x_method"			=> "CC",
		
		
		//"x_card_num"		=> "4111111111111111",
		//"x_exp_date"		=> "0115",
		
		
		"x_card_num"		=> $data['payment']['card_no'],
		"x_card_code"		=> $data['payment']['card_cvc'],
		"x_exp_date"		=> $data['payment']['card_expire_month'].substr($data['payment']['card_expire_year'],2,2),
		
		"x_amount"			=> $planpricedata->price,
		"x_description"		=> "Registration Subscription",
		
		"x_first_name"		=> $data['payment']['cardholder_name'],
		"x_last_name"		=> $data['payment']['cardholder_name'],
		"x_address"			=> $data['info']['streetname'],
		"x_state"			=> $data['info']['city'],
		"x_zip"				=> $data['info']['state']
		
		// Additional fields can be added here as outlined in the AIM integration
		// guide at: http://developer.authorize.net
		);
		$post_string = "";
		foreach( $post_values as $key => $value )
		{ $post_string .= "$key=" . urlencode( $value ) . "&"; }
		$post_string = rtrim( $post_string, "& " );
		$request = curl_init($post_url); // initiate curl object
		curl_setopt($request, CURLOPT_HEADER, 0); // set to 0 to eliminate header info from response
		curl_setopt($request, CURLOPT_RETURNTRANSFER, 1); // Returns response data instead of TRUE(1)
		curl_setopt($request, CURLOPT_POSTFIELDS, $post_string); // use HTTP POST to send form data
		curl_setopt($request, CURLOPT_SSL_VERIFYPEER, FALSE); // uncomment this line if you get no gateway response.
		$post_response = curl_exec($request);
		curl_close ($request); // close curl object
		$response_array = explode($post_values["x_delim_char"],$post_response);
		return $response_array[0];
		
	}*/
	
	public function SaveSubscription($data)
	{		
			$db = JFactory::getDbo();
			$planpricedata = $this->getplanPriceSub($data->plan[0]);
			//insert subscription
			$date = JFactory::getDate();
			$insertdata =new stdClass();
			$params = &JComponentHelper::getParams( 'com_subscription');
			$time_period = $params->get('time_period');
			$insertdata->id = null;
			$insertdata->userid = $data->id;
			$insertdata->purchasedate = $date->toSql();
			$insertdata->expirationdate = date('Y-m-d',strtotime(date("Y-m-d") . "+ ".$time_period." month"));
			$insertdata->licensekey = $this->getLicenseKey();
			$insertdata->numoflicense = $planpricedata->plan;
			$insertdata->price = $planpricedata->price;
			$insertdata->createdby = $data->id;
			$insertdata->usertype = 0;
			$insertdata->status = 'confirmed';
			$insertdata->ipaddress = $_SERVER['REMOTE_ADDR'];
			$insertdata->modify_date = $date->toSql();
			$db->insertObject( '#__subscription', $insertdata, id );
			//insert subscription_log
			$subdata =new stdClass();
			$subdata->id = null;
			$subdata->userid = $insertdata->userid;
			$subdata->price = $insertdata->price;
			$subdata->numoflicense = $insertdata->numoflicense;
			$subdata->purchasedate = $insertdata->purchasedate;
			$subdata->expirationdate = $insertdata->expirationdate;
			$subdata->licensekey = $insertdata->licensekey;
			$subdata->createdby = $insertdata->createdby;
			$subdata->ipaddress = $insertdata->ipaddress;
			$subdata->status = $insertdata->status;
			$subdata->modify_date = $insertdata->modify_date;
			$db->insertObject( '#__subscription_log', $subdata, id );
			return $insertdata;
	}
	public function getLicenseKey()
	{
			$d=date ("d");
			$m=date ("m");
			$y=date ("Y");
			$t=time();
			$dmt=$d+$m+$y+$t;    
			$ran= rand(0,10000000);
			$dmtran= $dmt+$ran;
			$un=  uniqid();
			$dmtun = $dmt.$un;
			$mdun = md5($dmtran.$un);
			$sort=substr($mdun, 16); // if you want sort length code.
			return $mdun;
	}
	
	public static function getplanPriceSub($id)
	{
		$options = array();
		$db		= JFactory::getDbo();
		$query	= $db->getQuery(true);
		$query->select('*');
		$query->from('#__plan_price AS a');
		$query->where('a.id ='.$id);
		// Get the options.
		$db->setQuery($query);
		$result = $db->loadObjectList();
		return $result[0];
	}
	/**
	** for writting new user registration log
	
	*/
	public static function getStoraDataTemp($data)
	{
		$config = new JConfig();
		$log_path = $config->log_path;
		$datastr = array('Name'=>$data['name'],'Username'=>$data['username'],'Email'=>$data['email'],'School'=>$data['info']['companyname'],'Address'=>$data['info']['streetname'],'State'=>$data['info']['state'],'City'=>$data['info']['city'],'Zipcode'=>$data['info']['zipcode'],'Subscription Plan'=>$data['plan'][0],'Date Time'=>date("Y-m-d h:i:s"),'IP Address'=>$_SERVER['REMOTE_ADDR']);
		$content = json_encode($datastr);
		$filename = $log_path.'/user_'.date("Y_m_d_h_i_s").'.txt';
		$fp = fopen($filename,"wb");
		fwrite($fp,$content);
		fclose($fp);
	}
}
