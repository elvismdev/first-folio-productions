<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_login
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JHtml::_('behavior.keepalive');
$user=&JFactory::getUser();
?>
<form action="<?php echo JRoute::_('index.php', true, $params->get('usesecure')); ?>" method="post" id="login-form" class="form-vertical">
<div class="login-form">

<?php if ($params->get('greeting')) : ?>
	<?php if ($params->get('name') == 0): 
		{
			if($user->groups['2']==2)
		{
			echo'<a href="index.php?option=com_users&view=profile" style="color:#fff;text-decoration:underline">'.JText::sprintf('MOD_LOGIN_HINAME', htmlspecialchars($user->get('name'))).'</a>';
		}
		else
		{
			echo JText::sprintf('MOD_LOGIN_HINAME', htmlspecialchars($user->get('name')));
		}
	}
	   else : {
		echo'<a href="index.php?option=com_users&view=profile" style="color:#fff;text-decoration:underline">'.JText::sprintf('MOD_LOGIN_HINAME',htmlspecialchars($user->get('username'))).'</a>';
	} endif; ?>
	
<?php endif; ?>
		&nbsp;&nbsp;
		<input type="submit" name="Submit" class="btn" value="<?php echo JText::_('JLOGOUT'); ?>" />
		<input type="hidden" name="option" value="com_users" />
		<input type="hidden" name="task" value="user.logout" />
		<input type="hidden" name="return" value="<?php echo $return; ?>" />
		<?php echo JHtml::_('form.token'); ?>
</div>
	
</form>
