<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;
require_once JPATH_ADMINISTRATOR . '/components/com_subscription/helpers/subscription.php';

$session = JFactory::getSession();
$user = $session->get('user');
$iduser = $user->id;
$idsubscription = $session->get('idsubscription');
	   
$totaluser = SubscriptionHelper::getTotalSubUser($this->data->id,$idsubscription);
?>
<div class="profile <?php echo $this->pageclass_sfx?>">
<?php if ($this->params->get('show_page_heading')) : ?>
	<h2>
		<?php echo $this->escape($this->params->get('page_heading')); ?>
        <ul class="pull-right">
	<li class="btn-group">
		<a class="btn btn-primary" href="<?php echo JRoute::_('index.php?option=com_users&task=profile.edit&user_id='.(int) $this->data->id);?>">
			<span class="icon-user"></span> <?php echo JText::_('COM_USERS_EDIT_PROFILE'); ?></a>
	</li>
</ul>
	</h2>

<?php endif; ?>

<?php
if($this->data->info['usertype'] != 1 && $this->data->info['numoflicense'] != 1)
{
$r_user = explode("-",$this->data->info['numoflicense']);
$tot = $r_user[1] - $totaluser;
if($tot == 0)
	{ ?>
    		<div id="system-message-container">
                <div id="system-message">
                    <div class="alert alert-success">
                        <div><p><?php echo JText::_('COM_SUBSCRIPTION_NO_USER_MSG'); ?></p></div>
                    </div>
                </div>
            </div>
<?php } 
}
?>


<?php if (JFactory::getUser()->id == $this->data->id) : ?>

<?php endif; ?>

   <h3><?php echo JText::_('COM_USERS_ACC_INFO'); ?></h3>
    <dl class="dl-horizontal">
    
        <dt><?php echo JText::_('COM_USERS_ACC_INFO_NAME'); ?>:</dt>
        <dd><?php echo $this->data->name; ?></dd>
        
        <dt><?php echo JText::_('COM_USERS_ACC_INFO_USERNAME'); ?>:</dt>
        <dd><?php echo $this->data->username; ?></dd>
        
        <dt><?php echo JText::_('COM_USERS_ACC_INFO_R_DATE'); ?>:</dt>
        <dd><?php echo date('M d,Y',strtotime($this->data->registerDate)); ?></dd>
        
        <dt><?php echo JText::_('COM_USERS_ACC_INFO_L_DATE'); ?>:</dt>
        <dd><?php echo date('M d,Y',strtotime($this->data->lastvisitDate)); ?></dd>
    
    </dl>
	<div class="divider"></div>



    <h3><?php echo JText::_('COM_USERS_USER_INFO'); ?></h3>
    <dl class="dl-horizontal">
        
        <dt><?php echo JText::_('COM_USERS_USER_INFO_ORGANIZATIONNAME_LABLE'); ?>:</dt>
        <dd><?php echo $this->data->info['companyname']; ?></dd>
        
        <dt><?php echo JText::_('COM_USERS_USER_INFO_ADDRESS_LABLE'); ?>:</dt>
        <dd><?php echo $this->data->info['streetname']; ?></dd>
        
        <dt><?php echo JText::_('COM_USERS_USER_INFO_CITY_LABLE'); ?>:</dt>
        <dd><?php echo $this->data->info['city']; ?></dd>
        
        <dt><?php echo JText::_('COM_USERS_USER_INFO_STATE_LABLE'); ?>:</dt>
        <dd><?php echo $this->data->info['state']; ?></dd>
        
        <dt><?php echo JText::_('COM_USERS_USER_INFO_ZIPCODE_LABLE'); ?>:</dt>
        <dd><?php echo $this->data->info['zipcode']; ?></dd>
        
    </dl>

	<div class="divider"></div>

<?php if($this->data->info['usertype'] != 1){ ?>

    <h3><?php echo JText::_('COM_USERS_SUB_INFO'); ?></h3>
    <dl class="dl-horizontal">
       
        <dt><?php echo JText::_('COM_USERS_SUB_INFO_PURCHASE_DATE'); ?>:</dt>
        <dd><?php echo date('M d,Y',strtotime($this->data->info['startdate'])); ?></dd>
        
        <dt><?php echo JText::_('COM_USERS_SUB_INFO_EXPIRE_DATE'); ?>:</dt>
        <dd><?php echo date('M d,Y',strtotime($this->data->info['enddate'])); ?></dd>
        
        <dt><?php echo JText::_('COM_USERS_NO_OF_SEAT'); ?>:</dt>
        <dd><?php echo $this->data->info['numoflicense']; ?></dd>
                
        <dt><?php echo JText::_('COM_USERS_SUB_INFO_PRICE'); ?>:</dt>
        <dd><?php echo "$".$this->data->info['price']; ?></dd>
        
		<?php
        if($this->data->info['numoflicense'] != 1)
		{ ?>
        		<dt><?php echo JText::_('COM_USERS_SUB_INFO_C_USER'); ?>:</dt>
                <dd><?php echo $totaluser; ?></dd>
                
                <dt><?php echo JText::_('COM_USERS_SUB_INFO_R_USER'); ?>:</dt>
                <dd><?php echo $tot; ?>
                </dd>
        <?php } ?>
    
    </dl>
    <div class="divider"></div>
<?php } ?>
</div>
