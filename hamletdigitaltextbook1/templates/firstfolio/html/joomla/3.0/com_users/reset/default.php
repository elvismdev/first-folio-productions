<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JHtml::_('behavior.keepalive');
JHtml::_('behavior.formvalidation');
?>
<div class="reset <?php echo $this->pageclass_sfx?>">
	<?php if ($this->params->get('show_page_heading')) : ?>
	
		<h2>
			<?php echo $this->escape($this->params->get('page_heading')); ?>
		</h2>
        <?php else:?>
        <h2><?php echo JText::_('Forgot Password'); ?></h2>
	
	<?php endif; ?>

	<form id="user-registration" action="<?php echo JRoute::_('index.php?option=com_users&task=reset.request'); ?>" method="post" class="form-validate form-horizontal">

		<?php foreach ($this->form->getFieldsets() as $fieldset) : ?>
		<p><?php echo JText::_($fieldset->label); ?></p>

	<div class="space20"></div>	
    <div class="control-label"><?php echo  $this->form->getLabel('email'); ?></div>
			<?php foreach ($this->form->getFieldset($fieldset->name) as $name => $field) : ?>
				<?php echo $field->input; ?>
							
			<?php endforeach; ?>
		
		<?php endforeach; ?>
			<button type="submit" class="btn"><?php echo JText::_('JSUBMIT'); ?></button>
			<?php echo JHtml::_('form.token'); ?>
		
	</form>
</div>
