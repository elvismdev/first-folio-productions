<?php
/**
 * @version     1.0.0
 * @package     com_contentdetails
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      intotality <pooja@intotalityinc.com> - http://intotalityinc.com
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controllerform');

/**
 * Detail controller class.
 */
class SubscriptionControllerAdduser extends JControllerForm
{

	public function getModel($name = 'Adduser', $prefix = 'SubscriptionModel')
	{
		$model = parent::getModel($name, $prefix, array('ignore_request' => true));
		return $model;
	}
	
    public function save()
	{
		
		// Check for request forgeries.
		JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));
		$app	= JFactory::getApplication();
		$model	= $this->getModel('Adduser', 'SubscriptionModel');

		// Get the user data.
		$requestData = $this->input->post->get('jform', array(), 'array');

		// Validate the posted data.
		$form	= $model->getForm();
		if (!$form)
		{
			JError::raiseError(500, $model->getError());
			return false;
		}
		$data	= $model->validate($form, $requestData);

		// Check for validation errors.
		if ($data === false)
		{
			// Get the validation messages.
			$errors	= $model->getErrors();

			// Push up to three validation messages out to the user.
			for ($i = 0, $n = count($errors); $i < $n && $i < 3; $i++)
			{
				if ($errors[$i] instanceof Exception)
				{
					$app->enqueueMessage($errors[$i]->getMessage(), 'warning');
				} else {
					$app->enqueueMessage($errors[$i], 'warning');
				}
			}
			
			// Save the data in the session.
			$app->setUserState('com_subscription.adduser.data', $requestData);

			// Redirect back to the adduser screen.
			$this->setRedirect(JRoute::_('index.php?option=com_subscription&view=adduser&layout=edit', false));
			return false;
		}

		// Attempt to save the data.
		$return	= $model->register($data);
		$app->setUserState('com_subscription.adduser.data', null);
		$this->setMessage(JText::_('COM_USERS_REGISTRATION_SAVE_SUCCESS'),'success');
		$this->setRedirect(JRoute::_('index.php?option=com_subscription&view=userlist', false));
		return true;
	}
}