<?php
/**
 * @version     1.0.0
 * @package     com_contentdetails
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      intotality <pooja@intotalityinc.com> - http://intotalityinc.com
 */

// No direct access.
defined('_JEXEC') or die;
jimport('joomla.log.log');
jimport('joomla.application.component.controlleradmin');

/**
 * Details list controller class.
 */
class SubscriptionControllerPayment extends JControllerAdmin
{
	public function getModel($name = 'payment', $prefix = 'SubscriptionModel')
	{
		$model = parent::getModel($name, $prefix, array('ignore_request' => true));
		return $model;
	}
	
    public function save()
	{
		// Check for request forgeries.
		global $mainframe;
		JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));
		$app	= JFactory::getApplication();
		$model = $this->getModel();
		$mainframe              = JFactory::getApplication();
		$data = $app->input->post->get('jform', array(), 'array');
		if(!isset($data['selectplan']['plan'][0]) && $data['selectplan']['plan'][0] == "")
		{
			$mainframe->redirect('index.php?option=com_subscription&view=payment&layout=active','Please select plan option!','error');
		}
		$userdate = $model->getValidEmail($data['adduser_details']['email']);
		if($userdate->tot == 0)
		{
			$mainframe->redirect('index.php?option=com_subscription&view=payment&layout=active',JText::_('COM_USERS_FAILD_EMAIL_ADDRESS'),'error');
		}
		$result = $model->getPaymentUser($userdate->id,$data);
		if($result['ACK'] != 'success')
		{
			$errorTextLog = $result['L_ERRORCODE0'].' : '.$result['L_SHORTMESSAGE0'].' : '.$result['L_LONGMESSAGE0'];
			JLog::add($_SERVER['REMOTE_ADDR'].' '.$errorTextLog,JLog::ERROR,'gantry');
			
			$errorText = $result['L_LONGMESSAGE0'];
			if($result['L_ERRORCODE0']=='10002' || $result['L_ERRORCODE0']=='10008')
				$errorText = JText::_('PAYPAL_TMP_ERROR');
				
			$mainframe->redirect('index.php?option=com_subscription&view=payment&layout=active',$errorText,'error');
			//$mainframe->redirect('index.php?option=com_subscription&view=payment&layout=active',JText::_('COM_USER_CARD_INVALID'),'error');
		}
		else
		{
			$this->setMessage(JText::_('COM_USER_CARD_SUCCESS'),'success');
			$this->setRedirect(JRoute::_('index.php?option=com_users&view=login', false));
		}
	}
	
	
	
	
	
}