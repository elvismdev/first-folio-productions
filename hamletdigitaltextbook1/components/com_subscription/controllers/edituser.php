<?php
/**
 * @version     1.0.0
 * @package     com_contentdetails
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      intotality <pooja@intotalityinc.com> - http://intotalityinc.com
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controllerform');

/**
 * Detail controller class.
 */
class SubscriptionControllerEdituser extends JControllerForm
{

	public function getModel($name = 'Edituser', $prefix = 'SubscriptionModel')
	{
		$model = parent::getModel($name, $prefix, array('ignore_request' => true));
		return $model;
	}
	
    public function save()
	{
		// Check for request forgeries.
		JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));
		$app	= JFactory::getApplication();
		$model	= $this->getModel();
		$user	= JFactory::getUser();
		$userId = JRequest::getVar('id'); 
		$data = $app->input->post->get('jform', array(), 'array');
		$olduserdata = $model->getUserEditInfo($data['id']);
		//check uniq user name
		if($data['username'] != $olduserdata->username)
		{
			$uresult =  $model->getCheckUniqueUserName($data['username']);
			if($uresult > 0)
			{
					$this->setMessage(JText::_('COM_USERS_USERNAME_EXIST'),'error');
					$this->setRedirect(JRoute::_('index.php?option=com_subscription&view=edituser&layout=edit&id='.$data['id'], false));
					return true;
			}
		}
		//check uniq email
		if($data['email'] != $olduserdata->email)
		{
			$eresult =  $model->getCheckUniqueEmail($data['email']);
			if($eresult > 0)
			{
				$this->setMessage(JText::_('COM_USERS_EMAIL_EXIST'),'error');
				$this->setRedirect(JRoute::_('index.php?option=com_subscription&view=edituser&layout=edit&id='.$data['id'], false));
				return true;
			}
		}
		if($data['password1'] != "" && $data['password2'] != "" && $data['password1'] != $data['password2'])
		{
			$this->setMessage(JText::_('COM_USERS_PASSWORD_NOMATCH'),'error');
			$this->setRedirect(JRoute::_('index.php?option=com_subscription&view=edituser&layout=edit&id='.$data['id'], false));
			return true;
		}
				
		$result = $model->save($data);
		
		//$app->setUserState('com_subscription.edituser.data', null);
		$this->setMessage(JText::_('COM_USERS_REGISTRATION_EDIT_SUCCESS'),'success');
		$this->setRedirect(JRoute::_('index.php?option=com_subscription&view=userlist', false));
		return true;
	 }
	 
	 protected function allowEdit($data = array(), $key = 'id')
	{
		
		$recordId = (int) isset($data[$key]) ? $data[$key] : 0;
		$user     = JFactory::getUser();
		$userId   = $user->get('id');
		$asset    = 'com_content.article.' . $recordId;

		// Check general edit permission first.
		if ($user->authorise('core.edit', $asset))
		{
			return true;
		}

		// Fallback on edit.own.
		// First test if the permission is available.
		if ($user->authorise('core.edit.own', $asset))
		{
			// Now test the owner is the user.
			$ownerId = (int) isset($data['created_by']) ? $data['created_by'] : 0;
			if (empty($ownerId) && $recordId)
			{
				// Need to do a lookup from the model.
				$record = $this->getModel()->getItem($recordId);

				if (empty($record))
				{
					return false;
				}

				$ownerId = $record->created_by;
			}

			// If the owner matches 'me' then do the test.
			if ($ownerId == $userId)
			{
				return true;
			}
		}

		// Since there is no asset tracking, revert to the component permissions.
		return parent::allowEdit($data, $key);
	}
}