<?php
/**
 * @version     1.0.0
 * @package     com_contentdetails
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      intotality <pooja@intotalityinc.com> - http://intotalityinc.com
 */

// No direct access.
defined('_JEXEC') or die;

jimport('joomla.application.component.controlleradmin');

/**
 * Details list controller class.
 */
class SubscriptionControllerUserlist extends JControllerAdmin
{
	/**
	 * Proxy for getModel.
	 * @since	1.6
	 */
	public function getModel($name = 'userlist', $prefix = 'SubscriptionModel')
	{
		$model = parent::getModel($name, $prefix, array('ignore_request' => true));
		return $model;
	}
	public function backuser()
	{
		$this->setRedirect(JRoute::_('index.php?option=com_users&view=user&layout=edit', false));
	}
    public function Delete()
	{
		
		if(isset($_POST['cid']) && count($_POST['cid']) > 0)
		{
			$model = $this->getModel();
			$result = $model->getDelete($_POST['cid']);
			$this->setMessage(JText::_('COM_SUBSCRIPTION_DELETE_MSG'),'success');
			$this->setRedirect(JRoute::_('index.php?option=com_subscription&view=userlist', false));
		}
		else
		{
			$this->setMessage(JText::_('COM_SUBSCRIPTION_DELETE_SELECT_ONE'),'error');
			$this->setRedirect(JRoute::_('index.php?option=com_subscription&view=userlist', false));
		}
	}
	public function Block()
	{
		
		$updateid = JRequest::getVar('updateid'); 
		if(isset($updateid) && $updateid != "")
		{
			$cid[0] = $updateid;
			$model = $this->getModel();
			$result = $model->getBlock($cid);
			$this->setMessage(JText::_('COM_SUBSCRIPTION_BLOCK_MSG'),'success');
			$this->setRedirect(JRoute::_('index.php?option=com_subscription&view=userlist', false));
		}
		else if(isset($_POST['cid']) && count($_POST['cid']) > 0)
		{
			$model = $this->getModel();
			$result = $model->getBlock($_POST['cid']);
			$this->setMessage(JText::_('COM_SUBSCRIPTION_BLOCK_MSG'),'success');
			$this->setRedirect(JRoute::_('index.php?option=com_subscription&view=userlist', false));
		}
		else
		{
			$this->setMessage(JText::_('COM_SUBSCRIPTION_BLOCK_SELECT_ONE'),'error');
			$this->setRedirect(JRoute::_('index.php?option=com_subscription&view=userlist', false));
		}
		
				
	}
	public function Unblock()
	{
		$updateid = JRequest::getVar('updateid'); 
		if(isset($updateid) && $updateid != "")
		{
			$cid[0] = $updateid;
			$model = $this->getModel();
			$result = $model->getUnblock($cid);
			$this->setMessage(JText::_('COM_SUBSCRIPTION_UNBLOCK_MSG'),'success');
			$this->setRedirect(JRoute::_('index.php?option=com_subscription&view=userlist', false));
		}
		else if(isset($_POST['cid']) && count($_POST['cid']) > 0)
		{
			$model = $this->getModel();
			$result = $model->getUnblock($_POST['cid']);
			$this->setMessage(JText::_('COM_SUBSCRIPTION_UNBLOCK_MSG'),'success');
			$this->setRedirect(JRoute::_('index.php?option=com_subscription&view=userlist', false));
		}
		else
		{
			$this->setMessage(JText::_('COM_SUBSCRIPTION_UNBLOCK_SELECT_ONE'),'error');
			$this->setRedirect(JRoute::_('index.php?option=com_subscription&view=userlist', false));
		}
	}
	
	
    
    
    
}