<?php
/**
 * @copyright	Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access.
defined('_JEXEC') or die;
jimport('joomla.application.component.controllerform');
/**
 * Countries list controller class.
 *
 * @package		Joomla.Administrator
 * @subpackage	com_providers
 * @since		1.6
 */
class SubscriptionControllerAjax extends JControllerForm
{
	public function checkusername()
	{
		$model = JModelLegacy::getInstance('edituser', 'SubscriptionModel');
		$username = JRequest::getVar('username');
		$uresult =  $model->getCheckUniqueUserName($username);
		echo $uresult;
		exit();
	}
	public function checkemail()
	{
		$model = JModelLegacy::getInstance('edituser', 'SubscriptionModel');
		$email = JRequest::getVar('email');
		$uresult =  $model->getCheckUniqueEmail($email);
		echo $uresult;
		exit();
	}
}
