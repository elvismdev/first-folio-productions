<?php
/**
 * @version     1.0.0
 * @package     com_contactdetails
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      intotality <pooja@intotalityinc.com> - http://intotalityinc.com
 */

// No direct access.
defined('_JEXEC') or die;

jimport('joomla.application.component.controlleradmin');

/**
 * Details list controller class.
 */
class SubscriptionControllerImportcontact extends JControllerAdmin
{
	public function getModel($name = 'importcontact', $prefix = 'SubscriptionModel')
	{
		$model = parent::getModel($name, $prefix, array('ignore_request' => true));
		return $model;
	}
	public function save()
	{
		// Check for request forgeries.
		JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));
		$app	= JFactory::getApplication();
		$model	= $this->getModel();
		$postvalue = JRequest::get( 'post' );
		$data 	= $postvalue['cid'];
		if(!isset($postvalue['cid']) || empty($postvalue['cid']))
		{
			$this->setMessage(JText::_('COM_SUBSCRIPTION_IMPORTCONTACT_SELECT_ONE'),'error');
			$this->setRedirect(JRoute::_('index.php?option=com_subscription&view=importcontact', false));
			return true;
		}
		$resultcount = $model->checkusersubscriptioncount();
		if($resultcount < count($data))
		{
			$this->setMessage(JText::sprintf('COM_SUBSCRIPTION_IMPORTCONTACT_ONLY',$resultcount),'error');
			$this->setRedirect(JRoute::_('index.php?option=com_subscription&view=importcontact', false));
			return true;
		}
		$resultcount = $model->save($data);
		
		$this->setMessage(JText::_('COM_SUBSCRIPTION_IMPORTCONTACT_SUCCESS'),'success');
		$this->setRedirect(JRoute::_('index.php?option=com_subscription&view=userlist', false));
		return true;
	}
}