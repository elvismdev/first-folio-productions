<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_search
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * @param   array
 * @return  array
 */
function SubscriptionBuildRoute(&$query)
{
	$segments = array();
	//echo $query['view'];
	if (isset($query['view']))
	{
		unset($query['view']);
	}
	//echo $segments;die;
	return $segments;
}

/**
 * @param   array
 * @return  array
 */
function SubscriptionParseRoute($segments)
{
	$vars = array();

	$searchword	= array_shift($segments);
	//$vars['searchword'] = $searchword;
	//$vars['view'] = 'search';

	return $vars;
}
