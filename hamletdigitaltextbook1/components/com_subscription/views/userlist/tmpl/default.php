<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted Access');
require_once JPATH_ADMINISTRATOR . '/components/com_subscription/helpers/subscription.php';
// load tooltip behavior
JHtml::addIncludePath(JPATH_COMPONENT.'/helpers/html');
JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
$session = JFactory::getSession();
$selected = $this->state->get('filter.idsubscription');
$listOrder = $this->escape($this->state->get('list.ordering'));
$listDirn = $this->escape($this->state->get('list.direction'));
?>
<h2><?php echo JText::_('COM_SUBSCRIPTION_USERLIST'); ?></h2>
<?php
if($session->get('usertype') == 0 && $session->get('numoflicense') != 1)
{
	if($this->tot_user == 0)
		{ ?>
            <div id="system-message-container">
                <div id="system-message">
                    <div class="alert alert-success">
                        <div><p><?php echo JText::_('COM_SUBSCRIPTION_NO_USER_MSG'); ?></p></div>
                    </div>
                </div>
            </div>
	<?php } 
}
?>



<?php
if($session->get('numoflicense') == 1)
{ ?>
	
	<div id="system-message-container">
                <div id="system-message">
                    <div class="alert alert-warning">
                        <div><p><?php echo JText::_('COM_SUBSCRIPTION_ONLY_ONE_USERS'); ?></p></div>
                    </div>
                </div>
            </div>
<?php
}
else
{
?>

	<form action="<?php echo JRoute::_('index.php?option=com_subscription&view=userlist'); ?>" method="post" name="adminForm" id="adminForm" class="form-horizontal">

			<div id="filter-bar" class="btn-toolbar">
            
            <div class="filter-search btn-group pull-left">
			<input type="text" name="filter_search" id="filter_search" value="<?php echo $this->escape($this->state->get('filter.search')); ?>" placeholder="<?php echo JText::_('COM_SUBSCRIPTION_SEARCH'); ?>"/>
		</div>
        
        	<div class="filter-search btn-group pull-left">
            <select name="filter_idsubscription" class="inputbox" id="filter_idsubscription">
            	<option value="all" <?php if($selected == "all"){?> selected="selected"<?php } ?>>
				<?php echo JText::_('COM_SUBSCRIPTION_ALL_SELECT'); ?></option>
				<?php foreach ($this->subscriptionlist as $j => $subitem) : ?>
                		<option value="<?php echo $subitem->id;?>" <?php if($selected ==$subitem->id){?> selected="selected"<?php } ?>>
						<?php echo date('M d,Y',strtotime($subitem->purchasedate))." -- ".date('M d,Y',strtotime($subitem->expirationdate));?>
                        </option>
                <? endforeach; ?>
				</select>
            </div>
            
            <div class="btn-group pull-left">
				<button class="btn " type="submit" title="<?php echo JText::_('JSEARCH_FILTER_SUBMIT'); ?>"><i class="icon-search"></i></button>
                
                <button class="btn " type="button" title="<?php echo JText::_('JSEARCH_FILTER_CLEAR'); ?>" onclick="document.id('filter_idsubscription').value='all';document.id('filter_search').value='';this.form.submit();"><i class="icon-remove"></i></button>
                
            </div>
            
            
			
			
            <div class="btn-group pull-right hidden-phone">
					<?php if(count($this->items) > 0) { ?> 
					<a class="btn" href="javascript:OnBlock();"><i class="icon-ban-circle"></i> <?php echo JText::_('COM_SUBSCRIPTION_BLOCK'); ?></a>
                    <a class="btn" href="javascript:OnUnblock();"><i class="icon-check"></i> <?php echo JText::_('COM_SUBSCRIPTION_UNBLOCK'); ?></a>
                    <?php } ?>
					
					<?php if($session->get('usertype') == 0 && $session->get('numoflicense') != 1)
							{ if($this->tot_user > 0) { ?>
                                <a class="btn" href="<?php echo JRoute::_('index.php?option=com_subscription&view=adduser&layout=edit'); ?>">
                                <i class="icon-user"></i> <?php echo JText::_('COM_SUBSCRIPTION_ADDUSER'); ?>
                                </a>
                   <?php } }?>
			</div>
          </div>
          <div class="clear"></div>
		<div class="space20"></div>
        	
        	<table class="table table-striped" id="detailList">
			<thead>
				<tr>
               <th width="1%" class="hidden-phone">
						<input type="checkbox" name="checkall-toggle" value="" title="<?php echo JText::_('JGLOBAL_CHECK_ALL'); ?>" onclick="Joomla.checkAll(this)" />
					</th>
                <th class='left'>
				<?php echo JHtml::_('grid.sort', 'COM_SUBSCRIPTION_TRANSACTIONS_NAME', 'a.name', $listDirn, $listOrder); ?>
				</th>
                <th class='left'>
				<?php echo JText::_('COM_SUBSCRIPTION_TRANSACTIONS_USER_NAME'); ?>
				</th>
                <th class='left'>
				<?php echo JText::_('COM_SUBSCRIPTION_TRANSACTIONS_EMAIL'); ?>
				</th>
                <th class='left'>
					<?php echo JText::_('COM_SUBSCRIPTION_TRANSACTIONS_ENABLED'); ?>
                </th>
               </tr>
			</thead>
			
			<tbody>
					<?php 
                    if(count($this->items) > 0)
                    { 
						foreach ($this->items as $i => $item) : ?>
                            <tr class="row<?php echo $i % 2; ?>">
                                <td class="center hidden-phone">
                                <?php echo JHtml::_('grid.id', $i, $item->id); ?>
                                </td>
                                <td>
                               		<a href="<?php echo JRoute::_('index.php?option=com_subscription&view=edituser&layout=edit&id=' . (int) $item->id); ?>">
										<?php echo $item->name; ?>
                                        </a>
                                </td> 
                                <td>
                                <?php echo $item->username; ?>
                                </td>
                                <td>
                                <?php echo $item->email; ?>
                                </td> 
                                <td align="center">
                                <?php
                                if($item->block == 1)
                                { ?>
                                    <a href="<?php echo JRoute::_('index.php?option=com_subscription&task=userlist.Unblock&updateid='.$item->id); ?>" title="unblock">
                                   	 	<img  src="<?php echo JURI::root();?>administrator/components/com_subscription/assets/images/stop.png">
                                    </a>
                                <?php }
                                else
                                { ?>
                                    <a href="<?php echo JRoute::_('index.php?option=com_subscription&task=userlist.Block&updateid='.$item->id); ?>" title="block">
                                    	<img  src="<?php echo JURI::root();?>administrator/components/com_subscription/assets/images/confirmed.png">
                                    </a>
                                <?php }
                                ?>
                                </td>
                                
                            </tr>
                    	<?php endforeach; ?>
				<?php }else { ?>
                				<tr>
                                	<td class="center" colspan="7">
                                    	<?php echo JText::_('COM_SUBSCRIPTION_NOTFOUND'); ?>
                                    </td>
                                </tr>
                <?php } ?>

			</tbody>
            <!--<tfoot>
                <?php 
                if(isset($this->items[0]))
				{
                    $colspan = count(get_object_vars($this->items[0]));
                }
                else{
                    $colspan = 10;
                }
            ?>
			<tr>
				<td colspan="<?php echo $colspan ?>">
					<?php echo $this->pagination->getListFooter(); ?>
				</td>
			</tr>
			</tfoot>-->
		</table>
        <table>
                <?php 
                if(isset($this->items[0]))
				{
                    $colspan = count(get_object_vars($this->items[0]));
                }
                else{
                    $colspan = 10;
                }
            ?>
			<tr>
				<td colspan="<?php echo $colspan ?>">
					<?php echo $this->pagination->getListFooter(); ?>
				</td>
			</tr>
			</table>
         <input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>" />
	<input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>" />
        <input type="hidden" name="task" id="task"/>
</form>

<?php } ?>

<script type="text/javascript">
function OnBlock()
{
	jQuery('#task').val('userlist.Block');
	jQuery('#adminForm').submit();

}
function OnUnblock()
{
	jQuery('#task').val('userlist.Unblock');
	jQuery('#adminForm').submit();
}
function OnDelete()
{
	var r=confirm("<?php echo JText::_('COM_SUBSCRIPTION_DELETE_CONFIRM'); ?>")	
	if (r==true)
  	{
		jQuery('#task').val('userlist.Delete');
		jQuery('#adminForm').submit();
	}
}
function OnClear()
{
	alert(jQuery('#filter_idsubscription').val());
	jQuery('#filter_idsubscription').val('');
	alert(jQuery('#filter_idsubscription').val());
	jQuery('#adminForm').submit();
}

</script>