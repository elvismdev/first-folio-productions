<?php
/**
 * @version     1.0.0
 * @package     com_contentaddusers
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      intotality <pooja@intotalityinc.com> - http://intotalityinc.com
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

/**
 * View to edit
 */
class SubscriptionViewPayment extends JViewLegacy
{
	protected $state;
	protected $item;
	protected $form;
	protected $id;
	protected $key_url;

	/**
	 * Display the view
	 */
	public function display($tpl = null)
	{
		$user 		= JFactory::getUser(); 
		if ($user->id && $user->id > 0) 
		{	
			global $mainframe;
			$mainframe              = JFactory::getApplication();
			$mainframe->redirect('index.php?option=com_users&view=profile');
		}
		$this->form		= $this->get('Form');
		

		
		// Check for errors.
		if (count($errors = $this->get('Errors'))) 
		{
            throw new Exception(implode("\n", $errors));
		}

		parent::display($tpl);
	}
}
