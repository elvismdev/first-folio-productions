<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted Access');
require_once JPATH_ADMINISTRATOR . '/components/com_subscription/helpers/subscription.php';
// load tooltip behavior
JHtml::_('behavior.tooltip');
?>
<?php
$session = JFactory::getSession();
$idsubscription =  $session->get('idsubscription');
$record = "";
if($session->get('usertype') == 0 && $session->get('numoflicense') != 1)
{
	if($this->tot_user == 0)
	{ ?>
	<div id="system-message-container">
	  <div id="system-message">
		<div class="alert alert-success">
		  <div>
			<p><?php echo JText::_('COM_SUBSCRIPTION_NO_USER_MSG'); ?></p>
		  </div>
		</div>
	  </div>
	</div>
	<?php } 
}
?>
 
<form action="<?php echo JRoute::_('index.php?option=com_subscription&view=importcontact'); ?>" method="post" name="adminForm" id="adminForm">
    <h2><?php echo JText::_('COM_SUBSCRIPTION_IMPORTCONTACT'); ?> </h2> 
    		
            
    		
            <div class="row-fluid">
					<?php
                    $j=0; 
                    foreach ($this->items as $i => $item) : 
                    if (!in_array($item->id, $this->importedusers)) 
                    {
                        $record = $item->id;
                        if($j > 3)
                        { $j=0; ?>
                                </div>
                                <div class="row-fluid">
                  <?php } $j++;?>
                        
                        <div class="span3">
                            <label class="checkbox" for="cb<?php echo $i;?>">
                            <?php echo JHtml::_('grid.id', $i, $item->id); ?> <?php echo $item->name?>
                            </label>
      
                        </div>
                    <?php 
                    } 
                    endforeach;  
                    ?>
             </div>
             <div class="clear"></div>
            
			<?php if($record != "")
            {?>
            <div class="space20"></div>
            <div class="divider"></div>
            <div class="space20"></div>
            <div class="user-import">
                
                <div class="pull-left">
                <label for="checkall-toggle">
                    <input type="checkbox" id="checkall-toggle" name="checkall-toggle" value="" title="<?php echo JText::_('JGLOBAL_CHECK_ALL'); ?>" onclick="Joomla.checkAll(this)"/>
                    <?php echo JText::_('JGLOBAL_CHECK_ALL'); ?>
                </label>
                </div>
                
                <div class="pull-right"><button type="submit" class="btn"><?php echo JText::_('COM_SUBSCRIPTION_IMPORTCONTACT_BTN'); ?></button></div>
                
            </div>
           <?php } 
		   else 
		   {?>
               <div class="row-fluid">
               
                   <div class="span3">
                   <?php echo JText::_('COM_SUBSCRIPTION_CONTACT_NOT_FOUND'); ?>
                    </div>
               </div>
               
           <?php } ?> 
           
    
    <input type="hidden" name="option" value="com_subscription" />
    <input type="hidden" name="task" value="importcontact.save" />
    <?php echo JHtml::_('form.token');?> 
    </form>

