<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted Access');
require_once JPATH_ADMINISTRATOR . '/components/com_subscription/helpers/subscription.php';
// load tooltip behavior
JHtml::_('behavior.tooltip');
?>
<?php
$session = JFactory::getSession();
$idsubscription =  $session->get('idsubscription');
$record = "";
if($session->get('usertype') == 0 && $session->get('numoflicense') != 1)
{
if($this->tot_user == 0)
{ ?>

<div id="system-message-container">
  <div id="system-message">
    <div class="alert alert-success">
      <div>
        <p><?php echo JText::_('COM_SUBSCRIPTION_NO_USER_MSG'); ?></p>
      </div>
    </div>
  </div>
</div>
<?php } 
}
?>
 
    <form action="<?php echo JRoute::_('index.php?option=com_subscription&view=importcontact'); ?>" method="post" name="adminForm" id="adminForm">
    <h2>
    <?php echo JText::_('COM_SUBSCRIPTION_IMPORTCONTACT'); ?>
    <button type="submit" class="btn pull-right"><?php echo JText::_('COM_SUBSCRIPTION_IMPORTCONTACT_BTN'); ?></button>
    </h2> 
    
    <div style="border:solid 1px #009; width:900; padding:10px;">
    		<?php 
			foreach ($this->items as $i => $item) : 
			if (in_array($item->id, $this->importedusers)) 
			{
				$record = $item->id;				
				?>
                <div style="float:left;width:250px; padding:5px;">
					<div style="float:left;"><?php echo JHtml::_('grid.id', $i, $item->id); ?></div>
                    <div><label for="cb<?php echo $i;?>"><?php echo $item->name?></label></div>
    				<div style="clear:both;"></div>                
                </div>
                
		<?php 
			}
			endforeach; 
		?>
        
    <div style="clear:both;"></div>    
	
           <div style="background-color:#555;">     
                <div style="float:left; width:300px;">
                 <input type="checkbox" name="checkall-toggle" value="" title="<?php echo JText::_('JGLOBAL_CHECK_ALL'); ?>" onclick="Joomla.checkAll(this)" />
                </div>
                <div style="float:left">
                 <button type="submit" class="btn"><?php echo JText::_('COM_SUBSCRIPTION_IMPORTCONTACT_BTN'); ?></button>
                </div>
                <div style="clear:both;"></div>     
            </div>
    
    </div>
    <!-- 
    <table class="table table-striped">
    <?php if(count($this->items) > 0)
    {?>
        <tr>
            <td width="1%">
          <input type="checkbox" name="checkall-toggle" value="" title="<?php echo JText::_('JGLOBAL_CHECK_ALL'); ?>" onclick="Joomla.checkAll(this)" />
          
            </td>
            <td>
            <?php echo '<b>'.JText::_('Users').'</b>'; ?>
            </td>
        </tr>
    
		<?php 
			foreach ($this->items as $i => $item) : 
			if (!in_array($item->id, $this->importedusers)) 
			{
				$record = $item->id;				
				?>
				<tr>
				<td class="center">
				<?php echo JHtml::_('grid.id', $i, $item->id); ?>
				</td>
				<td> <label for="cb<?php echo $i;?>"><?php echo $item->name?></label></td>
				</tr>
				<?php 
			}
			endforeach; 
		?>
        <?php } ?>
    
    <?php if($record == ""){?>
    <tr>
        <td colspan="2" align="center">
            <?php echo JText::_('COM_SUBSCRIPTION_CONTACT_NOT_FOUND'); ?>
        </td>
    </tr>
    <?php } ?>
    
    
    </table>
    -->
        
    <input type="hidden" name="option" value="com_subscription" />
    <input type="hidden" name="task" value="importcontact.save" />
    <?php echo JHtml::_('form.token');?> 
    </form>

