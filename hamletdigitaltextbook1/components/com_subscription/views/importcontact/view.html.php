<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.view');

class SubscriptionViewimportcontact extends JViewLegacy
{
	/**
	 * Execute and display a template script.
	 *
	 * @param   string  $tpl  The name of the template file to parse; automatically searches through the template paths.
	 *
	 * @return  mixed  A string if successful, otherwise a Error object.
	 */
		protected $items;
		protected $items_location;
		protected $pagination;
		protected $state;
		protected $form;
		protected $change_status;
		protected $activationLink;
		protected $googmaps;
		protected $AccountActivaOrNot;
		protected $script;
		protected $totalrecord;
		protected $limitrecord;
		protected $tot_user;
		protected $importedusers;
		
	
	function display($tpl = null) 
        {
                
				
				//check login
				$user 		= JFactory::getUser(); 
				if ($user->get('guest')) 
				{	
					global $mainframe;
					$mainframe              = JFactory::getApplication();
					$mainframe->redirect('index.php?option=com_users&view=login');
				}
				$model 					= JModelLegacy::getInstance('adduser', 'SubscriptionModel');
				$this->tot_user			= $model->getUserSubPaymentStatus();
				$impcmodel 				= JModelLegacy::getInstance('importcontact', 'SubscriptionModel');
				$this->importedusers	= $impcmodel->TotalSubUserList();
				$items 					= $impcmodel->TotalRecord();
				
				// Check for errors.
                if (count($errors = $this->get('Errors'))) 
                {
                        JError::raiseError(500, implode('<br />', $errors));
                        return false;
                }
				// Assign data to the view
                $this->items = $items;
				// Display the template
                parent::display($tpl);
 
        }
}
