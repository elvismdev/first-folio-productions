<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted Access');
require_once JPATH_ADMINISTRATOR . '/components/com_subscription/helpers/subscription.php';
// load tooltip behavior
JHtml::_('behavior.tooltip');
$session = JFactory::getSession();
?>
<h2> <?php echo JText::_('COM_SUBSCRIPTION_TRANSACTION_HISTORY'); ?></h2>
<form action="<?php echo JRoute::_('index.php?option=com_subscription&view=history'); ?>" method="post" name="adminForm">
 
	<table class="table table-striped" id="detailList">
			<thead>
                        <tr>
                        
                        <th class='left'>
                        <?php echo JText::_('COM_SUBSCRIPTION_TRANCATION_LABLE_PURCHASEDATE'); ?>
                        </th>
                       <!-- <th class='left'>
                        <?php echo JText::_('COM_SUBSCRIPTION_TITLE_MODIFY_DATE'); ?>
                        </th>-->
                        <th class='left'>
                        <?php echo JText::_('COM_SUBSCRIPTION_TRANCATION_LABLE_EXPIREDATE'); ?>
                        </th>
                        
                        <th class='left'>
                        <?php echo JText::_('COM_SUBSCRIPTION_TRANCATION_LABLE_NO_OF_SEAT'); ?>
                        </th>
                        
						<?php if($session->get('numoflicense') != 1)
                        { ?>
                        <th class='left' >
                        <?php echo JText::_('COM_SUBSCRIPTION_TRANCATION_LABLE_CREATED_USER'); ?>
                        </th>
                        <?php } ?> 
                        
                        <th class='left'>
                        <?php echo JText::_('COM_SUBSCRIPTION_TRANCATION_LABLE_PRICE'); ?>
                        </th>
                        
                        <th class='left'  width="15%">
                        <?php echo JText::_('COM_SUBSCRIPTION_TRANCATION_LABLE_PAYMENTSTATUS'); ?>
                        </th>
                        </tr>
			</thead>
                        <!--<tfoot>
                        <?php 
                        if(isset($this->items[0]))
                        {
                        $colspan = count(get_object_vars($this->items[0]));
                        }
                        else{
                        $colspan = 10;
                        }
                        ?>
                        <tr>
                        <td colspan="<?php echo $colspan ?>">
                        <?php echo $this->pagination->getListFooter(); ?>
                        </td>
                        </tr>
                        </tfoot>-->
						
                        <tbody>
					<?php 
                    if(count($this->items) > 0)
                    { 
						foreach ($this->items as $i => $item) : ?>
                        	<tr class="row<?php echo $i % 2; ?>">
                            
                            <td>
                            <?php echo date('M d, Y',strtotime($item->purchasedate)); ?>
                            </td> 
                            <!--<td>
                            <?php echo date('M d, Y',strtotime($item->modify_date)); ?>
                            </td> -->
                            <td>
                            <?php echo date('M d, Y',strtotime($item->expirationdate)); ?>
                            </td> 
                            
                            <td>
                            <?php echo $item->numoflicense; ?>
                            </td>
                            
                            <?php if($session->get('numoflicense') != 1)
                            { ?>
                            <td>
                            <?php //echo SubscriptionHelper::getTotalCreatedUserByDate($item->userid,$item->id,$item->purchasedate,$item->modify_date); ?>
                            <?php echo SubscriptionHelper::getTotalLicenseSubscriptionUser($item->id); ?>
                            
                            </td>
                            <?php } ?>       
                            
                            <td >
                            <?php echo "$".$item->price; ?>
                            </td> 
                            <td>
                            <img src="<?php echo JURI::root();?>administrator/components/com_subscription/assets/images/<?php echo $item->status;?>.png" title="<?php echo $item->status;?>">
                             <?php //echo $item->status; ?>
                            </td>
                            
                            
                            
                            
                            </tr>
                    <?php endforeach; ?>
				<?php }else { ?>
            				<tr>
                                	<td class="center" colspan="9">
                                    	<?php echo JText::_('COM_SUBSCRIPTION_NOTFOUND'); ?>
                                    </td>
                                </tr>
                <?php } ?>
			</tbody>
	</table>
    <table>
                        <?php 
                        if(isset($this->items[0]))
                        {
                        $colspan = count(get_object_vars($this->items[0]));
                        }
                        else{
                        $colspan = 10;
                        }
                        ?>
                        <tr>
                        <td colspan="<?php echo $colspan ?>">
                        <?php echo $this->pagination->getListFooter(); ?>
                        </td>
                        </tr>
                        </table>
        
<?php //echo $this->pagination->getListFooter(); ?>
</form>