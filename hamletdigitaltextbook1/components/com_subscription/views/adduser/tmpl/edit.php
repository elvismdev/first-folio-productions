<?php
defined('_JEXEC') or die;
require_once JPATH_ADMINISTRATOR . '/components/com_subscription/helpers/subscription.php';
JHtml::_('behavior.formvalidation');
// Get the form fieldsets.
$fieldsets = $this->form->getFieldsets();
?>
<script type="text/javascript">
	Joomla.submitbutton = function(task)
	{
		if (task == 'adduser.cancel' || document.formvalidator.isValid(document.id('adduser-form')))
		{
			Joomla.submitform(task, document.getElementById('adduser-form'));
		}
	}
</script>
<?php if($this->tot_user == 0)
	{ ?>

<div id="system-message-container">
  <div id="system-message">
    <div class="alert alert-success">
      <div>
        <p><?php echo JText::_('COM_SUBSCRIPTION_NO_USER_MSG'); ?></p>
      </div>
    </div>
  </div>
</div>
<?php } else { ?>
<form action="<?php echo JRoute::_('index.php?option=com_subscription&layout=edit&id=' . (int) $this->item->id); ?>" method="post" name="adduserForm" id="adduser-form" class="form-validate form-horizontal" enctype="multipart/form-data">
  
    <h2><?php echo JText::_('COM_USERS_USER_ACCOUNT_DETAILS'); ?></h2>
   <?php foreach ($this->form->getFieldsets() as $fieldset): // Iterate through the form fieldsets and display each one.?>
	<?php $fields = $this->form->getFieldset($fieldset->name);?>
	<?php if (count($fields)):?>
		
		<?php if (isset($fieldset->label)):// If the fieldset has a label set, display it as the legend.
		?>
			<h3><?php echo JText::_($fieldset->label);?></h3>
		<?php endif;?>
		<?php foreach ($fields as $field) :// Iterate through the fields in the set and display them.?>
			<?php if ($field->hidden):// If the field is hidden, just display the input.?>
				<?php echo $field->input;?>
			<?php else:?>
				<div class="control-group">
					<div class="control-label">
					<?php echo $field->label; ?>
					<?php if (!$field->required && $field->type != 'Spacer') : ?>
						<span class="optional"><?php echo JText::_('COM_USERS_OPTIONAL');?></span>
					<?php endif; ?>
					</div>
					
						<?php echo $field->input;?>
					
				</div>
			<?php endif;?>
		<?php endforeach;?>
        <div class="space20"></div>
		 <div class="divider"></div>
         <div class="space20"></div>
	<?php endif;?>
<?php endforeach;?>
   
 
   <div class="control-group">
   <div class="controls">
    <button type="submit" class="validate btn"><?php echo JText::_('JSAVE');?></button>
    <a class="btn" href="<?php echo JRoute::_('index.php?option=com_subscription&view=userlist'); ?>" title="<?php echo JText::_('JCANCEL');?>"><?php echo JText::_('JCANCEL');?></a>
    <input type="hidden" name="option" value="com_subscription" />
    <input type="hidden" name="task" value="adduser.save" />
    <?php echo JHtml::_('form.token');?> </div>
    </div>
  <?php echo JHtml::_('form.token'); ?>
</form>
<?php } ?>
