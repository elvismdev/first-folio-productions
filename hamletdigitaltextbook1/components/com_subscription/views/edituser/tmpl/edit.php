<?php
defined('_JEXEC') or die;
require_once JPATH_ADMINISTRATOR . '/components/com_subscription/helpers/subscription.php';
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
// Get the form fieldsets.
$fieldsets = $this->form->getFieldsets();
?>
<script type="text/javascript">
	Joomla.submitbutton = function(task)
	{
		if (task == 'edituser.cancel' || document.formvalidator.isValid(document.id('edituser-form')))
		{
			Joomla.submitform(task, document.getElementById('edituser-form'));
		}
	}
</script>

<form action="<?php echo JRoute::_('index.php?option=com_subscription&layout=edit&id=' . (int) $this->item->id); ?>" method="post" name="edituserForm" id="edituser-form" class="form-validate form-horizontal" enctype="multipart/form-data" onsubmit="return FormSubmit()">

	<h2><?php echo  JText::_('COM_USERS_USER_ACCOUNT_DETAILS'); ?></h2>
				<?php foreach ($this->form->getFieldset('adduser_details') as $field) : ?>
					<div class="control-group">
						<div class="control-label">
							<?php echo $field->label; ?>
						</div>
						<div class="controls">
							<?php echo $field->input; ?>
                            <span id="error_<?php echo $field->id; ?>" class="error"></span>
						</div>
					</div>
				<?php endforeach; ?>
	
    <div class="space20"></div>
    <div class="divider"></div>
    <div class="space20"></div>
    <div class="control-group">
    <div class="controls">
    
			 <!-- <input type="button" class="validate btn" onclick="FormSubmit();" value="<?php echo JText::_('JSAVE');?>" /> -->
             <input type="submit" class="btn btn-primary validate" onclick="return FormSubmit();" value="<?php echo JText::_('JREGISTER');?>" /> 
           <a class="btn" href="<?php echo JRoute::_('index.php?option=com_subscription&view=userlist'); ?>" title="<?php echo JText::_('JCANCEL');?>"><?php echo JText::_('JCANCEL');?></a>
			<input type="hidden" name="option" value="com_subscription" />
            <input type="hidden" name="oldusername"  id="oldusername" value="<?php echo $this->olddata->username;?>" />
            <input type="hidden" name="oldemail"  id="oldemail" value="<?php echo $this->olddata->email;?>" />
			<input type="hidden" name="task" value="edituser.save" />
			<?php echo JHtml::_('form.token');?>
		</div></div>
	<?php echo JHtml::_('form.token'); ?>
</form>


<script type="text/ecmascript">
function FormSubmit()
{
	var password2 = jQuery('#jform_password2').val();
	var password1 = jQuery('#jform_password1').val();
	var username = jQuery('#jform_username').val();
	var username = jQuery('#jform_username').val();
	var oldusername = jQuery('#oldusername').val();
	var email = jQuery('#jform_email').val();
	var oldemail = jQuery('#oldemail').val();
	var name = jQuery('#jform_name').val();
	var companyname = jQuery('#jform_companyname').val();
	var streetname = jQuery('#jform_streetname').val();
	var city = jQuery('#jform_city').val();
	var state = jQuery('#jform_state').val();
	var zipcode = jQuery('#jform_zipcode').val();
	var textcheck = "";
	
	if(name != "" && companyname != "" && streetname != "" && city != "" && state != "" && zipcode != "")
	{
			if(password2 != "" && password1 != "" && password2 != password1)
			{
				jQuery('#jform_password1').focus();
				jQuery('#error_jform_username').html('');
				jQuery('#error_jform_email').html('');
				jQuery('#jform_password1').addClass( "invalid" );
				//jQuery('#error_jform_password1').html('Password not matched.');
				jQuery('#jform_password1').attr('aria-invalid','true');
				return false;
			}
			else if(username != oldusername)
			{
					jQuery.ajax({ url:'index.php?option=com_subscription&task=ajax.checkusername&username='+username,
					type: 'get',
					success: function(output) 
					{
						if(output > 0)
						{
							
							alert('asd');
							jQuery('#jform_username').focus();
							jQuery('#jform_username').addClass( "invalid" );
							jQuery('#error_jform_password1').html('');
							jQuery('#error_jform_username').html('This username already exist.');
							jQuery('#jform_username').attr('aria-invalid','true');
							return false;
						}
						else
						{
							if(email != oldemail)
							{
								jQuery.ajax({ url:'index.php?option=com_subscription&task=ajax.checkemail&email='+email,
								type: 'get',
								success: function(data) 
								{
									if(data > 0)
									{
										jQuery('#jform_email').focus();
										jQuery('#jform_email').addClass( "invalid" );
										jQuery('#error_jform_email').html('This email address already exist.');
										jQuery('#error_jform_username').html('');
										jQuery('#error_jform_password1').html('');
										jQuery('#jform_email').attr('aria-invalid','true');
										return false;
									}
									else
									{
										//jQuery('#edituser-form').submit();
										return true;
									}
								}
								});
							}
						}
					}
					});
			}
			else
			{
					if(email != oldemail)
					{
						jQuery.ajax({ url:'index.php?option=com_subscription&task=ajax.checkemail&email='+email,
						type: 'get',
						success: function(data) 
						{
							if(data > 0)
							{
								jQuery('#jform_email').focus();
								jQuery('#error_jform_email').html('This email address already exist.');
								jQuery('#error_jform_username').html('');
								jQuery('#error_jform_password1').html('');
								jQuery('#jform_email').addClass( "invalid" );
								jQuery('#jform_email').attr('aria-invalid','true');
								return false;
							}
							else
							{
								//jQuery('#edituser-form').submit();
								return true;
							}
						}
						});
					}
					else
					{
						//jQuery('#edituser-form').submit();
						return true;
					}	
			}
	}
	else
	{
		return false;	
	}
	
}
</script>