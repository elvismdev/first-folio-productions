<?php
/**
 * @version     1.0.0
 * @package     com_contentaddusers
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      intotality <pooja@intotalityinc.com> - http://intotalityinc.com
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

/**
 * View to edit
 */
class SubscriptionViewEdituser extends JViewLegacy
{
	protected $state;
	protected $item;
	protected $form;
	protected $olddata;
	

	/**
	 * Display the view
	 */
	public function display($tpl = null)
	{
		//check login
		$model = JModelLegacy::getInstance('edituser', 'SubscriptionModel');
		$id   = JRequest::getVar('id');
		$user 		= JFactory::getUser(); 
		if ($user->get('guest')) 
		{	
			global $mainframe;
			$mainframe              = JFactory::getApplication();
			$mainframe->redirect('index.php?option=com_users&view=login');
		}
		//
		$this->state	= $this->get('State');
		$this->item		= $this->get('Item');
		$this->form		= $this->get('Form');
		$this->olddata	= $model->getUserEditInfo($id);
		
		// Check for errors.
		if (count($errors = $this->get('Errors'))) 
		{
            throw new Exception(implode("\n", $errors));
		}

		parent::display($tpl);
	}
}
