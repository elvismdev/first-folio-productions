<?php

/**
 * @version     1.0.0
 * @package     com_subscription
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      intotality <pooja@intotalityinc.com> - http://intotalityinc.com
 */
defined('_JEXEC') or die;

jimport('joomla.application.component.modellist');
jimport('joomla.application.component.helper');

/**
 * Methods supporting a list of Subscription records.
 */
class SubscriptionModeluserlist extends JModelList {

    var $_total        = null;
    var $_pagination   = null;
	
	 public function __construct($config = array()) 
	 {
        if (empty($config['filter_fields'])) 
		{
            $config['filter_fields'] = array('id', 'a.id','name', 'a.name');
        }

        parent::__construct($config);
    }
	
	 protected function getStoreId($id = '') 
	 {
       $id.= ':' . $this->getState('filter.idsubscription');
	   $id.= ':' . $this->getState('filter.search');
	   return parent::getStoreId($id);
    }
	
	
	
	 protected function populateState($ordering = null, $direction = null) 
	{
        // Initialise variables.
        $app = JFactory::getApplication('administrator');

        $idsubscription = $app->getUserStateFromRequest($this->context . '.filter.idsubscription', 'filter_idsubscription');
		$this->setState('filter.idsubscription', $idsubscription);
		
		$search = $app->getUserStateFromRequest($this->context . '.filter.search', 'filter_search');
        $this->setState('filter.search', $search);
		
		// Load the parameters.
        $params = JComponentHelper::getParams('com_subscription');
        $this->setState('params', $params);

        // List state information.
        parent::populateState('a.id', 'asc');
    }
	
	 protected function getListQuery()
        {
                $session = JFactory::getSession();
				$user = $session->get('user');
				$iduser = $user->id;
				$usersubscription = $session->get('idsubscription');
				
				$db = JFactory::getDBO();
                $query = $db->getQuery(true);
				$query->select(
				$this->getState(
				'list.select', 'a.*,s.status'
				)
				);
				$query->from($db->quoteName('#__users_license').' As ul,'.$db->quoteName('#__users').' AS a,'.$db->quoteName('#__subscription').' AS s');
				$query->where('a.id = ul.userid');
				$query->where('s.id = ul.idsubscription');
				$query->where('ul.parent_userid ='.$iduser);
				
				
					// Filter the items over the search string if set.
				if ($this->getState('filter.search') !== '' && $this->getState('filter.search') !== null)
				{
					// Escape the search token.
					$token = $db->quote('%' . $db->escape($this->getState('filter.search')) . '%');
		
					// Compile the different search clauses.
					$searches = array();
					$searches[] = 'a.name LIKE ' . $token;
					$searches[] = 'a.username LIKE ' . $token;
					$searches[] = 'a.email LIKE ' . $token;
		
					// Add the clauses to the query.
					$query->where('(' . implode(' OR ', $searches) . ')');
				}
		
				// Filter by search in title
				$idsubscription = $this->getState('filter.idsubscription');
				if (!empty($idsubscription)) 
				{
						if($idsubscription != "all")
						{
							$query->where('ul.idsubscription ='.$idsubscription);
						}
				}
				else
				{
					$query->where('ul.idsubscription ='.$usersubscription);
				}
				
				$query->group('ul.userid');
				// Add the list ordering clause.
				$query->order($db->escape($this->getState('list.ordering', 'a.name')) . ' ' . $db->escape($this->getState('list.direction', 'ASC')));
				return $query;
        }
		
		public function getSubscriptionDates($id)
		{
				$db		= JFactory::getDbo();
				$query	= $db->getQuery(true);
				$query->select('date(s.purchasedate) as startdate,date(s.expirationdate) as enddate');
				$query->from('#__subscription as s');
				$query->where('s.status = "confirmed"');
				$query->where('s.userid ='.$id);
				// Get the options.
				$db->setQuery($query);
				$result = $db->loadObjectList();
				return $result[0];
		}
		
		public function getBlock($ids)
		{
			$db =& JFactory::getDBO();
			$query= 'UPDATE #__users SET block=1 where id in ('.implode(",",$ids).')';
			$db->setQuery($query);
			$db->query();
			return true;
		}
		
		public function getUnblock($ids)
		{
			$db =& JFactory::getDBO();
			$query= 'UPDATE #__users SET block=0 where id in ('.implode(",",$ids).')';
			$db->setQuery($query);
			$db->query();
			return true;
		}
		
		public function getDelete($ids)
		{
			$db = JFactory::getDbo();
			$session = JFactory::getUser();
			//$iduser = $session->id;
				
			$query1= 'Delete From #__users where id in ('.implode(",",$ids).')';
			$db->setQuery($query1);
			$db->query();
			
			$query2= 'Delete From #__users_license where userid in ('.implode(",",$ids).')';
			$db->setQuery($query2);
			$db->query();
			
			$query3= 'Delete From #__user_usergroup_map where user_id in ('.implode(",",$ids).')';
			$db->setQuery($query3);
			$db->query();
			
			$query4= 'Delete From #__userinformation where userid in ('.implode(",",$ids).')';
			$db->setQuery($query4);
			$db->query();
			
			return true;
		}
}
