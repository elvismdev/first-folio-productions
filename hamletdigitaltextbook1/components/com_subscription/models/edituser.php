<?php
/**
 * @version     1.0.0
 * @package     com_subscription
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      intotality <pooja@intotalityinc.com> - http://intotalityinc.com
 */

// No direct access.
defined('_JEXEC') or die;

//jimport('joomla.application.component.modeladmin');

/**
 * Subscription model.
 */
class SubscriptionModeledituser extends JModelForm
{
	protected $text_prefix = 'COM_SUBSCRIPTION';
	public function getTable($type = 'Adduser', $prefix = 'SubscriptionTable', $config = array())
	{
		return JTable::getInstance($type, $prefix, $config);
	}
	public function getForm($data = array(), $loadData = true)
	{
		// Get the form.
		$form = $this->loadForm('com_subscription.edituser', 'edituser', array('control' => 'jform', 'load_data' => $loadData));
		if (empty($form))
		{
			return false;
		}

		return $form;
	}
	protected function loadFormData()
	{
		$data = $this->getData();
		$this->preprocessData('com_subscription.edituser', $data);
		return $data;
	}
	protected function preprocessForm(JForm $form, $data, $group = 'user')
	{
		$userParams = JComponentHelper::getParams('com_subscription');
		parent::preprocessForm($form, $data, $group);
	}
	protected function populateState()
	{
		
		$params	= JFactory::getApplication()->getParams('com_subscription');
		// Get the user id.
		$userId = JFactory::getApplication()->getUserState('com_subscription.edit.edituser.id');
		$userId = !empty($userId) ? $userId : (int) JFactory::getUser()->get('id');
		// Set the user id.
		$this->setState('edituser.id', $userId);
		// Load the parameters.
		$this->setState('params', $params);
	}
	
	
	public function getData()
	{
		
		$id = JRequest::getVar('id'); 
		$result = "";
		if(isset($id))
		{			 
				$user     = JFactory::getUser();
				$idparent   = $user->get('id');
				$db		= JFactory::getDbo();
				$query	= $db->getQuery(true);
				$query->select('a.*,s.companyname,s.idparent,s.streetname,s.city,s.state,s.zipcode');
				$query->from($db->quoteName('#__users').' AS a,'.$db->quoteName('#__userinformation').' AS s');
				$query->where('s.idparent ='.$idparent);
				$query->where('a.id = s.userid');
				$query->where('a.id ='.$id);
				$db->setQuery($query);
				$this->data = $db->loadObjectList();
				$this->data[0]->email1 = $this->data[0]->email;
				$this->data[0]->email2 = $this->data[0]->email;
				unset($this->data[0]->password);
				unset($this->data[0]->password2);
				return $this->data[0];
		}
	}
	
	public function save($data)
	{
		$userId = $data['id'];
		$msg = "";
		if($data['id'])
		{
			$db =& JFactory::getDBO();
			if($data['password1'] != "" && $data['password2'] != "")
			{
				
				$salt = JUserHelper::genRandomPassword(32);
				$combinesalt = $data['password1'].$salt;
				$hash = md5($combinesalt);
				$finalpasswd = $hash.':'.$salt;
			$query= "UPDATE #__users SET name='".$data['name']."',username ='".$data['username']."',email ='".$data['email']."',password ='".$finalpasswd."'  where id=".$data['id'];
			}
			else
			{
			$query= "UPDATE #__users SET name='".$data['name']."',username ='".$data['username']."',email ='".$data['email']."' where id=".$data['id'];
			}
			$db->setQuery($query);
			$db->query();
		}
		if($data['id'])
		{
			$db =& JFactory::getDBO();
			$query= "UPDATE #__userinformation SET companyname='".$data['companyname']."',streetname ='".$data['streetname']."',city ='".$data['city']."',state ='".$data['state']."',zipcode ='".$data['zipcode']."'  where userid=".$data['id'];
			$db->setQuery($query);
			$db->query();
			return "update";
		}
	}
	
	public function getUserEditInfo($id)
	{
		
		$db		= JFactory::getDbo();
		$query	= $db->getQuery(true);
		$query->select('a.*,s.companyname,s.streetname,s.city,s.state,s.zipcode');
		$query->from($db->quoteName('#__users').' AS a,'.$db->quoteName('#__userinformation').' AS s');
		$query->where('a.id = s.userid');
		$query->where('a.id ='.$id);
		$db->setQuery($query);
		$data = $db->loadObjectList();
		return $data[0];
	}
	public function getCheckUniqueEmail($email)
	{
		$db		= JFactory::getDbo();
		$query	= $db->getQuery(true);
		$query->select('count(a.id) as tot');
		$query->from($db->quoteName('#__users').' AS a');
		$query->where('a.email ="'.$email.'"');
		$db->setQuery($query);
		$data = $db->loadObjectList();
		return $data[0]->tot;
	}
	public function getCheckUniqueUserName($username)
	{
		$db		= JFactory::getDbo();
		$query	= $db->getQuery(true);
		$query->select('count(a.id) as tot');
		$query->from($db->quoteName('#__users').' AS a');
		$query->where('a.username ="'.$username.'"');
		$db->setQuery($query);
		$data = $db->loadObjectList();
		return $data[0]->tot;
	}
}