<?php
/**
 * @version     1.0.0
 * @package     com_subscription
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      intotality <pooja@intotalityinc.com> - http://intotalityinc.com
 */

// No direct access.
defined('_JEXEC') or die;

//jimport('joomla.application.component.modelform');

/**
 * Subscription model.
 */
class SubscriptionModelpayment extends JModelForm
{
	protected $text_prefix = 'COM_SUBSCRIPTION';
	public function getTable($type = 'Transaction', $prefix = 'SubscriptionTable', $config = array())
	{
		return JTable::getInstance($type, $prefix, $config);
	}
	public function getForm($data = array(), $loadData = true)
	{
		$app	= JFactory::getApplication();
		$form = $this->loadForm('com_subscription.payment', 'active', array('control' => 'jform', 'load_data' => $loadData));
		if (empty($form)) 
		{
			return false;
		}
		return $form;
	}
	protected function loadFormData()
	{
		// Check the session for previously entered form data.
		$data = JFactory::getApplication()->getUserState('com_subscription.active.payment.data', array());
		return $data;
	}
	protected function populateState()
	{
		parent::populateState();

		$id = JFactory::getApplication()->input->get('id', 0, 'int');
		$this->setState('payment.id', $id);
	}
	
	public function NVPToArray($NVPString)
	{ 
		$proArray = array();
		while(strlen($NVPString))
		{
			// name
			$keypos= strpos($NVPString,'=');
			$keyval = substr($NVPString,0,$keypos);
			// value
			$valuepos = strpos($NVPString,'&') ? strpos($NVPString,'&'): strlen($NVPString);
			$valval = substr($NVPString,$keypos+1,$valuepos-$keypos-1);
			// decoding the respose
			$proArray[$keyval] = urldecode($valval);
			$NVPString = substr($NVPString,$valuepos+1,strlen($NVPString));
		}
		return $proArray;
	}
	
	public function getPaymentUser($id,$data)
	{
		
		 //delete user
		$db = JFactory::getDbo();
		$params = &JComponentHelper::getParams('com_subscription');
		$userdata = $this->getinfo($id);
		$planpricedata = $this->getplanPriceSub($data['selectplan']['plan'][0]);
		$userinfo = $this->getUserInformation($userdata->id);
		
		$api_endpoint = "https://api-3t.paypal.com/nvp"		
		
		/*$request_params = array
							(
							'METHOD' => 'DoDirectPayment', 
							'USER' => $params->get('api_username'), 
							'PWD' => $params->get('api_password'), 
							'SIGNATURE' => $params->get('api_signature'), 
							'VERSION' => 85.0, 
							'PAYMENTACTION' => 'Sale', 					
							'IPADDRESS' => $_SERVER['REMOTE_ADDR'],
							'CREDITCARDTYPE' => $data['payment']['card_type'], 
							'ACCT' => $data['payment']['card_no'], 						
							'EXPDATE' =>$data['payment']['card_expire_month'].$data['payment']['card_expire_year'], 			
							'CVV2' => $data['payment']['card_cvc'], 
							'AMT' => $planpricedata->price
							);*/
							
		$request_params = array
								(
								'METHOD' => 'DoDirectPayment', 
								'USER' => $params->get('api_username'), 
								'PWD' => $params->get('api_password'), 
								'SIGNATURE' => $params->get('api_signature'), 
								'VERSION' => 85.0, 
								'PAYMENTACTION' => 'Sale', 					
								'IPADDRESS' => $_SERVER['REMOTE_ADDR'],
								'CREDITCARDTYPE' => $data['payment']['card_type'], 
								'ACCT' => $data['payment']['card_no'], 						
								'EXPDATE' =>$data['payment']['card_expire_month'].$data['payment']['card_expire_year'], 			
								'CVV2' => $data['payment']['card_cvc'], 
								'AMT' => $planpricedata->price, 
								'CURRENCYCODE' => 'USD', 
								'FIRSTNAME' => $data['name'], 
								'EMAIL' => $data['email1'], 
								'STREET' => $data['info']['streetname'], 
								'CITY' => $data['info']['city'], 
								'STATE' => $data['info']['state'], 					
								'COUNTRYCODE' => 'US', 
								'ZIP' => $data['info']['zipcode'], 
								'DESC' => 'Hamlet digital textbook Subscription Plan - $'.$planpricedata->price 
								);						
		//echo '<pre>';print_r($request_params);
		
		// Loop through $request_params array to generate the NVP string.
		$nvp_string = '';
		foreach($request_params as $var=>$val)
		{
			$nvp_string .= '&'.$var.'='.urlencode($val);	
		}
		
		// Send NVP string to PayPal and store response
		$curl = curl_init();
				curl_setopt($curl, CURLOPT_VERBOSE, 1);
				curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
				curl_setopt($curl, CURLOPT_TIMEOUT, 30);
				curl_setopt($curl, CURLOPT_URL, $api_endpoint);
				curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($curl, CURLOPT_POSTFIELDS, $nvp_string);
		
		$result = curl_exec($curl);
		curl_close($curl);
		// Parse the API response
		
		$result_array = $this->NVPToArray($result);
		
		if($result_array['ACK'] == "Success")
		{
				//insert subscription
				$config = JFactory::getConfig();
				$date = JFactory::getDate();
				$insertdata =new stdClass();
				//$params = &JComponentHelper::getParams( 'com_subscription');
		 		$time_period = $params->get('time_period');
		 		$insertdata->id = null;
				$insertdata->userid = $id;
				$insertdata->purchasedate = $date->toSql();
				$insertdata->expirationdate = date('Y-m-d',strtotime(date("Y-m-d") . "+ ".$time_period." month"));
				$insertdata->licensekey = $this->getLicenseKey();
				$insertdata->numoflicense = $planpricedata->plan;
				$insertdata->price = $planpricedata->price;
				$insertdata->createdby = $id;
				$insertdata->usertype = 0;
				$insertdata->status = 'confirmed';
				$insertdata->ipaddress = $_SERVER['REMOTE_ADDR'];
				$insertdata->modify_date = $date->toSql();
				$db->insertObject( '#__subscription', $insertdata, id );
				//insert subscription_log
				$subdata =new stdClass();
				$subdata->id = null;
				$subdata->userid = $insertdata->userid;
				$subdata->price = $insertdata->price;
				$subdata->numoflicense = $insertdata->numoflicense;
				$subdata->purchasedate = $insertdata->purchasedate;
				$subdata->expirationdate = $insertdata->expirationdate;
				$subdata->licensekey = $insertdata->licensekey;
				$subdata->createdby = $insertdata->createdby;
				$subdata->ipaddress = $insertdata->ipaddress;
				$subdata->status = $insertdata->status;
				$subdata->modify_date = $insertdata->modify_date;
				$db->insertObject( '#__subscription_log', $subdata, id );
				$subject        =  JText::_('COM_PAYMENT_SUCCESSFULLY_SUBJECT');
				$msg 			= JText::sprintf('COM_PAYMENT_SUCCESSFULLY_MSG',$config->get('sitename'),$userdata->name,$subdata->numoflicense,date('M d,Y',strtotime($subdata->purchasedate)),date('M d,Y',strtotime($subdata->expirationdate)),'$'.number_format($subdata->price, 2),$config->get('sitename'));
				// Send the registration email.
				$mode 			= 1;
				$sendmail = JFactory::getMailer()->sendMail($config->get('mailfrom'), $config->get('fromname'), $userdata->email, $subject, $msg,$mode);
				//return 'success';
				return $result_array;
		}
		else
		{	
				//return $id;
				return $result_array;
		}
	}
	
	public function getLicenseKey()
	{
			$d=date ("d");
			$m=date ("m");
			$y=date ("Y");
			$t=time();
			$dmt=$d+$m+$y+$t;    
			$ran= rand(0,10000000);
			$dmtran= $dmt+$ran;
			$un=  uniqid();
			$dmtun = $dmt.$un;
			$mdun = md5($dmtran.$un);
			$sort=substr($mdun, 16); // if you want sort length code.
			return $mdun;
	}
	
	public static function getplanPriceSub($id)
	{
		$options = array();
		$db		= JFactory::getDbo();
		$query	= $db->getQuery(true);
		$query->select('*');
		$query->from('#__plan_price AS a');
		$query->where('a.id ='.$id);
		// Get the options.
		$db->setQuery($query);
		$result = $db->loadObjectList();
		return $result[0];
	}
	public static function getUserInformation($id)
	{
		
		$options = array();
		$db		= JFactory::getDbo();
		$query	= $db->getQuery(true);
		$query->select('a.*');
		$query->from('#__userinformation AS a');
		$query->where('a.userid ='.$id);
		// Get the options.
		$db->setQuery($query);
		$result = $db->loadObjectList();
		return $result[0];
	}
	
	public function getValidEmail($email)
	{
		$db		= $this->getDbo();
		$query	= $db->getQuery(true);
		$query->select('count(a.id) as tot,a.id');
		$query->from($db->quoteName('#__users').' AS a,'.$db->quoteName('#__userinformation').' AS s');
		$query->where('a.id = s.userid');
		$query->where('s.usertype = 0');
		$query->where('a.email = "'.$email.'"');
		$db->setQuery($query);
		$result = $db->loadObjectList();
		return $result[0];
	}
	
	public function getinfo($id)
	{
		$db		= $this->getDbo();
		$query	= $db->getQuery(true);
		$query->select('a.id,a.name,a.email');
		$query->from($db->quoteName('#__users').' AS a');
		$query->where('a.id = '.$id);
		$db->setQuery($query);
		$result = $db->loadObjectList();
		return $result[0];
	}
	
}