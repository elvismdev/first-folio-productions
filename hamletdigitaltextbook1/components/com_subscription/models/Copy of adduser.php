<?php
/**
 * @version     1.0.0
 * @package     com_subscription
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      intotality <pooja@intotalityinc.com> - http://intotalityinc.com
 */

// No direct access.
defined('_JEXEC') or die;

//jimport('joomla.application.component.modeladmin');

/**
 * Subscription model.
 */
class SubscriptionModeladduser extends JModelForm
{
	/**
	 * @var		string	The prefix to use with controller messages.
	 * @since	1.6
	 */
	protected $text_prefix = 'COM_SUBSCRIPTION';


	/**
	 * Returns a reference to the a Table object, always creating it.
	 *
	 * @param	type	The table type to instantiate
	 * @param	string	A prefix for the table class name. Optional.
	 * @param	array	Configuration array for model. Optional.
	 * @return	JTable	A database object
	 * @since	1.6
	 */
	public function getTable($type = 'Adduser', $prefix = 'SubscriptionTable', $config = array())
	{
		return JTable::getInstance($type, $prefix, $config);
	}

	/**
	 * Method to get the record form.
	 *
	 * @param	array	$data		An optional array of data for the form to interogate.
	 * @param	boolean	$loadData	True if the form is to load its own data (default case), false if not.
	 * @return	JForm	A JForm object on success, false on failure
	 * @since	1.6
	 */
	public function getForm($data = array(), $loadData = true)
	{
		// Initialise variables.
		$app	= JFactory::getApplication();
		// Get the form.
		$form = $this->loadForm('com_subscription.adduser', 'adduser', array('control' => 'jform', 'load_data' => $loadData));
		if (empty($form)) 
		{
			return false;
		}
		return $form;
	}
	protected function loadFormData()
	{
		// Check the session for previously entered form data.
		$data = JFactory::getApplication()->getUserState('com_subscription.edit.adduser.data', array());
		if (empty($data)) 
		{
			$data = $this->getItem();
		}
		return $data;
	}

	/**
	 * Method to get a single record.
	 *
	 * @param	integer	The id of the primary key.
	 *
	 * @return	mixed	Object on success, false on failure.
	 * @since	1.6
	 */
	public function getItem()
	{
		$id = JRequest::getVar('id');
		$result = "";
		if(isset($id))
		{			 
				$db		= JFactory::getDbo();
				$query	= $db->getQuery(true);
				$query->select('a.*,s.companyname,s.streetname,s.city,s.state,s.zipcode');
				$query->from($db->quoteName('#__users').' AS a,'.$db->quoteName('#__userinformation').' AS s');
				$query->where('a.id = s.userid');
				$query->where('a.id ='.$id);
				$db->setQuery($query);
				$results = $db->loadObjectList();
				//print_r($results[0]);die;
				return $results[0];
		}
		else
		{
			return $results;
		}
		
	}
	protected function prepareTable($table)
	{
		jimport('joomla.filter.output');

		if (empty($table->id)) {

			// Set ordering to the last item if not set
			if (@$table->ordering === '') {
				$db = JFactory::getDbo();
				$db->setQuery('SELECT MAX(ordering) FROM #__users_license');
				$max = $db->loadResult();
				$table->ordering = $max+1;
			}

		}
	}
	
	public function getParentUserData($id)
	{
		$db		= JFactory::getDbo();
		$query	= $db->getQuery(true);
		$query->select('a.*');
		$query->from($db->quoteName('#__subscription').' AS a');
		$query->where("a.userid = ".$id);
		$query->where("a.status = 'confirmed'");
		$db->setQuery($query);
		$results = $db->loadObjectList();
		return $results[0];
	}
	
	protected function populateState()
	{
		parent::populateState();

		$idparent = JFactory::getApplication()->input->get('idparent', 0, 'int');
		$this->setState('adduser.idparent', $idparent);
	}
}