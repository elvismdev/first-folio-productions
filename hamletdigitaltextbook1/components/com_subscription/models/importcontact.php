<?php

/**
 * @version     1.0.0
 * @package     com_subscription
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      intotality <pooja@intotalityinc.com> - http://intotalityinc.com
 */
defined('_JEXEC') or die;

jimport('joomla.application.component.modellist');
jimport('joomla.application.component.helper');

/**
 * Methods supporting a list of Subscription records.
 */
class SubscriptionModelimportcontact extends JModelList {

    var $_total        = null;
    var $_pagination   = null;
	
	
	 public function __construct($config = array()) 
	 {
        

        parent::__construct($config);
    }
	
	 protected function getStoreId($id = '') 
	 {
       $id.= ':' . $this->getState('filter.search');
	   return parent::getStoreId($id);
    }
	
	
	
	 protected function populateState($ordering = null, $direction = null) 
	{
        // Initialise variables.
        $app = JFactory::getApplication('administrator');
		
		$search = $app->getUserStateFromRequest($this->context . '.filter.search', 'filter_search');
        $this->setState('filter.search', $search);
		
		// Load the parameters.
        $params = JComponentHelper::getParams('com_subscription');
        $this->setState('params', $params);

        // List state information.
        parent::populateState('a.id', 'asc');
    }
	
	
	 public function TotalRecord()
        {
               $session = JFactory::getSession();
			   $user = $session->get('user');
			   $iduser = $user->id;
			   $idsubscription = $session->get('idsubscription');
			    // Create a new query object.           
                $db = JFactory::getDBO();
                $query = $db->getQuery(true);
				$query->select(
				$this->getState(
				'list.select', 'a.*,ul.idsubscription'
				)
				);
				$query->from($db->quoteName('#__users_license').' As ul,'.$db->quoteName('#__users').' AS a');
				$query->where('a.id = ul.userid');
				$query->where('ul.parent_userid ='.$iduser);
				$query->where('ul.idsubscription !='.$idsubscription);
				$query->group('ul.userid');
				$query->order('a.id ASC');
				$db->setQuery($query);
				$result = $db->loadObjectList();
				return $result;
        }
	
	public function TotalSubUserList()
	{
	   $option = array();
	   $session = JFactory::getSession();
	   $user = $session->get('user');
	   $iduser = $user->id;
	   $idsubscription = $session->get('idsubscription');
		// Create a new query object.           
		$db = JFactory::getDBO();
		$query = $db->getQuery(true);
		$query->select(
		$this->getState(
		'list.select', 'a.id,ul.idsubscription'
		)
		);
		$query->from($db->quoteName('#__users_license').' As ul,'.$db->quoteName('#__users').' AS a');
		$query->where('a.id = ul.userid');
		$query->where('ul.parent_userid ='.$iduser);
		$query->where('ul.idsubscription = '.$idsubscription);
		$query->group('a.id');
		$query->order('a.id DESC');
		$db->setQuery($query);
		$result = $db->loadObjectList();
			foreach ($result as $i => $item) :
				$option[] =  $item->id;
			endforeach;
		return $option;
	}
	public function checkusersubscriptioncount()
	{
			$session = JFactory::getSession();
			$user = $session->get('user');
			$iduser = $user->id;
			$usersubscription = $session->get('idsubscription');
			$db		= JFactory::getDbo();
			$query	= $db->getQuery(true);
			$query->select('s.numoflicense as plan');
			$query->from('#__subscription as s');
			$query->where('s.status = "confirmed"');
			$query->where('s.id ='.$usersubscription);
			$db->setQuery($query);
			$result = $db->loadObjectList();
			if($result[0]->plan)
			{
					
					$query2	= $db->getQuery(true);
					$query2->select('count(ul.id) as tot');
					$query2->from('#__users_license as ul');
					$query2->where('ul.parent_userid ='.$iduser);
					$query2->where('ul.idsubscription ='.$usersubscription);
					$db->setQuery($query2);
					$scount = $db->loadObjectList();
					$r_user = explode("-",$result[0]->plan);
					$tot = $r_user[1] - $scount[0]->tot;
					return $tot;
			}
		
	}
		
	public function save($data)
	{
		$db		= $this->getDbo();
		$session = JFactory::getSession();
		$user = $session->get('user');
		$iduser = $user->id;
		$usersubscription = $session->get('idsubscription');
		foreach ($data as $i => $item) :
				$insertdata =new stdClass();
				$insertdata->id = null;
				$insertdata->userid = $item;
				$insertdata->parent_userid = $iduser;
				$insertdata->idsubscription = $usersubscription;
				$db->insertObject( '#__users_license', $insertdata, id );
				//update user  users
				$query= 'UPDATE #__users SET block=0 where id = '.$item;
				$db->setQuery($query);
				$db->query();
		endforeach;
		return "success";
	}
}
