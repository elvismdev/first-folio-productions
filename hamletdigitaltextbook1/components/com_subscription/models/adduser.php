<?php
/**
 * @version     1.0.0
 * @package     com_subscription
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      intotality <pooja@intotalityinc.com> - http://intotalityinc.com
 */

// No direct access.
defined('_JEXEC') or die;

//jimport('joomla.application.component.modeladmin');

/**
 * Subscription model.
 */
class SubscriptionModeladduser extends JModelForm
{
	protected $text_prefix = 'COM_SUBSCRIPTION';
	public function getTable($type = 'Adduser', $prefix = 'SubscriptionTable', $config = array())
	{
		return JTable::getInstance($type, $prefix, $config);
	}
	public function getForm($data = array(), $loadData = true)
	{
		// Get the form.
		$form = $this->loadForm('com_subscription.adduser', 'adduser', array('control' => 'jform', 'load_data' => $loadData));
		if (empty($form))
		{
			return false;
		}

		return $form;
	}
	protected function loadFormData()
	{
		$data = $this->getData();
		$this->preprocessData('com_subscription.adduser', $data);
		return $data;
	}
	protected function preprocessForm(JForm $form, $data, $group = 'user')
	{
		$userParams = JComponentHelper::getParams('com_subscription');
		parent::preprocessForm($form, $data, $group);
	}
	protected function populateState()
	{
		// Get the application object.
		$app = JFactory::getApplication();
		$params = $app->getParams('com_subscription');
		// Load the parameters.
		$this->setState('params', $params);
	}
	
	
	public function getItem()
	{
		$id = JRequest::getVar('id');
		$result = "";
		if(isset($id))
		{			 
				$db		= JFactory::getDbo();
				$query	= $db->getQuery(true);
				$query->select('a.*,s.companyname,s.streetname,s.city,s.state,s.zipcode');
				$query->from($db->quoteName('#__users').' AS a,'.$db->quoteName('#__userinformation').' AS s');
				$query->where('a.id = s.userid');
				$query->where('a.id ='.$id);
				$db->setQuery($query);
				$results = $db->loadObjectList();
				//print_r($results[0]);die;
				return $results[0];
		}
		else
		{
			return $results;
		}
		
	}
	
	public function getData()
	{
		
		$id = JRequest::getVar('id');
		$result = "";
		if(isset($id))
		{			 
				$db		= JFactory::getDbo();
				$query	= $db->getQuery(true);
				$query->select('a.*,s.companyname,s.streetname,s.city,s.state,s.zipcode');
				$query->from($db->quoteName('#__users').' AS a,'.$db->quoteName('#__userinformation').' AS s');
				$query->where('a.id = s.userid');
				$query->where('a.id ='.$id);
				$db->setQuery($query);
				$this->data = $db->loadObjectList();
				$this->data[0]->email1 = $this->data[0]->email;
				$this->data[0]->email2 = $this->data[0]->email;
				unset($this->data[0]->password);
				unset($this->data[0]->password2);
				return $this->data[0];
		}
		else
		{
			
				if ($this->data === null)
				{
					$this->data = new stdClass;
					$app = JFactory::getApplication();
					$params = JComponentHelper::getParams('com_subscription');
		
					// Override the base user data with any data in the session.
					$temp = (array) $app->getUserState('com_subscription.adduser.data', array());
					foreach ($temp as $k => $v)
					{
						$this->data->$k = $v;
					}
		
					// Get the groups the user should be added to after adduser.
					$this->data->groups = array();
		
					// Get the default new user group, Registered if not specified.
					$system = $params->get('new_usertype', 2);
		
					$this->data->groups[] = $system;
		
					// Unset the passwords.
					unset($this->data->password1);
					unset($this->data->password2);
		
					// Get the dispatcher and load the users plugins.
					$dispatcher = JEventDispatcher::getInstance();
					JPluginHelper::importPlugin('user');
		
					// Trigger the data preparation event.
					$results = $dispatcher->trigger('onContentPrepareData', array('com_subscription.adduser', $this->data));
		
					// Check for errors encountered while preparing the data.
					if (count($results) && in_array(false, $results, true))
					{
						$this->setError($dispatcher->getError());
						$this->data = false;
					}
				}
		}
		return $this->data;
	}
	
	public function register($temp)
	{
		$session = JFactory::getUser();
		$idparent = $session->id;
				
		$params = JComponentHelper::getParams('com_subscription');
		// Initialise the table with JUser.
		$user = new JUser;
		$data = (array) $this->getData();
		// Merge in the adduser data.
		foreach ($temp as $k => $v)
		{
			$data[$k] = $v;
		}
		// Prepare the data for the user object.
		$data['email'] = JStringPunycode::emailToPunycode($data['email1']);
		$data['password'] = $data['password1'];
		$useractivation = $params->get('useractivation');
		$sendpassword = $params->get('sendpassword', 1);
		// Bind the data.
		if (!$user->bind($data))
		{
			$this->setError(JText::sprintf('COM_USERS_REGISTRATION_BIND_FAILED', $user->getError()));
			return false;
		}
		// Store the data.
		if (!$user->save())
		{
			$this->setError(JText::sprintf('COM_USERS_REGISTRATION_SAVE_FAILED', $user->getError()));
			return false;
		}
		
		if($user->id)
		{
				$db =& JFactory::getDBO();
				$config = JFactory::getConfig();
				$session = JFactory::getSession();
				$siteurl = JUri::root();
				//insert user_group_map
				$subdata 			= new stdClass();
				$subdata->id 		= null;
				$subdata->user_id 	= $user->id;
				$subdata->group_id 	= 2;
				$db->insertObject( '#__user_usergroup_map', $subdata, id );
				//insert user information
				$info =new stdClass();
				$info->id = null;
				$info->idparent 	= $idparent;
				$info->userid 		= $user->id;
				$info->usertype 	= 1;
				$info->companyname 	= $data['companyname'];
				$info->streetname 	= $data['streetname'];
				$info->city 		= $data['city'];
				$info->state 		= $data['state'];
				$info->zipcode 		= $data['zipcode'];
				$info->ipaddress 	= $data['ipaddress'];
				$db->insertObject( '#__userinformation', $info, id );
				//insert user_group_map
				$ldata 			= new stdClass();
				$ldata->id 		= null;
				$ldata->userid 	= $user->id;
				$ldata->parent_userid  = $idparent;
				$ldata->idsubscription = $session->get('idsubscription');
				$db->insertObject( '#__users_license', $ldata, id );
				//send mail
				$subject        = JText::sprintf('COM_USERS_EMAIL_ACCOUNT_DETAILS',$config->get('sitename'),$user->name);
				$msg 			= JText::sprintf('COM_USERS_EMAIL_REGISTERED_BODY',$user->name,$config->get('sitename'),$session->name,$siteurl,$user->username,$data['password1']);
				// Send the registration email.
				$mode 			= 1;
				$sendmail = JFactory::getMailer()->sendMail($config->get('mailfrom'), $config->get('fromname'), $user->email, $subject, $msg);
				return "save";
		}
	}
	
	public function getUserSubPaymentStatus()
	{
		$session = JFactory::getSession();
	    $user = $session->get('user');
	    $id = $user->id;
	    $idsubscription = $session->get('idsubscription');
	   
		$date = JFactory::getDate();
		$options = array();
		$db		= JFactory::getDbo();
		$query	= $db->getQuery(true);
		$query->select('a.numoflicense,(select count(ul.id) from #__users_license ul where ul.parent_userid=a.userid and ul.idsubscription='.$idsubscription.') as totaluser');
		$query->from('#__subscription AS a');
		$query->where('a.userid ='.$id);
		$query->where('a.status = "confirmed"');
		$query->where('date(a.expirationdate) >'.date('Y-m-d'));
		$db->setQuery($query);
		$result = $db->loadObjectList();
		$numoflicense = explode("-",$result[0]->numoflicense);
		$tot = $numoflicense[1] - $result[0]->totaluser;
		return $tot;
	}
	
	public function getParentSubscriptionList() 
	{
		$session = JFactory::getUser();
		$id = $session->id;
		$db		= JFactory::getDbo();
		$query	= $db->getQuery(true);
		$query->select('a.*');
		$query->from($db->quoteName('#__subscription').' AS a');
		$query->where("a.userid = ".$id);
		$query->order("a.id desc");
		$db->setQuery($query);
		$results = $db->loadObjectList();
		return $results;
	}
	
	public function getScript() 
	{
			return 'media/system/js/subscription.js';
	}
	
}