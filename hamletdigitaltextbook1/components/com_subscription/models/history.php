<?php

/**
 * @version     1.0.0
 * @package     com_subscription
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      intotality <pooja@intotalityinc.com> - http://intotalityinc.com
 */
defined('_JEXEC') or die;

jimport('joomla.application.component.modellist');
jimport('joomla.application.component.helper');

/**
 * Methods supporting a list of Subscription records.
 */
class SubscriptionModelhistory extends JModelList {

    var $_total        = null;
    var $_pagination   = null;
	
	
	 protected function getListQuery()
        {
                // Create a new query object.           
                $db = JFactory::getDBO();
                $query = $db->getQuery(true);
				$query->select(
				$this->getState(
				'list.select',
				'a.*'
				)
				);
				// Select some fields
				$query->from($db->quoteName('#__subscription').' AS a');
				// Join over the user
				$query->select('u.name AS user_name');
				$query->join('LEFT', $db->quoteName('#__users').' AS u ON u.id = a.userid');
				// Join over the created by
				$query->select('us.name AS createdby_name');
				$query->join('LEFT', $db->quoteName('#__users').' AS us ON us.id = a.createdby');
				// Filter by published state
				$session = JFactory::getUser();
				$iduser = $session->id;
				$query->where('a.userid ='.$iduser);
				// Add the list ordering clause.
				//$orderCol	= $this->state->get('list.ordering', 'ordering');
				//$orderDirn	= $this->state->get('list.direction', 'DESC');
				//$query->order($db->escape($orderCol.' '.$orderDirn));
				$query->order('a.id DESC');
                return $query;
        }
}
