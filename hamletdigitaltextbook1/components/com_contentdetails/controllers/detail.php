<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_content
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * @package     Joomla.Site
 * @subpackage  com_content
 */
 class ContentdetailsControllerDetail extends JControllerForm
{
	function showhighlight()
	{
		$post = JRequest::get('post');
		$id=$post['articleid'];
		$params=$post['params'];
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('introtext')
			->from('#__content')
			->where('id = '.$id);
		$db->setQuery($query);
		$row = (array) $db->loadObjectList();
		JPluginHelper::importPlugin( 'content' );
	  $dispatcher = JEventDispatcher::getInstance();
	  $results = $dispatcher->trigger( 'onShowhighlight', array($row,$params) );
	
		echo  json_encode($row);exit;
	}
	
	
}
 ?>