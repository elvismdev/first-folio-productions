<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_articles_latest
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

require_once JPATH_SITE . '/components/com_content/helpers/route.php';

JModelLegacy::addIncludePath(JPATH_SITE . '/components/com_content/models', 'ContentModel');

/**
 * Helper for mod_articles_latest
 *
 * @package     Joomla.Site
 * @subpackage  mod_articles_latest
 */
abstract class ModArticletitleHelper
{
	public static function getList(&$params)
	{
		// Get the dbo
		$id=JRequest::getInt('id');
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('id, title')
			->from('#__content')
			->where('state = 1 AND id = '.$id);
		$db->setQuery($query);
		$row = (array) $db->loadObjectList();
		
		return $row;
	}
}
