<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_articles_latest
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
 jimport('joomla.environment.browser');
 $browser = JBrowser::getInstance();
 $agent = $browser->getAgentString();
 $document = JFactory::getDocument();
 $template_dir = JURI::root(true).'/templates/firstfolio'; 

?>
<script type="text/javascript">
function ChangeActiveIcon(id)
{
	
	var newpath = '<?php echo $template_dir ?>/images/icons/' ;
	
	if(id == 'showdefination')
	{
		jQuery('#img_showdefination').attr('src', newpath+'icon-show-defination-a.png');
		jQuery('#img_showanalysis').attr('src', newpath+'icon-show-analysis.png');
		jQuery('#img_showall').attr('src', newpath+'icon-show-all.png');
		jQuery('#img_hideall').attr('src', newpath+'icon-hide-all.png');
		jQuery('#img_hideanalysis').attr('src', newpath+'icon-hide-analysis.png');
		jQuery('#img_hidedefination').attr('src', newpath+'icon-hide-defination.png');
		
	}
	else if(id == 'hidedefination')
	{
		jQuery('#img_hidedefination').attr('src', newpath+'icon-hide-defination-a.png');
		jQuery('#img_showdefination').attr('src', newpath+'icon-show-defination.png');
		jQuery('#img_showanalysis').attr('src', newpath+'icon-show-analysis.png');
		jQuery('#img_showall').attr('src', newpath+'icon-show-all.png');
		jQuery('#img_hideall').attr('src', newpath+'icon-hide-all.png');
		jQuery('#img_hideanalysis').attr('src', newpath+'icon-hide-analysis.png');
	}
	else if(id == 'showall')
	{
		jQuery('#img_showall').attr('src', newpath+'icon-show-all-a.png');
		jQuery('#img_showanalysis').attr('src', newpath+'icon-show-analysis.png');
		jQuery('#img_showdefination').attr('src', newpath+'icon-show-defination.png');
		jQuery('#img_hideanalysis').attr('src', newpath+'icon-hide-analysis.png');
		jQuery('#img_hidedefination').attr('src', newpath+'icon-hide-defination.png');
		jQuery('#img_hideall').attr('src', newpath+'icon-hide-all.png');
	}
	else if(id == 'hideall')
	{
		jQuery('#img_hideall').attr('src', newpath+'icon-hide-all-a.png');
		jQuery('#img_hideanalysis').attr('src', newpath+'icon-hide-analysis.png');
		jQuery('#img_hidedefination').attr('src', newpath+'icon-hide-defination.png');
		jQuery('#img_showdefination').attr('src', newpath+'icon-show-defination.png');
		jQuery('#img_showanalysis').attr('src', newpath+'icon-show-analysis.png');
		jQuery('#img_showall').attr('src', newpath+'icon-show-all.png');
		
	}
	else if(id == 'hideanalysis')
	{
		jQuery('#img_hideanalysis').attr('src', newpath+'icon-hide-analysis-a.png');
		jQuery('#img_hidedefination').attr('src', newpath+'icon-hide-defination.png');
		jQuery('#img_hideall').attr('src', newpath+'icon-hide-all.png');
		jQuery('#img_showdefination').attr('src', newpath+'icon-show-defination.png');
		jQuery('#img_showanalysis').attr('src', newpath+'icon-show-analysis.png');
		jQuery('#img_showall').attr('src', newpath+'icon-show-all.png');
	}
	else
	{
		
		jQuery('#img_showanalysis').attr('src', newpath+'icon-show-analysis-a.png');
		jQuery('#img_showdefination').attr('src', newpath+'icon-show-defination.png');
		jQuery('#img_showall').attr('src', newpath+'icon-show-all.png');
		jQuery('#img_hideanalysis').attr('src', newpath+'icon-hide-analysis.png');
		jQuery('#img_hidedefination').attr('src', newpath+'icon-hide-defination.png');
		jQuery('#img_hideall').attr('src', newpath+'icon-hide-all.png');
		
	}
	
	
}

function showhighlight(params)
{
	jQuery.post("index.php?option=com_contentdetails&task=detail.showhighlight",
    {articleid:'<?php echo $_REQUEST['id'];?>',params:params},
    function(data)
    {
      jQuery.each(data, function(index,obj) {
		  //console.log(obj.introtext);
		  
		  jQuery('.item-page').html(obj.introtext);
		  
		  <?php if($browser->isMobile() || stristr($agent, 'mobile') && !(stristr($agent, 'iPad')) ){?>
		 		jQuery('.tooltip').tooltipster({position:'top'});
				 jQuery('#rt-highlights').find('img').click(function(){
					 
	});
				 
		 <?php } else {?>
			//alert("main"); 
		  if(params=='defination')
		 {    
		 	  jQuery('.defination').tooltipster();
			  jQuery('.defination').tooltipster('show');
			  jQuery('#showdefination').addClass('active');
			  jQuery('#showanalysis,#hideanalysis,#hidedefination,#showall,#hideall').removeClass('active');
		}
		 if(params=='analysis')
		 {
			  jQuery('.analysis').tooltipster();
			  jQuery('.analysis').tooltipster('show');
			  jQuery('#showanalysis').addClass('active');
			  jQuery('#showdefination,#showall,#hideanalysis,#hidedefination,#hideall').removeClass('active');
		}
		 if(params=='all')
		 {
			  jQuery('.tooltip').tooltipster();
			  jQuery('.tooltip').tooltipster('show');
			  jQuery('#showall').addClass('active');
			  jQuery('#showdefination,#showanalysis,#hideanalysis,#hidedefination,#hideall').removeClass('active');
		}
		jQuery('.tooltip').tooltipster();
		//show defination
		 jQuery(".defination").click(function (event) 
			 {
					
					var $target = jQuery(event.target);
					if($target.parent().find('span').hasClass('highlight'))
					 {
						$target.parent().find('span').removeClass('highlight');
					}
					 else
					 {	
						$target.parent().find('span').addClass('highlight');
					}
			});
		//show analysis
		  jQuery(".analysis").click(function (event) 
			{
				 var $target = jQuery(event.target);
				 if($target.parent().find('span').hasClass('highlight'))
				 {
					$target.parent().find('span').removeClass('highlight');
				 }
				 else
				 {
					$target.parent().find('span').addClass('highlight');
				 }
			});		
		
		
 		// jQuery('.tooltip').tooltipster({onlyOne:'true'});
		 <?php } ?>
	  });
	  
    }, "json");
	
}
function hidehighlight(params)
{
	
	 <?php if($browser->isMobile() || stristr($agent, 'mobile') && !(stristr($agent, 'iPad')) ){?>
	 if(params=='defination')
	{
		jQuery('.defination').tooltipster();
		jQuery(".defination").tooltipster("hide");
		jQuery("span#defination").removeClass("highlight").addClass("tooltip");
		
	}
	if(params=='analysis')
	{
		jQuery('.analysis').tooltipster();
		jQuery(".analysis").tooltipster("hide");
		jQuery("span#analysis").removeClass("highlight").addClass("tooltip");
		
	}
	if(params=='all')
	{
		jQuery(".tooltip").tooltipster("hide");
		jQuery(".item-page").find('span').removeClass("highlight");
	}
	 <?php }else{?>
	 
	 
	if(params=='defination')
	{
		jQuery(".defination").tooltipster("hide");
		jQuery('.defination').tooltipster();
		jQuery("span#defination").removeAttr("class");
		jQuery('#hidedefination').addClass('active');
		jQuery('#hideanalysis,#hideall,#showdefination,#showanalysis,#showall').removeClass('active');
			
	}
	if(params=='analysis')
	{
		jQuery(".analysis").tooltipster("hide");
		jQuery('.analysis').tooltipster();
		jQuery("span#analysis").removeAttr("class");
		jQuery('#hideanalysis').addClass('active');
		jQuery('#hidedefination,#hideall,#showdefination,#showanalysis,#showall').removeClass('active');
		
	}
	if(params=='all')
	{
		jQuery(".tooltip").tooltipster("hide");
		jQuery(".item-page").find('span').removeClass("highlight");
		jQuery('#hideall').addClass('active');
		jQuery('#hidedefination,#hideanalysis,#showdefination,#showanalysis,#showall').removeClass('active');
		
	}
	
	jQuery('.tooltip').tooltipster();
	 
	
	<?php } ?>
	
	
}
</script>
<div>
<form type="post" action="">
<?php
   // print_r($browser);
	if($browser->isMobile() || stristr($agent, 'mobile') && !(stristr($agent, 'iPad')) )
		{?>
        <?php if($params->get('showdefination')==1):?>
       <img src="<?php echo $template_dir ?>/images/icons/icon-show-defination.png" onclick="ChangeActiveIcon('showdefination'); return showhighlight('defination');" title="Show Defination" id="img_showdefination" />
       <?php endif;?>
        <?php if($params->get('hidedefination')==1):?>
        <img src="<?php echo $template_dir ?>/images/icons/icon-hide-defination.png" onclick="ChangeActiveIcon('hidedefination'); return hidehighlight('defination');" title="Hide Defination" id="img_hidedefination"/>
        <?php endif;?>
        <?php if($params->get('showanalysis')==1):?>
        <img src="<?php echo $template_dir ?>/images/icons/icon-show-analysis.png" onclick="ChangeActiveIcon('showanalysis'); return showhighlight('analysis');" title="Show Analysis" id="img_showanalysis"/>
        <?php endif;?>
        <?php if($params->get('hideanalysis')==1):?>
        <img src="<?php echo $template_dir ?>/images/icons/icon-hide-analysis.png" onclick="ChangeActiveIcon('hideanalysis'); return hidehighlight('analysis');" title="Hide Analysis" id="img_hideanalysis"/>
      <?php endif;?>
      <?php if($params->get('showall')==1):?>
        <img src="<?php echo $template_dir ?>/images/icons/icon-show-all.png" onclick="ChangeActiveIcon('showall'); return showhighlight('all');" title="Show All" id="img_showall"/>
       <?php endif;?>
       <?php if($params->get('hideall')==1):?>
        <img src="<?php echo $template_dir ?>/images/icons/icon-hide-all.png" onclick="ChangeActiveIcon('hideall');return hidehighlight('all');" title="Hide All" id="img_hideall"/>
        <?php endif;?>
       
       
        
				
	<?php }else{
?>
<?php if($params->get('showdefination')==1):?>
  <button type="button" class="btn btn-default" id="showdefination" onclick="jQuery('body').tooltipster('hide');return showhighlight('defination');" ><?php echo JText::_('MOD_CONTENTDETAILS_SHOWDEFINATION');?></button>
  <?php endif;?> 
   <?php if($params->get('hidedefination')==1):?>
  <button type="button" class="btn btn-default" id="hidedefination" onclick="return hidehighlight('defination');"><?php echo JText::_('MOD_CONTENTDETAILS_HIDEDEFINATION');?></button>
 <?php endif;?> 
 <?php if($params->get('showanalysis')==1):?> 
  <button type="button" class="btn btn-default" id="showanalysis" onclick="jQuery('body').tooltipster('hide');return showhighlight('analysis');"><?php echo JText::_('MOD_CONTENTDETAILS_SHOWANALYSIS');?></button>
  <?php endif;?>
  <?php if($params->get('hideanalysis')==1):?> 
  <button type="button" class="btn btn-default"  id="hideanalysis" onclick="return hidehighlight('analysis');"><?php echo JText::_('MOD_CONTENTDETAILS_HIDEANALYSIS');?></button>
  <?php endif;?>
  <?php if($params->get('showall')==1):?>
  <button type="button" class="btn btn-default" id="showall" onclick="jQuery('body').tooltipster('hide');return showhighlight('all');"><?php echo JText::_('MOD_CONTENTDETAILS_SHOWALL');?></button>
  <?php endif;?>
  <?php if($params->get('hideall')==1):?>
  <button type="button" class="btn btn-default" id="hideall" onclick="return hidehighlight('all');"><?php echo JText::_('MOD_CONTENTDETAILS_HIDEALL');?></button>
  <?php endif;?>
  <?php } ?>
  </form>
</div>
