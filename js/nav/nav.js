﻿//////Using for Digital art work item page navigation
function changeNavLink(){
	if(document.getElementById("navigationActive") != null)
	{
		var DigiArt_navInputid = document.getElementById("navigationActive").value;
		if (DigiArt_navInputid == "about-us")
		{
			document.getElementById("about-us").className="active";
		}
		else if (DigiArt_navInputid == "shakespearecredits")
		{
			document.getElementById("shakespearecredits").className="active";
		}
		else if (DigiArt_navInputid == "the-plays")
		{
			document.getElementById("the-plays").className="active";
		}
		else if (DigiArt_navInputid == "for-teachers")
		{
			document.getElementById("for-teachers").className="active";
		}
		else if (DigiArt_navInputid == "testimonials")
		{
			document.getElementById("testimonials").className="active";
		}
		else if (DigiArt_navInputid == "bibliography")
		{
			document.getElementById("bibliography").className="active";
		}
	}
	if(document.getElementById("hamletActive") != null)
	{
		var DigiArt_navInputid = document.getElementById("hamletActive").value;
		if (DigiArt_navInputid == "madness")
		{
			document.getElementById("madness").className="active";
		}
		else if (DigiArt_navInputid == "music")
		{
			document.getElementById("music").className="active";
		}
		else if (DigiArt_navInputid == "digital-textbook")
		{
			document.getElementById("digital-textbook").className="active";
		}
		else if (DigiArt_navInputid == "for-teachers2")
		{
			document.getElementById("for-teachers2").className="active";
		}
		else if (DigiArt_navInputid == "the-movie")
		{
			document.getElementById("the-movie").className="active";
		}
		else if (DigiArt_navInputid == "editions-of-hamlet")
		{
			document.getElementById("editions-of-hamlet").className="active";
		}
	}
	if(document.getElementById("sublink") != null)
	{
		var DigiArt_navInputidtp = document.getElementById("sublink").value;
		if (DigiArt_navInputidtp == "who-we-are")
		{
			document.getElementById("who-we-are").className="active";
		}
		else if (DigiArt_navInputidtp == "mission-statement")
		{
			document.getElementById("mission-statement").className="active";
		}
		else if (DigiArt_navInputidtp == "bios")
		{
			document.getElementById("bios").className="active";
		}
		else if (DigiArt_navInputidtp == "the-word")
		{
			document.getElementById("the-word").className="active";
		}
		else if (DigiArt_navInputidtp == "hamlet")
		{
			document.getElementById("hamlet").className="active";
		}
	}
}
if (window.addEventListener) //DOM method for binding an event 
{
	window.addEventListener("load", function () {changeNavLink();}, false); 
}
else if (window.attachEvent) //IE exclusive method for binding an event 
{
	window.attachEvent('onload',function () {changeNavLink();});
}